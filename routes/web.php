<?php



Route::get('/welcome', function () {
    return view('welcome');
});
// up route is the main route




//==================================
//start from here our customized route below
//============================= ********** it final index added later done by nimmi
//Route::resource('course','CourseController@index')->name('course.index');
Route::resource('course','CourseController');
//=================================
Route::get('/', function () {
    $course = \App\Subject::all();
    return view('course.index',compact('course'));
//    return view('course.index');
});



//========= for create route

Route::get('/front/training_regis/create/', function () {
    return view('front.training_regis.create');
});


// =========================All the resources from bellow
Route::resource('/front/courses', 'CoursesController');
//Route::resource('/admin/adcourses', 'AdcoursesController');
Route::resource('front/adcourse', 'AdcourseController');
Route::resource('admin/training_regis','Training_regisController');
Route::resource('front/training_regis','Training_regisController');
//Route::resource('front/trai_regi_displays','Trai_regi_displaysController');
Route::resource('trai_regi_displays','Trai_regi_displaysController');
Route::resource('training_confirmations','Training_confirmationsController');
Route::resource('paybkash','PaybkashController');
Route::resource('comments','CommentsController');
Route::resource('admin/admin_comments','admin_CommentsController');
Route::resource('admin/subjects','SubjectsController');
Route::resource('admin/users','UsersController');
Route::resource('admin/mentors','MentorsController'); //MentorsController//
Route::resource('orders','OrderStepsController');
Route::resource('clientfeedbacks','ClientfeedbacksController');


//========== trainer's route ====================
//Route::resource('trainers','TrainersController');
Route::resource('trainers/trainer_pinfos','Trainer_pinfosController');
Route::resource('trainers/trainer_exps','Trainer_expsController');
Route::resource('trainers/trainer_edus','Trainer_edusController');
Route::resource('trainers/trainer_accs','Trainer_accsController');
Route::resource('trainers/trainer_accs','Trainer_accsController');


//===========trainer's route===================================






//====Single Route from Bellow ====

Route::get('/user', function () {
    return view('users/create');
});
Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/trainers/thanks', function () {
    return view('thanks');
});
//Route::get('/front/about/', function () {
//    return view('front.about');
//});


//========================== for backend route without password use purposes start
Route::get('/admin/', function () {
    return view('admin/index_admin');
});
//Route::get('/users', 'UserController@index')->name('users.index');

//========================== for backend route without password use purposes start

//========================== for backend route without password use purposes close


//========================== for backend route without password use purposes close


//========================== for backend route with password
Route::get('/admin/home', function () {
    return view('admin.home');
});
Route::get('/admin/login', function () {
    return view('admin.login');
});
//========================== for backend route with password

// bitfume customized controller
//Admin Routes
Route::group(['namespace' => 'Admin'],function(){
    Route::get('admin/home','HomeController@index')->name('admin.home');
    // Users Routes
  //  Route::resource('admin/user','UserController');
    // Roles Routes
   // Route::resource('admin/role','RoleController');
    // Permission Routes
 //   Route::resource('admin/permission','PermissionController');

    // Admin Auth Routes
    Route::get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin-login', 'Auth\LoginController@login');
});


// bitfume customized controller






//=================================
// for user registration purposes this controller
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


////for admin login purposes this controller
//Route::get('/admin', 'AdminController@index');

//
//Route::prefix('admin')->group(function (){
//    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
//    Route::get('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
//    Route::get('/', 'AdminController@index')->name('admin.dashboard');
//
//
//});

//Route::get('/admin', 'AdminController@index');





//custom registration controller

Route::get('reg', 'CustomAuthController@showRegisterForm')->name('custom.register');
Route::post('reg', 'CustomAuthController@register');

//====================================================================================


// Sign up and sign in bellow
//============================
Route::get('/signup', 'UserController@getSignup')->name('users.signup');
Route::post('/signup', 'UserController@getSignup')->name('users.signup');

//Route::get('/signup',[
//'uses' => 'UserController@getSignup'
//'as' => 'users.signup'
//
//]);


//Route::post('/signup',[
//'uses' => 'UserController@getSignup'
//'as' => 'user.signup'
//
//]);
//



//Route::get('/error',function(){
//    abort(404);
//});


Route::get('/wmail',function(){
    return view('emails.welcome');
});



Route::get('/users/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation');


