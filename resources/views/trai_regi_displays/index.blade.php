


@include('layouts.header')




<!-- ============== Online Payment =================== -->
<div class="row panel panel-default">

    <div class="col-md-5 col-md-offset-1">

        <div class="h2 text-danger">Online Payment </div>
        <div class="h4 text-info">Your Interested Training Course Review </div>
        <hr>
        <div>
            <!-- Display clients course review from database -->
            <h4 class="h4">Training Course : {{ $trai_regi_displays->course_title }}</h4>
            <h5 class="h5 "><strong>Center :</strong>  {{ $trai_regi_displays->course_venue }} </h5>
            <h5 class="h5"><strong>Course Fee :</strong>  {{$trai_regi_displays->course_fee}}/- </h5>
        </div>
        <hr>
        <!-- some necessary notes -->
        <p>Pleas be noted that you can pay us through online any day before Training/Workshop</p>
        <p class="">Alos noted that you Course Registration will not be Confirmed until you Pay!</p>
        <p>Pay online any day before Training/Workshop</p>
        <!-- Form start here -->
        <div>
            <a href="{{url('training_confirmations/create')}}"  class="btn btn-success" style="max-width: 500px;" >Pay here Online</a>
            <br><br><br>
        </div>

        <!-- Form close here -->
    </div>

    <!-- ===========FOR OFFICE PAYMENT METHOD HERE============== -->
    <div class="col-md-5 ">

        <h2 CLASS="h2 text-danger">You can Pay at Office</h2>
        <h4 CLASS="h4 text-info">Follow the Instruction to pay at Our Office</h4>
        <hr>

        <a href="{{url('trai_regi_displays/show')}}"  class="btn btn-success" style="max-width: 500px;" >Follow </a>


    </div>

</div>






@include('layouts.footer')
