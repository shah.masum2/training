@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        {{--@component('mail::header', ['url' => config('app.url')])--}}
            {{--{{ config('app.name') }}--}}
@component('mail::header', ['url' => config('/')])
            DataVision Training
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

{{--Previous slot is original and below is customized by Maum--}}
{{--<h3>Hello!</h3>--}}
{{--<h5>Welcome to Datavision Training</h5>--}}
    {{--<div align="center"><a href="{{url('/')}}" class="btn btn-info btn-block" style="max-width: 150px;"> Welcome </a></div>--}}
{{--<h5 class="h5">Thanks for your Registration !</h5>--}}

    {{--<h6>Regards,</h6>--}}
    {{--<h6>DataVision Training</h6>--}}
    {{--Up Markup is customized by Masum--}}
     {{--Subcopy--}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset



    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            {{--&copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.--}}
            &copy; {{ date('Y') }} DataVision Training. All rights reserved.
        @endcomponent
    @endslot
@endcomponent
