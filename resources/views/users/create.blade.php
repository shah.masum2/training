@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>

                    <div class="panel-body">
                        <!-- alert div for blank field -->
                        {{--<div class="form-group" style="color: darkred;">--}}
                            {{--@foreach ($errors->all() as $message)--}}
                                {{--{{ $message }} <br>--}}
                            {{--@endforeach--}}
                            {{--</div>--}}
                        <form class="form-horizontal" method="POST" action="{{ url('users/') }}">
                            {{ csrf_field() }}

                            <div class="row">

                                <div class="form-group col-md-6{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label>First Name *</label>
                                    <input type="text" class="form-control input-sm" name="first_name" placeholder="Enter First Name" value="{{ old('first_name') }}" autofocus>
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label>Last Name *</label>
                                    <input type="text" class="form-control input-sm" name="last_name" value="{{ old('last_name') }}" placeholder="Enter Last Name" autofocus>
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email *</label>
                                    <input type="email" class="form-control input-sm" name="email" value="{{ old('email') }}" placeholder="Enter Mail">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6 {{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label>Phone *</label>
                                    <input type="text" class="form-control input-sm" name="phone" value="{{ old('phone') }}" placeholder="Enter Phone Numer.">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6 {{ $errors->has('interest_course') ? ' has-error' : '' }}">
                                    <label>Area of Interest *</label>
                                    <select name="interest_course"  id="interest_course" class="form-control input-sm">
                                        <option value="">Select</option>
                                        <option value="PHP">PHP</option>
                                        <option value="JAVA">JAVA</option>
                                        <option value="ASP.NET">ASP.NET</option>
                                        <option value="DATABASE">DATABASE</option>
                                        <option value="NETWORKING">NETWORKING</option>
                                        <option value="CMS">CMS</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-6 {{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <label>Gender *</label>
                                    <select name="gender"  id="gender" class="form-control input-sm">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>

                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Organization </label>
                                    <input type="text" class="form-control input-sm" name="organization" value="{{ old('organization') }}" placeholder="Organization Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Designation </label>
                                    <input type="text" class="form-control input-sm" name="designation" value="{{ old('designation') }}"  placeholder="Enter Designation">
                                </div>
                                <div class="form-group col-md-6 {{ $errors->has('venue') ? ' has-error' : '' }}">
                                    <label>Choose Venue *</label>
                                    <select name="venue"  id="venue" class="form-control input-sm">
                                        <option value="Dhanmondi">Dhanmondi</option>
                                        <option value="Banani">Banani</option>

                                    </select>

                                </div>
                                <div class="form-group col-md-6 conceal" style="visibility: hidden;">

                                    <input type="text" class="form-control input-sm text-hide conceal">

                                </div>

                            </div> <!-- form first row -->



                            <hr>
                            <p class="text-danger">Create Your username and password  to login Datavision</p>
                            <!-- for user registration  field -->
                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                    <label>Username </label>
                                    <input type="text" class="form-control input-sm" name="user_id" value="{{ old('user_id') }}" placeholder="Enter Username">
                                    @if ($errors->has('user_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password </label>
                                    <input type="password" class="form-control input-sm" name="password" placeholder="Enter Password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label>Confirm Password </label>
                                    <input type="password" name="password_confirmation" id="password-confirm" class="form-control input-sm" placeholder="Confirm Password">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif

                                </div><!-- password-confirm -->


                            </div>



                            <div class="form-group col-md-6 col-md-offset-3">
                                <input type="submit" value="Submit" name="submit" class="btn btn-info btn-block">
                            </div>





                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
