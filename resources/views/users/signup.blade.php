@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('users.signup') }}">
                        {{ csrf_field() }}

                        <!-- original form field  -->
                        {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                            {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Register--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                            <!-- original form field close -->




                            <!-- custom form field below -->
                            <div class="row">

                                <div class="form-group col-md-6{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label>First Name *</label>
                                    <input type="text" class="form-control input-sm" name="first_name" placeholder="Enter First Name" value="{{ old('first_name') }}" autofocus>
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>




                                <div class="form-group col-md-6">
                                    <label>Last Name <span><p style="color: darkred;">*</p></span></label>
                                    <input type="text" class="form-control input-sm" name="last_name" placeholder="Enter Last Name">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Email </label>
                                    <input type="email" class="form-control input-sm" name="email" placeholder="Enter Mail">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Phone </label>
                                    <input type="text" class="form-control input-sm" name="phone" placeholder="Enter Phone Num.">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Area of Interest </label>
                                    <select name="interest_course" id="interest_course" class="form-control input-sm">
                                        <option value="">Select</option>
                                        <option value="PHP">PHP</option>
                                        <option value="JAVA">JAVA</option>
                                        <option value="ASP.NET">ASP.NET</option>
                                        <option value="DATABASE">DATABASE</option>
                                        <option value="NETWORKING">NETWORKING</option>
                                        <option value="CMS">CMS</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-6">
                                    <label>Gender </label>
                                    <select name="gender" id="gender" class="form-control input-sm">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>

                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Organization </label>
                                    <input type="text" class="form-control input-sm" name="organization" placeholder="Organization Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Designation </label>
                                    <input type="text" class="form-control input-sm" name="designation" placeholder="Enter Designation">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Choose Venue </label>
                                    <select name="venue" id="venue" class="form-control input-sm">
                                        <option value="Dhanmondi">Dhanmondi</option>
                                        <option value="Banani">Banani</option>

                                    </select>

                                </div>
                                <div class="form-group col-md-6 conceal" style="visibility: hidden;">

                                    <input type="text" class="form-control input-sm text-hide conceal">

                                </div>

                            </div> <!-- form first row -->



                            <hr>
                            <p class="text-danger">Create Your username and password  to login Datavision</p>
                            <!-- for user registration  field -->
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Username </label>
                                    <input type="text" class="form-control input-sm" name="user_id" placeholder="Enter Username">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Password </label>
                                    <input type="password" class="form-control input-sm" name="password" placeholder="Enter Password">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Confirm Password </label>
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                </div>


                            </div>



                            <div class="form-group col-md-6 col-md-offset-3">
                                <input type="submit" value="Submit" name="submit" class="btn btn-info btn-block">
                            </div>
                            <!-- alert div for blank field -->
                            <div class="form-group" style="color: darkred;">
                                @foreach ($errors->all() as $message)
                                    {{ $message }}
                                @endforeach
                            </div>




                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
