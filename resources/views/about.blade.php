
@include('layouts.header')

<!-- image row -->
<div class="container-fluid">
<div class="row">

    <img src="{{url('frontend/img/it2png.png')}}" class="img-rounded" alt="" height="400" width="">
    
</div> <!-- image row close -->
</div>

<div class="container-fluid">
<div class="row" style="margin-top: 15px;">

    <div class="col-md-8">
        <h3>Welcome to </h3>
        <h1>Datavision Training</h1>

        <p class="fadeInLeft" align="justify">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Consequuntur ex laudantium magnam, perferendis quisquam tempora
            tenetur voluptatem? Ab aliquid animi, autem consequatur corporis
            cupiditate dolor earum error facere facilis hic in labore laborum,
            magnam natus neque non nostrum obcaecati quaerat quasi quibusdam
            quisquam quo quod repellendus repudiandae saepe sapiente.
            Ad assumenda error numquam odit repellendus saepe veniam!
            Eius, ipsum, tenetur? Aliquam aspernatur culpa, doloremque
            error harum iusto, laudantium neque nulla officia quasi
            quidem recusandae rem sed, sit voluptatum? Asperiores assumenda
            consequatur consequuntur, culpa cumque deserunt dignissimos
            explicabo laboriosam necessitatibus quod sunt unde vel,
            voluptatibus. Ducimus non quam voluptas? Iure, totam.
        </p>
        <h5 class="btn btn-info btn-sm">Read more <i class="fa fa-angle-double-right" aria-hidden="true"></i> </h5>

        <hr>


        <!-- ========= image div =============== -->
        <div class="row" style="margin-top: 15px;">


            <div class="col-md-6">
                <div class=" h4" style="padding: 5px 50px;background-color: silver;">Present Courses</div>
                <img src="{{url('frontend/img/about/about1.jpg')}}" class="img-rounded" alt="" height="250" width="420">
                <br>
                <h4 style="margin-top: 10px;"><a href="#">This is the Business Solution</a></h4>
                <p class="" align="justify">
                    Lste magnam molestiae nisi,
                    qui quisquam repellendus sunt ut! Aperiam culpa eum magni
                    molestias velit?
                </p>

                <h4><a href="#">This is the Businee Solution</a></h4>
                <p class="" align="justify">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Ad aperiam commodi, ea eaque eius, ex excepturi nihil porro
                    quae quasi quia quod tempora vitae, voluptas voluptatibus.
                    <br> <a href="#" ><i class="fa fa-angle-double-right"> Read more </i></a>


                </p>





            </div>


            <div class="col-md-6">
                <div class=" h4" style="padding: 5px 50px;background-color: silver;">Present Courses</div>
                <img src="{{url('frontend/img/about/about3.jpg')}}" class="img-rounded" alt="" height="250" width="420">
                <br>
                <h4 style="margin-top: 10px;"><a href="#">This is the Business Solution</a></h4>
                <p class="" align="justify">
                    Lste magnam molestiae nisi,
                    qui quisquam repellendus sunt ut! Aperiam culpa eum magni
                    molestias velit?
                </p>

                <h4><a href="#">This is the Businee Solution</a></h4>
                <p class="" align="justify">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Ad aperiam commodi, ea eaque eius, ex excepturi nihil porro
                    quae quasi quia quod tempora vitae, voluptas voluptatibus.
                    <br> <a href="#" ><i class="fa fa-angle-double-right"> Read more </i></a>

                </p>





            </div>


        </div>
        <!-- ========= image div close =============== -->



    </div><!-- col-md-8 -->

    <div class="col-md-4">
        <div style="background-color: #00A094;padding: 25px;">

            <h3 style="color:white;">Latest News</h3>
            <hr>
            <!-- for news div -->
            <div>
                <p style="color: gold;"><i class="fa fa-envelope-o"></i> November 11, 2007 </p>
                <p style="color: lightgray;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus,
                    id incidunt placeat tempore voluptas. <br>
                    <a href="#" style="color: white;"> more ... <i class="fa fa-chevron-circle-right"></i></a></p>
            </div>

            <div>
                <p style="color: gold;"><i class="fa fa-envelope-o"></i> November 11, 2007 </p>
                <p style="color: lightgray;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus,
                    id incidunt placeat tempore voluptas. <br>
                    <a href="#" style="color: white;"> more ... <i class="fa fa-chevron-circle-right"></i></a></p>
            </div>

            <div>
                <p style="color: gold;"><i class="fa fa-envelope-o"></i> November 11, 2007 </p>
                <p style="color: lightgray;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus,
                    id incidunt placeat tempore voluptas. <br>
                    <a href="#" style="color: white;"> more ... <i class="fa fa-chevron-circle-right"></i></a></p>
            </div>
            <!-- for new div close -->




        </div>


        <!-- Comments Us div  -->
        <div class="panel panel-default" style="margin-top: 10px; padding: 25px; background-color: white;">

            <h3 style="margin-top: 10px;">Send Your Message</h3>
            @if(session()->has('message'))
                <p class="text-danger">{{ session('message') }}</p>
            @endif


            <form action="{{url('comments')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                @foreach ($errors->all() as $message)
                    {{ $message }}<br>
                @endforeach
                <input type="text" placeholder=" Name " name="name" class="form-control input-sm">
                <input type="email" placeholder="Enter your mail" name="email" class="form-control input-sm">
                <textarea  name="message" placeholder="Enter your Message" cols="2" class="form-control input-sm"> </textarea></textarea>
                <br>

                <input type="submit"  class="btn btn-primary" name="submit" value="Send">

            </form>

        </div>
        <!-- news letter div  -->
        <div class="panel panel-default" style="margin-top: 10px; padding: 25px; background-color: white;">
            <h3 style="margin-top: 10px;">Newsletter Sign-up</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus, quod!</p>

            <form action="" class=" ">
                <input type="text" placeholder="Enter your mail" name="newsletter" class="form-control input-sm">
                <br>

                <input type="submit" class="btn" name="cancel" value="Unsubscribe">
                <input type="submit"  class="btn btn-primary" name="submit" value="Subscibe">

            </form>

        </div>


    </div><!-- col-md-4 -->


</div>
</div>





@include('layouts.footer')
