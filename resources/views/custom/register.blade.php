@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('custom.register') }}">
                        {{ csrf_field() }}

                        <!-- alert div for blank field -->
                            <div class="form-group" style="color: darkred;">
                                @foreach ($errors->all() as $message)
                                    {{ $message }} <br>
                                @endforeach
                            </div>



                            <!-- custom form field below -->
                            <div class="row">

                                <div class="form-group col-md-6{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label>First Name *</label>
                                    <input type="text" class="form-control input-sm" name="first_name" value="{{ old('first_name') }}" placeholder="Enter First Name" value="{{ old('first_name') }}" autofocus>
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>




                                <div class="form-group col-md-6">
                                    <label>Last Name </label>
                                    <input type="text" class="form-control input-sm" name="last_name" value="{{ old('last_name') }}" placeholder="Enter Last Name">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Email </label>
                                    <input type="email" class="form-control input-sm" name="email" value="{{ old('email') }}" placeholder="Enter Mail">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Phone </label>
                                    <input type="text" class="form-control input-sm" name="phone" value="{{ old('phone') }}" placeholder="Enter Phone Num.">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Area of Interest </label>
                                    <select name="interest_course" id="interest_course" class="form-control input-sm">
                                        <option value="">Select</option>
                                        <option value="PHP">PHP</option>
                                        <option value="JAVA">JAVA</option>
                                        <option value="ASP.NET">ASP.NET</option>
                                        <option value="DATABASE">DATABASE</option>
                                        <option value="NETWORKING">NETWORKING</option>
                                        <option value="CMS">CMS</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-6">
                                    <label>Gender </label>
                                    <select name="gender" id="gender" class="form-control input-sm">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>

                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Organization </label>
                                    <input type="text" class="form-control input-sm" value="{{ old('organization') }}" name="organization" placeholder="Organization Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Designation </label>
                                    <input type="text" class="form-control input-sm" value="{{ old('designation') }}" name="designation" placeholder="Enter Designation">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Choose Venue </label>
                                    <select name="venue" id="venue" class="form-control input-sm">
                                        <option value="Dhanmondi">Dhanmondi</option>
                                        <option value="Banani">Banani</option>

                                    </select>

                                </div>
                                <div class="form-group col-md-6 conceal" style="visibility: hidden;">

                                    <input type="text" class="form-control input-sm text-hide conceal">

                                </div>

                            </div> <!-- form first row -->



                            <hr>
                            <p class="text-danger">Create Your username and password  to login Datavision</p>
                            <!-- for user registration  field -->
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Username </label>
                                    <input type="text" class="form-control input-sm" value="{{ old('user_id') }}" name="user_id" placeholder="Enter Username">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Password </label>
                                    <input type="password" class="form-control input-sm" name="password" placeholder="Enter Password">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Confirm Password </label>
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                </div>


                            </div>



                            <div class="form-group col-md-6 col-md-offset-3">
                                <input type="submit" value="Submit" name="submit" class="btn btn-info btn-block">
                            </div>




                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
