<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<link href="{{ asset('backend/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <link rel="manifest" href="{{ asset('front-end/site.webmanifest') }}">
    <link rel="{{asset('apple-touch-icon" href="icon.png') }}">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{asset('front-end/css/normalize.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/animate.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/responsive-nav.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/main.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/contact.css') }}">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body onscroll="myFunction()">

<div class="home_body"> <!-- === Home body start === -->

    <!-- === nav start === -->
    <div class="sticky_nav">
        <div id="nav1" class="nav_top bg_navi_blue white">
            <div class="container  text-right ">
                <ul class="list-inline list-unstyled">
                    <li> 02-9614457</li>
                    <li>Email: info@datavisiontraining.com</li>
                    @guest
                        <li><a href="{{route('register')}}"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a></li>
                        <li><a href="{{route('login')}}"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
                        @else

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->first_name.' '.Auth::user()->last_name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" style="text-align: center;">Logout</a>
                                        {{--<a href="{{ route('logout') }}" >Logout </a>--}}

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif



                </ul>
            </div>
        </div>

    </div>

    <nav id="navbar" class="navbar navbar-default" data-spy="affix" data-offset-top="197">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('/front-end/img/tr_logo.png')}}" class="img-responsive"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{url('/')}}" class="active">Home</a></li>
                    <li><a href="{{url('about')}}">About</a></li>
                    <li><a href="#home_course">Coures</a></li>
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Courses <span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Course Schedule <span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    <li><a href="{{url('/trainers/trainer_pinfos/create')}}">Join as a Trainer</a></li>
                    <li><a href="{{url('/contact/')}}">Contact</a></li>
                </ul>


            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <!-- === nav end === -->



{{--<!-- === nav start === -->--}}
    {{--<!-- nav1 -->--}}
    {{--<div id="nav1 app" class="nav_top bg_navi_blue white">--}}
        {{--<div class="container  text-right ">--}}
            {{--<ul class="list-inline list-unstyled">--}}
                {{--<li> 02-9614457</li>--}}
                {{--<li>Email: info@datavisiontraining.com</li>--}}
                {{--@guest--}}
                {{--<li><a href="{{route('register')}}"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a></li>--}}
                {{--<li><a href="{{route('login')}}"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>--}}
                {{--@else--}}

                        {{--<li class="dropdown">--}}
                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">--}}
                                {{--{{ Auth::user()->first_name.' '.Auth::user()->last_name }} <span class="caret"></span>--}}
                            {{--</a>--}}

                            {{--<ul class="dropdown-menu">--}}
                                {{--<li>--}}
                                    {{--<a href="{{ route('logout') }}"--}}
                                       {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();" style="text-align: center;">Logout</a>--}}
                                    {{--<a href="{{ route('logout') }}" >Logout </a>--}}

                                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                        {{--{{ csrf_field() }}--}}
                                    {{--</form>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--@endif--}}



            {{--</ul>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- /nav1 -->--}}


    {{--<nav id="navbar" class="navbar navba -default">--}}
        {{--<div class="container">--}}
            {{--<!-- Brand and toggle get grouped for better mobile display -->--}}
            {{--<div class="navbar-header">--}}
                {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">--}}
                    {{--<span class="sr-only">Toggle navigation</span>--}}
                    {{--<span class="icon-bar"></span>--}}
                    {{--<span class="icon-bar"></span>--}}
                    {{--<span class="icon-bar"></span>--}}
                {{--</button>--}}
                {{--<a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('/front-end/img/tr_logo.png')}}" alt="" height="80" width="120" class="img-responsive"></a>--}}
                {{--<a class="navbar-brand" href="{{url('/')}}">Brand</a>--}}
            {{--</div>--}}

            {{--<!-- Collect the nav links, forms, and other content for toggling -->--}}
            {{--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">--}}

                {{--<ul class="nav navbar-nav navbar-right">--}}
                    {{--<li><a href="{{url('/')}}" class="active">Home</a></li>--}}
                    {{--<li><a href="{{url('about')}}">About</a></li>--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Courses <span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Course Schedule <span class="caret"></span></a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a href="{{url('/trainers/trainer_pinfos/create')}}">Join as a Trainer</a></li>--}}
                    {{--<li><a href="{{url('/contact/')}}">Contact</a></li>--}}
                {{--</ul>--}}


            {{--</div><!-- /.navbar-collapse -->--}}
        {{--</div><!-- /.container-fluid -->--}}
    {{--</nav>--}}
    {{--<!-- === nav end === -->--}}
