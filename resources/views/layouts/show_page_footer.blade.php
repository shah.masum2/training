




<!-- === footer  start === -->

<div class="container-fluid">
    <div class="row footer_head bg_navi_blue text-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-6 margin_top margin_bottom">
                <h4><strong>Do you have any questions?</strong></h4>
                <h4>Feel free to contact us! </h4>
            </div>
            <div class="col-md-6 text-center">
                <a href="#" class="btn">Click Here!</a>
            </div>
        </div>
    </div>
    <div class="row footer_bg ">
        <div class="container">
            <div class="row footer_text margin_top margin_bottom">
                <div class="col-md-4">
                    <h3 class="text-left margin_top margin_bottom navi_blue"> <strong> DATAVISION</strong></h3>

                    <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
                </div>
                <div class="col-md-2 text-left">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ed_heading_top ">
                                <h5 class=" margin_top margin_bottom navi_blue"> User Help </h5>
                            </div>
                        </div>
                    </div><br>
                    <ul class="list-unstyled">
                        <li><a href="#">Latest info</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Updates</a></li>
                        <li><a href="#">Career</a></li>
                        <li><a href="#">Questions</a></li>
                    </ul>
                </div>
                <div class="col-md-2 text-left">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ed_heading_top ">
                                <h5 class="margin_top margin_bottom navi_blue"> Social Links </h5>
                            </div>
                        </div>
                    </div><br>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i>  Facebook</a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i>  Linked-in</a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> Google plus</a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i> Pinterest</a></li>
                    </ul>
                </div>
                <div class="col-md-4 text-left">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ed_heading_top ">
                                <h5 class=" margin_top margin_bottom navi_blue"> Address </h5>
                            </div>
                        </div>
                    </div><br>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i>  Address: H # 17 , R # 4 Lorem ipsum dolor sit amet adipiscing elit, Dhaka, Bangladesh</p>
                    <p><i class="fa fa-envelope" aria-hidden="true"></i>  E-maildatavision@gmail.com</p>
                    <p><i class="fa fa-phone" aria-hidden="true"></i>  Mob: +008 017555555</p>
                </div>
            </div>

            <div class="row footer_copy margin_top">
                <hr>
                <p class="text-center">&copy; DataVision | All right reserved</p>
            </div>
        </div>
    </div>
</div>



</div>           <!-- === footer  end === -->
</div>





{{--<link rel="stylesheet" href="{{asset('front-end/css/normalize.css') }}">--}}

<script src="{{asset('front-end/js/vendor/modernizr-3.5.0.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="{{asset('front-end/js/plugins.js') }}"></script>
<script src="{{asset('front-end/js/bootstrap.min.js') }}"></script>
<script src="{{asset('front-end/js/jquery-ui.js') }}"></script>
<script src="{{asset('front-end/js/responsive-nav.js') }}"></script>
<script src="{{asset('front-end/js/main.js') }}"></script>
<script src="{{asset('front-end/js/validator.js') }}"></script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<script>
    window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
    ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>
</body>
</html>
