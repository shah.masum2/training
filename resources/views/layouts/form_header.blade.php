<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<link href="{{ asset('backend/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <link rel="manifest" href="{{ asset('front-end/site.webmanifest') }}">
    <link rel="{{asset('apple-touch-icon" href="icon.png') }}">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{asset('front-end/css/normalize.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/animate.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/responsive-nav.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/main.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/contact.css') }}">
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<body onscroll="myFunction()">

<div class="home_body"> <!-- === Home body start === -->

    <!-- === nav start === -->
    <!-- nav1 -->
    <div id="nav1 app" class="nav_top bg_navi_blue white">
        <div class="container  text-right ">
            <ul class="list-inline list-unstyled">
                <li>+008 017-55335533</li>
                <li>Email: datavision@gmail.com</li>
                @guest
                <li><a href="{{route('register')}}"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a></li>
                <li><a href="{{route('login')}}"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
                @else

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->first_name.' '.Auth::user()->last_name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" style="text-align: center;">Logout</a>
                                    {{--<a href="{{ route('logout') }}" >Logout </a>--}}

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endif



            </ul>
        </div>
    </div>
    <!-- /nav1 -->


    <!-- === nav end === -->
