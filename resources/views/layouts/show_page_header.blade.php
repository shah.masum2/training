<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<link href="{{ asset('backend/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <link rel="manifest" href="{{ asset('front-end/site.webmanifest') }}">
    <link rel="apple-touch-icon" href="icon.png') }}">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{asset('front-end/css/normalize.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/animate.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/responsive-nav.css') }}">
    <link rel="stylesheet" href="{{asset('front-end/css/main.css') }}">
</head>
