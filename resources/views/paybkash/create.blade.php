



@include('layouts.header')

<div class="container center">

<div class="row">

    <div class="col-md-9  panel panel-default">
        <br>
        <h3 class="text-info">Follow the Step to Pay the Fee </h3>
        {{--<img src="{{url('frontend/img/payment_img/bkash_payment_method3.png')}}" width="600" height="400" alt="">--}}

        <div class="">
            {{--{{$paybkashs}}--}}
            <table class="table table-strip">
              <tbody>
                <tr class="success">
                    <td>1.  Go to your bKash Mobile Menu by dialing *247#</td>
                </tr>
                <tr class="success">
                    <td>2.  Choose “Payment”</td>
                </tr>
                <tr class="success">
                    <td>3.  Enter the Merchant bKash Account Number 0180000000</td>
                </tr>
                <tr class="success">
                    <td>4.  Enter the amount Course Fee TK. </td>
                </tr>

                <tr class="success">
                    <td>5.  Enter a reference* against your payment (you can mention the purpose of the transaction in one word. e.g. Bill)</td>
                </tr>

                <tr class="success">
                    <td>6.  Enter the Counter Number* 15)</td>
                </tr>
                <tr class="success">
                    <td>7.  Now enter your bKash Mobile Menu PIN to confirm</td>
                </tr>

                <tr class="success">
                    <td>8.  After Transaction you will get an SMS</td>
                </tr>

               </tbody>

           </table>


        </div>
<p class="text-danger"> Please input the transaction ID of 10 Digit  </p>

        <div>
            <form action="{{url('paybkash')}}" method="post">
                {{csrf_field()}}
                <div class="row">

                    <input type="hidden" name="user_id" id="" value="{{$paybkashs->user_id}}">
                    <input type="hidden" name="subject_id" id="" value="{{$paybkashs->subject_id}}">
                    <input type="hidden" name="course_fee" id="" value="{{$paybkashs->course_fee}}">
                    {{--<input type="hidden" name="venue" id="" value="{{$paybkashs->venue}}">--}}
                    {{--<input type="hidden" name="mobile" id="" value="">--}}

                    <div class="col-md-9">
                        <input type="text" placeholder="Please enter the Transaction ID " name="transaction_id" class="form-control input-sm" required>
                    </div>
                    <div class="col-md-3">
                        <input type="submit" name="submit"  value="Submit" class="btn btn-info btn-block">
                        <br>
                    </div>

                </div>
            </form>
        </div>


    </div>


</div>

</div>


@include('layouts.footer')







