

@include('layouts.header')


<div class="container">


    <!-- online payment methods confirmation details and submit -->
    <div class="row">
        {{--<div class="">--}}
        <div class="panel">

        {{--{{$training_confirmations}}--}}

        <!-- payment  headings -->
            <div class="h3 text-info"> <i class="fa fa-credit-card-alt text-danger"></i>
                Online Payment Details
            </div>  <!-- training name -->
            <hr>
            <div>
                <div class="h5"><strong>Training Name :</strong> {{$training_confirmations->course_title}}</div>
                {{--<h5 class="" style="font-size: 90%"> {{$training_confirmations->course_title}} </h5>--}}
            </div><!-- training Course -->

                <div class="h5"><strong> Center :</strong>{{$training_confirmations->course_venue}} </div>

            <div>
                {{--<div class="h4">Date : </div>--}}

            </div><!-- training Course -->

                <div class="h5 text-danger"> <strong>Total Cost :</strong> {{$training_confirmations->course_fee}} /=</div>

            <div>
                <div class="h3">Payment Method Select</div>

            </div><!-- training Course -->
            <hr>
            <form action="{{url('/training_confirmations')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group" style="color: darkred;">
                    @foreach ($errors->all() as $message)
                        {{ $message }} <br>
                    @endforeach
                </div>

                <input type="hidden" name="user_id" value="{{$training_confirmations->user_id}}">
                <input type="hidden" name="subject_id" value="{{$training_confirmations->subject_id}}">
                <input type="hidden" name="course_fee" value="{{$training_confirmations->course_fee}}">

                <input type="radio" name="payment_by" value="bKash" id="" required>
                <img src="{{url('frontend/img/payment_img/bkash.png')}}" alt="bKash" height="100" width="120" required>

                <input type="radio" name="payment_by" value="rocket" id="">
                <img src="{{url('frontend/img/payment_img/dbbl_mobile.jpg')}}" alt="rocket" height="100" width="120" required>


                <input type="radio" name="payment_by" value="mastercard" id="">
                <img src="{{url('frontend/img/payment_img/master.jpg')}}" alt="mastercard" height="100" width="120" required>


                <input type="radio" name="payment_by" value="visa" id="">
                <img src="{{url('frontend/img/payment_img/visa.png')}}" alt="visa" height="100" width="120" required>

                <br>

                <input type="radio" name="payment_by" value="mcash" id="">
                <img src="{{url('frontend/img/payment_img/mcash.png')}}" alt="mcash" height="100" width="120" required>

                <input type="radio" name="payment_by" value="dbbl_bank" id="">
                <img src="{{url('frontend/img/payment_img/dbbl_bank.jpg')}}" alt="dbbl_bank" height="100" width="120" required>


                <input type="radio" name="payment_by" value="qcash" id="">
                <img src="{{url('frontend/img/payment_img/qcash.png')}}" alt="qcash" height="100" width="120" required>


                <input type="radio" name="payment_by" value="paypal" id="">
                <img src="{{url('frontend/img/payment_img/paypal.jpg')}}" alt="paypal" height="100" width="120" required>





                <br><br>

                <!-- payment policies and refound policies -->
                <div>
                    <h5><strong>Payment Policies & Rules</strong></h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae consectetur culpa exercitationem id nisi quisquam?</p>


                </div><!-- payment policies -->
                <br>
                <div>
                    <h5><strong>Refunding Rules</strong></h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, nam!</p>
                </div><!-- payment policies -->

                <div align="center">
                    <input type="submit" value="Submit" name="submit" class="btn btn-info btn-block">
                </div>



            </form>

        </div> <!-- panel-default -->

        {{--</div>--}}

    </div> <!-- row close -->




</div>



@include('layouts.footer')

