@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="">
                <div class="panel-heading h3 text-danger" align="center">Student Registration </div>
                <hr>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}" id="form">
                        {{ csrf_field() }}
                        <div class="row">

                        <!-- original form field  -->
                        {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                            {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Register--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                            <!-- original form field close -->

                            <!-- custom form field below -->
                            <div class="row">

                                <div class="form-group col-md-6{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label>First Name *</label>
                                    <input type="text" class="form-control input-sm" name="first_name" placeholder="Enter First Name" value="{{ old('first_name') }}" autofocus>
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group padding_l col-md-6{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label>Last Name *</label>
                                    <input type="text" class="form-control input-sm" name="last_name" value="{{ old('last_name') }}" placeholder="Enter Last Name" autofocus>
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email *</label>
                                    <input type="email" class="form-control input-sm" name="email" value="{{ old('email') }}" placeholder="Enter Mail">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group padding_l col-md-6 {{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label>Phone *</label>
                                    <input type="text" class="form-control input-sm" name="phone" value="{{ old('phone') }}" placeholder="Enter Phone Numer.">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6 {{ $errors->has('interest_course') ? ' has-error' : '' }}">
                                    <label>Area of Interest *</label>
                                    <select name="interest_course"  id="interest_course" class="form-control input-sm">
                                        <option value="">Select</option>
                                        <option value="PHP">PHP</option>
                                        <option value="JAVA">JAVA</option>
                                        <option value="ASP.NET">ASP.NET</option>
                                        <option value="DATABASE">DATABASE</option>
                                        <option value="NETWORKING">NETWORKING</option>
                                        <option value="CMS">CMS</option>
                                    </select>
                                </div>


                                <div class="form-group padding_l col-md-6 {{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <label>Gender *</label>
                                    <select name="gender"  id="gender" class="form-control input-sm">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>

                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Organization </label>
                                    <input type="text" class="form-control input-sm" name="organization" value="{{ old('organization') }}" placeholder="Organization Name">
                                </div>
                                <div class="form-group padding_l col-md-6">
                                    <label>Designation </label>
                                    <input type="text" class="form-control input-sm" name="designation" value="{{ old('designation') }}"  placeholder="Enter Designation">
                                </div>
                                <div class="form-group col-md-6 {{ $errors->has('venue') ? ' has-error' : '' }}">
                                    <label>Interested Location *</label>
                                    <select name="venue"  id="venue" class="form-control input-sm">
                                        <option value="Dhanmondi">Dhanmondi</option>
                                        <option value="Banani">Banani</option>

                                    </select>

                                </div>
                                <div class="form-group col-md-6 conceal" style="visibility: hidden;">

                                    <input type="text" class="form-control input-sm text-hide conceal">

                                </div>

                            </div>
                            <!-- form first row -->


                            <hr>
                            <p class="text-danger">Create Your username and password  to login Datavision</p>
                            <!-- for user registration  field -->
                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                    <label>Username </label>
                                    <input type="text" class="form-control input-sm" name="user_id" value="{{ old('user_id') }}" placeholder="Enter Username">
                                    @if ($errors->has('user_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="padding_l form-group col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password </label>
                                    <input type="password" class="form-control input-sm" name="password" placeholder="Enter Password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label>Confirm Password </label>
                                    <input type="password" name="password_confirmation" id="password-confirm" class="form-control input-sm" placeholder="Confirm Password">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif

                                </div><!-- password-confirm -->




                            {{--<div class=" padding_l form-group col-md-6 g-recaptcha"  data-sitekey="6LeHWDsUAAAAAC5FnEmOwKuLChs0WJUwbwz51ZSD">--}}
                                {{--@if ($errors->has('g-recaptcha-response'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('g-recaptcha-response') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}

                            {{--<div class=" padding_l form-group col-md-6" style="padding-left: 25px;" >--}}
                                {{--<label for="captcha" class="col-md-4 control-label">Captcha</label>--}}
                                {{--<div class="col-md-6 pull-center">--}}
                                    {{--{{ Recaptcha::render() }}--}}
                                    {{--{{('captcha')->display() }}--}}

                                    {{--@if ($errors->has('g-recaptcha-response'))--}}
                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('g-recaptcha-response') }}</strong>--}}
                                    {{--</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}

                            <!-- ====================== -->
                            <!-- alert div for blank field -->
                            {{--<div class="form-group" style="color: darkred;">--}}
                                {{--@foreach ($errors->all() as $message)--}}
                                    {{--{{ $message }}--}}
                                {{--@endforeach--}}
                            {{--</div>--}}

                            <div class="g-recaptcha padding_l col-md-6" data-sitekey="6LeHWDsUAAAAAC5FnEmOwKuLChs0WJUwbwz51ZSD">

                            </div>

                            </div>

                        <div class="form-group col-md-6 col-md-offset-3">
                            <input align="center"  type="submit" value="Submit" name="submit" class="btn btn-info btn-block">
                        </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
