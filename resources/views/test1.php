<div class="col-md-7 ">
    <!-- welcome div its -->
    <div class="row">
        <h3>Welcome to </h3>
        <h1>Datavision Training</h1>

        <p class="fadeInLeft" align="justify">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Consequuntur ex laudantium magnam, perferendis quisquam tempora
            tenetur voluptatem? Ab aliquid animi, autem consequatur corporis
            cupiditate dolor earum error facere facilis hic in labore laborum,
            magnam natus neque non nostrum obcaecati quaerat quasi quibusdam
            quisquam quo quod repellendus repudiandae saepe sapiente.
            Ad assumenda error numquam odit repellendus saepe veniam!
            Eius, ipsum, tenetur? Aliquam aspernatur culpa, doloremque
            error harum iusto, laudantium neque nulla officia quasi
            quidem recusandae rem sed, sit voluptatum? Asperiores assumenda
            consequatur consequuntur, culpa cumque deserunt dignissimos
            explicabo laboriosam necessitatibus quod sunt unde vel,
            voluptatibus. Ducimus non quam voluptas? Iure, totam.
        </p>
        <h5 class="btn btn-info btn-sm">Read more <i class="fa fa-angle-double-right" aria-hidden="true"></i> </h5>


    </div>
    <br>

    <div class="row" style="margin-top: 10px;">


        <div class="col-md-6">
            <div class=" h4" style="padding: 5px 50px;background-color: silver;">Present Courses</div>
            <img src="{{url('frontend/img/about/about1.jpg')}}" class="img-rounded" alt="" height="" width="">
            <br>
            <h4 style="margin-top: 10px;"><a href="#">This is the Business Solution</a></h4>
            <p class="" align="justify">
                Lste magnam molestiae nisi,
                qui quisquam repellendus sunt ut! Aperiam culpa eum magni
                molestias velit?
            </p>

            <h4><a href="#">This is the Businee Solution</a></h4>
            <p class="" align="justify">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Ad aperiam commodi, ea eaque eius, ex excepturi nihil porro
                quae quasi quia quod tempora vitae, voluptas voluptatibus.
                <br> <a href="#" ><i class="fa fa-angle-double-right"> Read more </i></a>


            </p>





        </div>


        <div class="col-md-6">
            <div class=" h4" style="padding: 5px 50px;background-color: silver;">Present Courses</div>
            <img src="{{url('frontend/img/about/about3.jpg')}}" class="img-rounded" alt="" height="" width="">
            <br>
            <h4 style="margin-top: 10px;"><a href="#">This is the Business Solution</a></h4>
            <p class="" align="justify">
                Lste magnam molestiae nisi,
                qui quisquam repellendus sunt ut! Aperiam culpa eum magni
                molestias velit?
            </p>

            <h4><a href="#">This is the Businee Solution</a></h4>
            <p class="" align="justify">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Ad aperiam commodi, ea eaque eius, ex excepturi nihil porro
                quae quasi quia quod tempora vitae, voluptas voluptatibus.
                <br> <a href="#" ><i class="fa fa-angle-double-right"> Read more </i></a>

            </p>





        </div>


    </div>

</div> <!-- left content close here -->

<div class="col-md-4 col-md-offset-1" style="">

    <div class="row " style="background-color: #00A094;padding: 25px;">

        <h3 style="color:white;">Latest News</h3>
        <hr>
        <!-- for news div -->
        <div>
            <p style="color: gold;"><i class="fa fa-envelope-o"></i> November 11, 2007 </p>
            <p style="color: lightgray;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus,
                id incidunt placeat tempore voluptas. <br>
                <a href="#" style="color: white;"> more ... <i class="fa fa-chevron-circle-right"></i></a></p>
        </div>

        <div>
            <p style="color: gold;"><i class="fa fa-envelope-o"></i> November 11, 2007 </p>
            <p style="color: lightgray;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus,
                id incidunt placeat tempore voluptas. <br>
                <a href="#" style="color: white;"> more ... <i class="fa fa-chevron-circle-right"></i></a></p>
        </div>

        <div>
            <p style="color: gold;"><i class="fa fa-envelope-o"></i> November 11, 2007 </p>
            <p style="color: lightgray;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque ducimus,
                id incidunt placeat tempore voluptas. <br>
                <a href="#" style="color: white;"> more ... <i class="fa fa-chevron-circle-right"></i></a></p>
        </div>
        <!-- for new div close -->




    </div>
    <br>
    <!-- news letter start -->
    <div class="row" style="margin-top: 10px; padding: 25px; background-color: white;">
        <h3 style="margin-top: 10px;">Newsletter Sign-up</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus, quod!</p>

        <form action="" class="col-md-12 ">
            <input type="text" placeholder="Enter your mail" name="newsletter" class="form-control input-sm">
            <br>

            <input type="submit" class="btn" name="cancel" value="Unsubscribe">
            <input type="submit"  class="btn btn-primary" name="submit" value="Subscibe">

        </form>

    </div>

</div>
