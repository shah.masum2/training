<div class="wrapper ">
<div class="add_field_button ">

<div class=" trainer_add input_fields_wrap">
<div class="row margin_top">
    <input type="hidden" name="trainer_pinfo_id[]" value="{{$latest_trainer->id}}" id="trainer_pinfo_id" class="trainer_pinfo_id">
    <div class="col-md-2">
        <div class="form-group">
            <label class="control-label">Degree</label>
            <div class="form-group">
                <select name="degree[]" class="form-control degree" required>
                    <option value=""></option>
                    <option value="A'level">SSC/A'level</option>
                    <option value="O'level">HSC/O'level</option>
                    <option value="Diploma">Diploma</option>
                    <option value="Bachelor">Bachelor</option>
                    <option value="Master">Master</option>
                    <option value="Phd">Phd</option>
                    <option value="Mphil">Mphil</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <label class="control-label">Institute</label>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>
                <input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name">
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <label class="control-label">Department/Group</label>
            <div class="form-group">
                <select name="department_group[]" id="department_group" class="form-control department_group">
                    <option value="Please Select Department">Please Select Department</option>
                    <option value="Department Name">Department Name</option>
                    <option value="Department name">Department name</option>
                    <option value="Please Select Department">Please Select Department</option>
                    <option value="Department Name">Department Name</option>
                    <option value="Department name">Department name</option>
                    <option value="Please Select Department">Please Select Department</option>
                    <option value="Department Name">Department Name</option>
                    <option value="Department name">Department name</option>
                    <option value="Please Select Department">Please Select Department</option>
                    <option value="Department Name">Department Name</option>
                    <option value="Department name">Department name</option>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>
            <input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Year</label>
            <div class="form-group">
                <select name="year[]" class="form-control year">
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Board</label>
            <div class="form-group">
                <select name="board[]" class="form-control board">
                    <option value="">Select</option>
                    <option value="Barisal">Barisal</option>
                    <option value="Chittagong">Chittagong</option>
                    <option value="Comilla">Comilla</option>
                    <option value="Dhaka">Dhaka</option>
                    <option value="Jessore">Jessore</option>
                    <option value="Rajshahi">Rajshahi</option>
                    <option value="Sylhet">Sylhet</option>
                    <option value="Dinajpur">Dinajpur</option>
                </select>
            </div>
        </div>
    </div>
    <input type="hidden" name="status[]" value="1" class="status">

</div>
</div>


<div class="row">
<div class="col-md-12">
    <div class="pull-left margin_top margin_bottom"><a href="#" class=" add_field_button">Add More Degree</a></div>
    <div class="pull-right ">
        <button class="btn btn-primary nextBtn margin_top margin_bottom more_degree_btn add_field_button " type="button" >Next</button>
        <input type="hidden" name="status[]" value="1" class="status" >
        <input type="submit" name="submit" value="Next" class="btn btn-primary nextBtn margin_top margin_bottom add_field_button">

    </div>
</div>

<div class="pull-right ">
    <a class="btn btn-primary nextBtn margin_top margin_bottom add_field_button ">Back</a>&nbsp;&nbsp;
    <input type="hidden" name="status" value="1" >
    <input type="submit" name="submit" value="Next" class="btn btn-primary nextBtn margin_top margin_bottom add_field_button">
</div>


</div>
<br>

</div>
</div>
