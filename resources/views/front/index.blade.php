
@include('front.layouts.header')


            <div class="row">

             <!-- image slider start here -->
                <div class="slider-area">
                <div class=""><!-- slider-container -->
                    <div class=""> <!-- slider-wrapper -->
                        <div id="mainSlider" class="nivoSlider">
                            {{--@foreach($school_sliders as $school_slider)--}}


                            {{--<img src="{{ asset('/images/slider/').'/'.$school_slider->img }}"  alt="" title="#htmlcaption1"/>--}}
                            <img src="{{ url('frontend/img/3.jpg')}}"  alt="" title="#htmlcaption1"/>

                            {{--@endforeach--}}
                        </div>



                        <!--Slider caption start-->
                        {{--<div id="htmlcaption1" class="nivo-html-caption">--}}
                        {{--<div class="slider-progress"></div>--}}
                        {{--<div class="container">--}}
                        {{--<div class="main-caption-wrapper">--}}
                        {{--<div class="caption-position1">--}}
                        {{--<div class="caption-text-slide1 cap-style1 text-center">--}}
                        {{--<div class="cap-normal-text wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.8s">--}}
                        {{--<h2>Datavision Training</h2>--}}
                        {{--</div>--}}
                        {{--<div class="cap-bold-text wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.8s">--}}
                        {{--<h2>Start Building Your Strong Career</h2>--}}
                        {{--</div>--}}
                        {{--<div class="cap-small-text small-text-style1 wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.8s">--}}
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati, saepe.</p>--}}
                        {{--</div>--}}
                        {{--<div class="slider-button-area btn-def-black wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.8s">--}}
                        {{--<a href="#">View Details</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div id="htmlcaption2" class="nivo-html-caption">--}}
                        {{--<div class="slider-progress"></div>--}}
                        {{--<div class="container">--}}
                        {{--<div class="main-caption-wrapper">--}}
                        {{--<div class="caption-position2">--}}
                        {{--<div class="caption-text-slide2 cap-style2 text-center">--}}
                        {{--<div class="cap-bold-text wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                        {{--<h2 style="color:orange">Build Better Career</h2>--}}
                        {{--</div>--}}
                        {{--<div class="cap-small-text small-text-style1 wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                        {{--<p>Any successful career starts with good education. Together with us you will have deeper knowledge of the subjects that will be especially useful for you when climbing the career ladder.</p>--}}
                        {{--</div>--}}
                        {{--<div class="slider-button-area btn-def-black wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                        {{--<a href="#">View Details</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div id="htmlcaption3" class="nivo-html-caption">--}}
                        {{--<div class="slider-progress"></div>--}}
                        {{--<div class="container">--}}
                        {{--<div class="main-caption-wrapper">--}}
                        {{--<div class="caption-position3">--}}
                        {{--<div class="caption-text-slide3 cap-style3 text-center">--}}
                        {{--<div class="cap-normal-text wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.0s">--}}
                        {{--<h2>Make future Reliable</h2>--}}
                        {{--</div>--}}
                        {{--<div class="cap-bold-text wow wow fadeInDown" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                        {{--<h2>Make future Reliable</h2>--}}
                        {{--</div>--}}
                        {{--<div class="cap-small-text small-text-style3 wow wow fadeInDown" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                        {{--<p>Any successful career starts with good education. Together with us you will have deeper knowledge of the subjects that will be especially useful for you when climbing the career ladder.</p>--}}
                        {{--</div>--}}
                        {{--<div class="slider-button-area btn-def-black wow fadeInDown" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                        {{--<a href="#">View Details</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
             <!-- image slider close here -->



            </div>
    <!-- col-md-12 close -->


    <!-- another row start here after slide -->


    <!-- course details with left sidebar -->
    <div class="row">
        <div class="h3 text-center">Our Courses</div>

        <!-- left sidebar -->

            <div class="col-md-3">
                <div class="col-md-12 panel-body">
                    <div class="panel bg-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Related Courses</h3>
                        </div>
                        <ul class="list-group">
                            <li href="#" class="list-group-item"><a href="">Industrial Management</a></li>
                            <li href="#" class="list-group-item"><a href="">HR Rules</a></li>
                            <li href="#" class="list-group-item"><a href="">Morbi leo risus</a></li>
                            <li href="#" class="list-group-item"><a href="">Porta ac consectetur ac</a></li>
                            <li href="#" class="list-group-item"><a href="">Vestibulum at eros</a></li>
                        </ul>
                    </div>
                </div>
            </div>


            <!--  -->

            <!-- right main course body -->
            <div class="col-md-9 panel-body">

                <!-- Panel Start here  -->
                <div class="panel panel-body panel-group panel-primary" style="border-color:gray;">
                    <div class=" panel-heading">Course Details</div>
                    {{--@extends('front/courses/index')--}}
                    {{--@extends('front.training_regis.create')--}}
                    {{--@yield('front.courses')--}}
                    {{--@section('allcourse')--}}
                    {{--@php--}}
                    {{--$sl = 0;--}}
                    {{--@endphp--}}
                    {{--@foreach( $courses as $course)--}}

                    {{--<div class="col-md-6">--}}
                    {{--<div class="card">--}}
                    {{--<h3 class="panel heading">PHP & Web Development</h3>--}}
                    {{--<div class="">--}}
                    {{--<h4 class="panel-content">Special title treatment</h4>--}}
                    {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}

                    {{--<a href="{{url('front/courses/'.$course->$id)}}" class="btn btn-primary">Go Details</a>--}}
                    {{--<a href="{{ url('/front/courses/1') }}">Show</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<!--/.Panel-->--}}

                    {{--</div>--}}
                    {{--@endsection--}}




                    <div class="col-md-6">
                        <div class="card">
                            <h3 class="card-header primary-color white-text">Featured</h3>
                            <div class="card-body">
                                <h4 class="card-title">Special title treatment</h4>
                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                <a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>
                            </div>
                        </div>
                        <!--/.Panel-->


                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="card">--}}
                        {{--<h3 class="card-header primary-color white-text">Featured</h3>--}}
                        {{--<div class="card-body">--}}
                        {{--<h4 class="card-title">Special title treatment</h4>--}}
                        {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                        {{--<a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<!--/.Panel-->--}}



                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<div class="card">--}}
                        {{--<h3 class="card-header primary-color white-text">Featured</h3>--}}
                        {{--<div class="card-body">--}}
                        {{--<h4 class="card-title">Special title treatment</h4>--}}
                        {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                        {{--<a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<!--/.Panel-->--}}



                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="card">--}}
                        {{--<h3 class="card-header primary-color white-text">Featured</h3>--}}
                        {{--<div class="card-body">--}}
                        {{--<h4 class="card-title">Special title treatment</h4>--}}
                        {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                        {{--<a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<!--/.Panel-->--}}



                        {{--</div>--}}
                        {{--<div class="col-md-6">--}}
                        {{--<div class="card">--}}
                        {{--<h3 class="card-header primary-color white-text">Featured</h3>--}}
                        {{--<div class="card-body">--}}
                        {{--<h4 class="card-title">Special title treatment</h4>--}}
                        {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                        {{--<a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<!--/.Panel-->--}}



                        {{--</div>--}}

                    </div>
                </div>

            </div>
        </div>


<div class="row">


    <!-- Trainer images container start -->
    <hr class="text-info">

    <div class="row">

        <div class="h3" style="text-align: center; font-family: Helvetica; max-height: 100px;  color: #383734;margin-top: 15px;margin-bottom: 25px;">Trainer Panel</div>

        <div class="col-sm-6 col-md-4 " align="center">
            <div class="card">
                <img class="card-img-top img-rounded" src="{{url('frontend/img/person1.png')}}" height="200" width="250">
                <div class="card-block">
                    <h5 class="text-bold">Atahar Khan</h5>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 text-center" align="center">
            <div class="card text-center">
                <img class="card-img-top img-rounded" src="{{url('frontend/img/person1.png')}}" class="img-rounded" height="200" width="250">
                <div class="card-block">
                    <h4 class="card-title">Tawshif Ahsan Khan</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ipsam iusto sapiente ut veritatis, voluptatem.
                    </p>
                    <div class="meta">
                        <a href="#">Friends</a>
                    </div>
                    <div class="card-text">
                        Tawshif is a web designer living in Bangladesh.
                    </div>
                </div>
                <div class="card-footer">
                    <span class="float-right">Joined in 2013</span>
                    <span><i class=""></i>75 Friends</span>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 " align="center">
            <div class="card">
                <img class="card-img-top img-rounded" src="{{url('frontend/img/person1.png')}}" class="img-rounded" height="200" width="250">
                <p class="card-block text-justify text-center" style="max-width: 150px;">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, minima!
                </p>
            </div>
        </div>
    </div>



    <!-- Trainer images container close -->


</div>






@include('front.layouts.footer')


