{{--@extends('front.courses.index3')--}}



<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Your title</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--<link href="{{ asset('back-end/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <link rel="apple-touch-icon" href="{{ asset('frontend/apple-touch-icon.png') }}">
    <!-- Place favicon.ico in the root directory -->

    <!-- all css here -->
    <!-- bootstrap v3.3.6 css -->
    <!-- for 2nd navigation menu style css -->
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <!-- for first which is comment or hidden now navigation menu style css -->
    <link rel="stylesheet" href="{{ asset('frontend/css/menu.css') }}">


    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('frontend/css/animate.css') }}">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="{{ asset('frontend/css/jquery-ui.min.css') }}">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{ asset('frontend/css/meanmenu.min.css') }}">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.css') }}">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('frontend/style.css') }}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
    <!-- custom css link below -->
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <!-- modernizr css -->
    <script src="{{ asset('frontend/js/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>



<body style="background-color: #F7F2E6;">

<!-- 2nd container for content -->
<div class="container">

    <!-- First container for Logo and help line -->
    <div class="row" style="margin-top: 0px;">
        <div style="margin-top:50px; margin-bottom: 0px;" class="col-md-4"><a href="#" ><img src="{{url('frontend/img/dvt_logo.png')}}"  height="60" width="150" alt=""></a></div>
        <div class="col-md-4"></div>
        <div class="col-md-4" align="right" style="margin-top: 50px; font-size: 120%;">
            <strong> Hotline <br><i class="fa fa-phone"></i>  02-8839587</strong></div>
    </div>
    <!-- header and logo row close here -->



    <div class="row">
        <div class="col-md-12">
            <!-- navigation start here -->
            <nav class="">
                <div class="nav-fostrap">
                    <ul>
                        <li><a href="">Home</a></li>
                        <li><a href="">About Us</a></li>

                        <li><a href="javascript:void(0)">Courses <span class="fa fa-angle-down"> </span></a>
                            <ul class="dropdown">
                                <li><a href="">HTML</a></li>
                                <li><a href="">CSS</a></li>
                                <li><a href="">Javascript</a></li>
                                <li><a href="">JQuery</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" >Training Schedule <span class="fa fa-angle-down"> </span></a>
                            <ul class="dropdown">
                                <li><a href="">Workshop</a></li>
                                <li><a href="">Weekly</a></li>
                                <li><a href="">Monthly</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0)" >Center <span class="fa fa-angle-down"></span></a>
                            <ul class="dropdown">
                                <li><a href="">Tools</a></li>
                                <li><a href="">Backlink</a></li>
                            </ul>
                        </li>
                        <li><a href="">Advertising</a></li>
                        <li><a href="">Contact</a></li>
                    </ul>
                </div>
                <div class="nav-bg-fostrap">
                    <div class="navbar-fostrap"> <span></span> <span></span> <span></span> </div>

                </div>
            </nav>
            <!-- navigation close here -->



            <!-- image slider start here -->
            <div class="slider-area">
                <div class="slider-container">
                    <div class="slider-wrapper">
                        <div id="mainSlider" class="nivoSlider">
                            {{--@foreach($school_sliders as $school_slider)--}}


                            {{--<img src="{{ asset('/images/slider/').'/'.$school_slider->img }}"  alt="" title="#htmlcaption1"/>--}}
                            <img src="{{ url('frontend/img/3.jpg')}}"  alt="" title="#htmlcaption1"/>

                            {{--@endforeach--}}
                        </div>



                        <!--Slider caption start-->
                        {{--<div id="htmlcaption1" class="nivo-html-caption">--}}
                            {{--<div class="slider-progress"></div>--}}
                            {{--<div class="container">--}}
                                {{--<div class="main-caption-wrapper">--}}
                                    {{--<div class="caption-position1">--}}
                                        {{--<div class="caption-text-slide1 cap-style1 text-center">--}}
                                            {{--<div class="cap-normal-text wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.8s">--}}
                                                {{--<h2>Datavision Training</h2>--}}
                                            {{--</div>--}}
                                            {{--<div class="cap-bold-text wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.8s">--}}
                                                {{--<h2>Start Building Your Strong Career</h2>--}}
                                            {{--</div>--}}
                                            {{--<div class="cap-small-text small-text-style1 wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.8s">--}}
                                                {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati, saepe.</p>--}}
                                            {{--</div>--}}
                                            {{--<div class="slider-button-area btn-def-black wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.8s">--}}
                                                {{--<a href="#">View Details</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div id="htmlcaption2" class="nivo-html-caption">--}}
                            {{--<div class="slider-progress"></div>--}}
                            {{--<div class="container">--}}
                                {{--<div class="main-caption-wrapper">--}}
                                    {{--<div class="caption-position2">--}}
                                        {{--<div class="caption-text-slide2 cap-style2 text-center">--}}
                                            {{--<div class="cap-bold-text wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                                                {{--<h2 style="color:orange">Build Better Career</h2>--}}
                                            {{--</div>--}}
                                            {{--<div class="cap-small-text small-text-style1 wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                                                {{--<p>Any successful career starts with good education. Together with us you will have deeper knowledge of the subjects that will be especially useful for you when climbing the career ladder.</p>--}}
                                            {{--</div>--}}
                                            {{--<div class="slider-button-area btn-def-black wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                                                {{--<a href="#">View Details</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div id="htmlcaption3" class="nivo-html-caption">--}}
                            {{--<div class="slider-progress"></div>--}}
                            {{--<div class="container">--}}
                                {{--<div class="main-caption-wrapper">--}}
                                    {{--<div class="caption-position3">--}}
                                        {{--<div class="caption-text-slide3 cap-style3 text-center">--}}
                                            {{--<div class="cap-normal-text wow fadeInDown" data-wow-duration=".9s" data-wow-delay="0.0s">--}}
                                                {{--<h2>Make future Reliable</h2>--}}
                                            {{--</div>--}}
                                            {{--<div class="cap-bold-text wow wow fadeInDown" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                                                {{--<h2>Make future Reliable</h2>--}}
                                            {{--</div>--}}
                                            {{--<div class="cap-small-text small-text-style3 wow wow fadeInDown" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                                                {{--<p>Any successful career starts with good education. Together with us you will have deeper knowledge of the subjects that will be especially useful for you when climbing the career ladder.</p>--}}
                                            {{--</div>--}}
                                            {{--<div class="slider-button-area btn-def-black wow fadeInDown" data-wow-duration="1.2s" data-wow-delay="0.2s">--}}
                                                {{--<a href="#">View Details</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>

            <!-- image slider close here -->



        </div><!-- col-md-12 close -->
    </div><!-- row close -->

    <!-- another row start here after slide -->


    <!-- course details with left sidebar -->
    <div class="row">
        <div class="h3 text-center">Our Courses</div>

        <!-- left sidebar -->
       <div class="col-md-12">
                <div class="col-md-3">
                    <div class="col-md-12 panel-body">
                        <div class="panel bg-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"> Related Courses</h3>
                            </div>
                            <ul class="list-group">
                                <li href="#" class="list-group-item"><a href="">Industrial Management</a></li>
                                <li href="#" class="list-group-item"><a href="">HR Rules</a></li>
                                <li href="#" class="list-group-item"><a href="">Morbi leo risus</a></li>
                                <li href="#" class="list-group-item"><a href="">Porta ac consectetur ac</a></li>
                                <li href="#" class="list-group-item"><a href="">Vestibulum at eros</a></li>
                            </ul>
                        </div>
                    </div>
                </div>


        <!--  -->

        <!-- right main course body -->
        <div class="col-md-9 panel-body">

            <!-- Panel Start here  -->
            <div class="panel panel-body panel-group panel-primary" style="border-color:gray;">
                <div class=" panel-heading">Course Details</div>
                {{--@extends('front/courses/index')--}}
                {{--@extends('front.training_regis.create')--}}
                {{--@yield('front.courses')--}}
                {{--@section('allcourse')--}}
                {{--@php--}}
                {{--$sl = 0;--}}
                {{--@endphp--}}
                 {{--@foreach( $courses as $course)--}}

                {{--<div class="col-md-6">--}}
                    {{--<div class="card">--}}
                        {{--<h3 class="panel heading">PHP & Web Development</h3>--}}
                        {{--<div class="">--}}
                            {{--<h4 class="panel-content">Special title treatment</h4>--}}
                            {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}

                            {{--<a href="{{url('front/courses/'.$course->$id)}}" class="btn btn-primary">Go Details</a>--}}
                            {{--<a href="{{ url('/front/courses/1') }}">Show</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!--/.Panel-->--}}

                {{--</div>--}}
                {{--@endsection--}}




                <div class="col-md-6">
                    <div class="card">
                        <h3 class="card-header primary-color white-text">Featured</h3>
                        <div class="card-body">
                            <h4 class="card-title">Special title treatment</h4>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>
                        </div>
                    </div>
                    <!--/.Panel-->


                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="card">--}}
                        {{--<h3 class="card-header primary-color white-text">Featured</h3>--}}
                        {{--<div class="card-body">--}}
                            {{--<h4 class="card-title">Special title treatment</h4>--}}
                            {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                            {{--<a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!--/.Panel-->--}}



                {{--</div>--}}

                {{--<div class="col-md-6">--}}
                    {{--<div class="card">--}}
                        {{--<h3 class="card-header primary-color white-text">Featured</h3>--}}
                        {{--<div class="card-body">--}}
                            {{--<h4 class="card-title">Special title treatment</h4>--}}
                            {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                            {{--<a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!--/.Panel-->--}}



                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="card">--}}
                        {{--<h3 class="card-header primary-color white-text">Featured</h3>--}}
                        {{--<div class="card-body">--}}
                            {{--<h4 class="card-title">Special title treatment</h4>--}}
                            {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                            {{--<a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!--/.Panel-->--}}



                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<div class="card">--}}
                        {{--<h3 class="card-header primary-color white-text">Featured</h3>--}}
                        {{--<div class="card-body">--}}
                            {{--<h4 class="card-title">Special title treatment</h4>--}}
                            {{--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>--}}
                            {{--<a href="{{url('front/courses/')}}" class="btn btn-primary">Go Details</a>--}}
                         {{--</div>--}}
                    {{--</div>--}}
                    {{--<!--/.Panel-->--}}



                {{--</div>--}}

            </div>
        </div>

       </div>
    </div>


    <!-- Trainer images container start -->
    <hr class="text-info">

        <div class="row">

            <div class="h3" style="text-align: center; font-family: Helvetica; max-height: 100px;  color: #383734;margin-top: 15px;margin-bottom: 25px;">Trainer Panel</div>

            <div class="col-sm-6 col-md-4 " align="center">
                <div class="card">
                    <img class="card-img-top img-rounded" src="{{url('frontend/img/person1.png')}}" height="200" width="250">
                    <div class="card-block">
                        <h5 class="text-bold">Atahar Khan</h5>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 text-center" align="center">
                <div class="card text-center">
                    <img class="card-img-top img-rounded" src="{{url('frontend/img/person1.png')}}" class="img-rounded" height="200" width="250">
                    <div class="card-block">
                        <h4 class="card-title">Tawshif Ahsan Khan</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ipsam iusto sapiente ut veritatis, voluptatem.
                        </p>
                        <div class="meta">
                            <a href="#">Friends</a>
                        </div>
                        <div class="card-text">
                            Tawshif is a web designer living in Bangladesh.
                        </div>
                    </div>
                    <div class="card-footer">
                        <span class="float-right">Joined in 2013</span>
                        <span><i class=""></i>75 Friends</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 " align="center">
                <div class="card">
                    <img class="card-img-top img-rounded" src="{{url('frontend/img/person1.png')}}" class="img-rounded" height="200" width="250">
                    <p class="card-block text-justify text-center" style="max-width: 150px;">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, minima!
                    </p>
                </div>
            </div>
        </div>



    <!-- Trainer images container close -->





</div><!-- container close -->




















<!-- ========================================================= -->
<!-- footer start here -->

<div class="container-fluid" style="margin-top: 10px;">
<div class="">
    <div class="row">
        <div class="col-md-12" style=" background-color: #1a263a; color:grey;margin-top: 10px;padding: 30px;">
            <!-- Contact Us div its -->
            <div class="col-md-4" style="color: white;padding-left: 150px;">
                <h3>Contact Us</h3>
                <ul>
                    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> About Course</a></li>
                    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Terms & Conditions</a></li>
                    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Clients</a></li>
                    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Recent Events</a></li>
                    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Terms & Conditions</a></li>
                </ul>

            </div>

            <!-- social menu icon -->
            <div class="col-md-4" align="center">
                <h3 style="color: #ffffff;">Follow Us on</h3>

                <a href="#"><img src="{{url('frontend/img/social/facebook.png')}}" alt=""></a>
                <a href="#"><img src="{{url('frontend/img/social/twitter.png')}}" alt=""></a>
                <a href="#"><img src="{{url('frontend/img/social/linkedin.png')}}" alt=""></a>
                <a href="#"><img src="{{url('frontend/img/social/google-plus.png')}}" alt=""></a>

            </div>
            <!-- address div -->
            <div class="col-md-4" style="color: #f6f6f6;padding-right: 150px; text">
                <h3>Address</h3>
                <address class="">
                    Address: House # 15/A, Flat#2, Road # 4, Dhanmondi, Dhaka 1207 <br>
                    Phone: 02-9614457



                </address>

            </div>


        </div>
    </div>


</div>
</div>
<div class="container-fluid footer" style="max-height: 10px;" align="center">Copyright &copy; DatavisionTraining
</div>

<!-- main content page close here -->
<!-- javascript
================================================== -->



<!-- ////////////////////////////////////////// -->
<!-- main content page close here -->

<!-- all js here -->
<!-- jquery latest version -->
<script src="{{ asset('frontend/js/vendor/jquery-1.12.0.min.js') }}"></script>
<!-- bootstrap js -->
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<!-- owl.carousel js -->
<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
<!-- meanmenu js -->
<script src="{{ asset('frontend/js/jquery.meanmenu.js') }}"></script>
<!-- jquery-ui js -->
<script src="{{ asset('frontend/js/jquery-ui.min.js') }}"></script>
<!-- wow js -->
<script src="{{ asset('frontend/js/wow.min.js') }}"></script>
<!-- plugins js -->
<script src="{{ asset('frontend/js/plugins.js') }}"></script>
<!-- main js -->
<script src="{{ asset('frontend/js/main.js') }}"></script>

<!-- menu custom jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

<!--custom jquery  -->
<script src="{{ asset('frontend/js/index.js') }}"></script>
<!--custom jquery  -->
<script src="{{ asset('frontend/js/custom.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.js') }}"></script>
</body>
</html>
