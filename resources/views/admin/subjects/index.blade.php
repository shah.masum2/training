

@extends('admin.layouts.master')


@section('content')

    <i class="fa fa-table"></i> All Courses</div>

    <div class="card-body ">
        <!-- card body start that means main body here -->
        @if(session()->has('message'))
            {{ session('message') }}
        @endif


        <div class="table-responsive">
            <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>SL.</th>
                    <th>Course Title</th>
                    <th>Module</th>
                    <th>Course Start Date</th>
                    <th>Deadline</th>
                    <th>Time</th>
                    <th>Action</th>
                </tr>
                </thead>
                {{--<tfoot></tfoot>--}}
                <tbody>

                @php
                    $sl = 0;
                @endphp
                @foreach($subjects as $subject)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $subject->course_title }}</td>
                        <td>{{ $subject->course_module_type}}</td>
                        <td>{{ $subject->course_date }}</td>
                        <td>{{ $subject->course_deadline }}</td>
                        <td>{{ $subject->course_class_time }}</td>
                    {{--<td>{{ $subject->course_deadline }}</td>--}}

                        <!-- for full details to view -->
                        <td>
                            <a href="{{url('/admin/subjects/'.$subject->id)}}" class="btn btn-info btn-sm">View</a>

                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
        <!-- main body close -->
    </div>



@endsection




