

@extends('admin.layouts.master')


@section('content')

    <i class="fa fa-table"></i> Add New Course </div>


    <div class="card-body ">
        <div id="reportArea">
            @if(session()->has('message'))
                <div class="alert alert-danger">{{ session('message') }}</div>
            @endif

            <form action="{{ url('/admin/subjects') }}" method="post" data-toggle="validator" id="registration_form" enctype="multipart/form-data" role="form">
                {{ csrf_field() }}

                <div class="row">

                    <div class="form-group col-md-5">
                        <label class="control-label col-sm-12" for=""> Course Category :</label>
                        <div class="col-sm-12">
                            <select name="course_category_id" id="" class="form-control input-sm" data-error="Please select Course Catagory" required>
                                <option value="">Select</option>
                                <option value="Web Application">Web Application</option>
                                <option value="Graphics">Graphics</option>
                                <option value="3D Animation">3D Animation</option>
                                <option value="IT Training">IT Training</option>
                                <option value="HR Management">HR Management</option>
                                <option value="Event Management">Event Management</option>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-5">
                        <label class="control-label col-sm-12" for=""> Module Type :</label>
                        <div class="col-sm-12">
                            <select name="course_module_type" id="course_module_type" class="form-control input-sm" data-error="Please select Module Type" required>
                                <option value="">Select</option>
                                <option value="Workshop">Workshop</option>
                                <option value="Weekly Course">Weekly Course</option>
                                <option value="Certificate Course">Certificate Course</option>
                            </select>

                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="">Course Title:</label>
                    <div class="col-sm-10">
                        <input type="text"  name="course_title"  class="form-control input-sm" placeholder="" data-error="Please write Course Title" required>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="row">
                    <div class="form-group col-md-5">
                        <label class="control-label col-sm-12" for="">Course Image:</label>
                        <div class="col-sm-12">
                            <input type="file"  id="course_image" name="course_image" class="form-control input-sm" data-error="This field is required" required>
                            <span class="text-danger">Image size must be 1200 * 400</span>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-5">
                        <label class="control-label col-sm-12" for="">Start Date</label>
                        <div class="col-sm-12">
                            <input type="date"  name="course_date" class="form-control input-sm" placeholder="" data-error="Please select Start Date" required>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                </div>

                <div class="row">
                  <div class="col-md-10">
                    <div class="row">

                        <div class="form-group col-md-4">
                            <label class="control-label col-sm-12" for="">Time:</label>
                            <div class="col-sm-12">
                                <input type="time"  name="course_class_time" class="form-control input-sm" placeholder="" data-error="This field is required" required>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label col-sm-12" for="">Course Fee </label>
                            <input type="text" name="course_fee" id="" class="form-control input-sm" placeholder="Enter Course Fee" data-error="Please write Course Fee" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-4">
                            <label class="control-label col-sm-12" for="">Number of Classes </label>
                            <div class="col-sm-12">
                                <input type="number"  name="course_total_classes" class="form-control input-sm" placeholder="" data-error="Please select Module Type" required>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
               </div>

                <div class="form-group ">
                    <label class="control-label col-sm-2" for="">Total Hours </label>
                    <div class="col-sm-10">
                        <input type="text"  name="course_total_duration"  class="form-control input-sm" placeholder="Enter Total Hours" data-error="This field is required" required>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="row">
                    <div class="col-md-10">
                    <div class="row">

                    <div class="form-group col-md-4">
                        <label class="control-label col-sm-12" for="">Select Venue </label>
                        <div class="col-sm-12">
                            <select name="course_venue" id="course_venue" class="form-control input-sm" data-error="Please select Venue" required>
                                <option value="">Select</option>
                                <option value="Dhanmondi">Dhanmondi</option>
                                <option value="Banani">Banani</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-sm-12" for="">Course Type:</label>
                        <div class="col-sm-12">
                            <select name="course_type" id="course_type" class="form-control input-sm" data-error="Please select Course Type" required>
                                <option value="">Select</option>
                                <option value="Web Development">Web Development</option>
                                <option value="Graphics">Graphics</option>
                                <option value="HR Management">HR Management</option>
                                <option value="Database">Database</option>
                                <option value="Database">Database</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label col-sm-12" for="">Course Deadline </label>
                        <div class="col-sm-12">
                            <input type="date"  name="course_deadline" class="form-control input-sm" placeholder="" data-error="Please provide Course Deadline" required>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    </div>
                 </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="">Course About :</label>
                    <div class="col-sm-10">
                        <textarea name="course_about"   class="form-control input-sm"  rows="2" data-error="This field is required" required></textarea>
                    </div>
                    <div class="help-block with-errors"></div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_about');
                    </script>
                    <!-- ckeditor close -->

                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="">Course Object:</label>
                    <div class="col-sm-10">
                        <textarea name="course_object" placeholder="Write About Object and use comma(,) for line break" class="form-control input-sm input-sm"  rows="2" data-error="This field is required" required></textarea>
                    </div>
                    <div class="help-block with-errors"></div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_object');
                    </script>
                    <!-- ckeditor close -->

                </div>


                <div class="form-group">
                    <label class="control-label col-sm-2" for="">Others Information:</label>
                    <div class="col-sm-10">
                        <textarea name="course_others" id="course_others"  class="form-control input-sm"  rows="2" data-error="This field is required" required></textarea>

                    </div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_others');
                    </script>
                    <!-- ckeditor close -->
                    <div class="help-block with-errors"></div>

                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="">Who will Attend :</label>
                    <div class="col-sm-10">
                        <textarea  name="course_who_attend" id="course_who_attend"  class="form-control input-sm" placeholder="Enter the eligibility" rows="2" data-error="This field is required" required></textarea>
                    </div>
                    <div class="help-block with-errors"></div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_who_attend');
                    </script>
                    <!-- ckeditor close -->
                </div>


                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="submit" name="submit" class="btn btn-info btn-block" value="Submit">
                    </div>
                </div>



            </form>






        </div>
    </div>

@endsection

