

@extends('admin.layouts.master')


@section('content')
    @if(session()->has('message'))
        {{ session('message') }}
    @endif
    <i class="fa fa-table"></i> Update Course Information
    </div>


    <div class="card-body">
        <div id="reportArea">

            {!! Form::open(['url' => '/admin/subjects/'.$subjects->id,'method'=>'patch','file'=>'true']) !!}
            {{--<form action="{{ url('/admin/subjects/'.$subjects->id) }}" method="post" enctype="multipart/form-data" >--}}
            {{ csrf_field() }}

                <div class="form-group">
                    {!! Form::label('Course Category') !!}
                    {{--<label class="control-label col-sm-2" for=""> Course Category :</label>--}}
                    <div class="col-sm-10">
                        {{--<select value="{{$subjects->course_category_id }}" name="course_category_id" id="" class="form-control input-sm">--}}
                            {{--<option value="IT Training">IT Training</option>--}}
                            {{--<option value="IT Training">IT Training2</option>--}}
                            {{--<option value="IT Training">IT Training3</option>--}}
                            {{--<option value="IT Training">IT Training4</option>--}}
                            {{--<option value="IT Training">IT Training5</option>--}}
                            {{--<option value="IT Training">IT Training6</option>--}}
                        {{--</select>--}}
                        {!! Form::text('course_category_id',$subjects->course_category_id,['class'=>'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course Title') !!}
                    {{--<label class="control-label col-sm-2" for="">Course Title:</label>--}}
                    <div class="col-sm-10">
                        {!! Form::text('course_title',$subjects->course_title,['class'=>'form-control input-sm']) !!}
                        {{--<input type="text"  value="{{$subjects->course_title }}" name="course_title"  class="form-control input-sm" placeholder="">--}}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course Image') !!}
                    {{--<label class="control-label col-sm-2" for="">Course Image:</label>--}}
                    <div class="col-sm-10">
                        {!! Form::text('course_image',$subjects->course_image,['class'=>'form-control input-sm']) !!}
                        {{--<input type="file"  id="course_image" value="{{$subjects->course_image }}" name="course_image" class="form-control input-sm">--}}
                        <span class="text-danger">Image size must be 1200 * 400</span>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Start Date') !!}
                    {{--<label class="control-label col-sm-2" for="">Start Date</label>--}}
                    <div class="col-sm-10">
                        {!! Form::text('course_date',$subjects->course_date,['class'=>'form-control input-sm']) !!}
                        {{--<input type="date"  value="{{$subjects->course_date }}" name="course_date" class="form-control input-sm" placeholder="">--}}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Time') !!}
                    {{--<label class="control-label col-sm-2" for="">Time:</label>--}}
                    <div class="col-sm-10">
                        {!! Form::time('course_class_time',$subjects->course_class_time,['class'=>'form-control input-sm']) !!}
                        {{--<input type="time"  value="{{$subjects->course_class_time }}" name="course_class_time" class="form-control input-sm" placeholder="">--}}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course Type') !!}
                    {{--<label class="control-label col-sm-2" for="">Course Type:</label>--}}
                    <div class="col-sm-10">
                        {!! Form::text('course_type',$subjects->course_type,['class'=>'form-control input-sm']) !!}
                        {{--<select value="{{$subjects->course_type }}" name="course_type" id="course_type" class="form-control input-sm">--}}
                            {{--<option value="Web Development">Web Development</option>--}}
                            {{--<option value="Graphics">Graphics</option>--}}
                            {{--<option value="HR Management">HR Management</option>--}}
                            {{--<option value="Database">Database</option>--}}
                            {{--<option value="Database">Database</option>--}}
                        {{--</select>--}}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course Deadline') !!}
                    {{--<label class="control-label col-sm-2" for="">Course Deadline </label>--}}
                    <div class="col-sm-10">
                        {!! Form::date('course_deadline',$subjects->course_deadline,['class'=>'form-control input-sm']) !!}
                        {{--<input type="date"  value="{{$subjects->course_deadline }}" name="course_deadline" class="form-control input-sm" placeholder="">--}}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course About') !!}
                    {{--<label class="control-label col-sm-2" for="">Course About :</label>--}}
                    <div class="col-sm-10">
                        {!! Form::textarea('course_about',$subjects->course_about,['class'=>'form-control input-sm']) !!}
                        {{--<textarea  name="course_about"   class="form-control input-sm"  rows="2">{{$subjects->course_about }}</textarea>--}}
                    </div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_about');
                    </script>
                    <!-- ckeditor close -->

                </div>
                <div class="form-group">
                    {!! Form::label('Course Object') !!}
                    {{--<label class="control-label col-sm-2" for="">Course Object:</label>--}}
                    <div class="col-sm-10">
                        {!! Form::textarea('course_object',$subjects->course_object,['class'=>'form-control input-sm']) !!}
                        {{--<textarea  name="course_object"  class="form-control input-sm input-sm"  rows="2">{{$subjects->course_object }}</textarea>--}}
                    </div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_object');
                    </script>
                    <!-- ckeditor close -->

                </div>
                <div class="form-group">
                    {!! Form::label('Others Information') !!}
                    {{--<label class="control-label col-sm-2" for="">Others Information:</label>--}}
                    <div class="col-sm-10">
                        {!! Form::textarea('course_others',$subjects->course_others,['class'=>'form-control input-sm']) !!}
                        {{--<textarea value="" name="course_others" id="course_others"  class="form-control input-sm"  rows="2">{{$subjects->course_others }}</textarea>--}}

                    </div>

                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_others');
                    </script>
                    <!-- ckeditor close -->

                </div>

                <div class="form-group">
                    {!! Form::label('Who will Attend ') !!}
                    {{--<label class="control-label col-sm-2" for="">Who will Attend :</label>--}}
                    <div class="col-sm-10">
                        {!! Form::textarea('course_who_attend',$subjects->course_who_attend,['class'=>'form-control input-sm']) !!}
                        {{--<input type="text" value="{{$subjects->course_who_attend }}" name="course_who_attend"  class="form-control input-sm" placeholder="Enter the eligibility" >--}}
                    </div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_who_attend');
                    </script>
                    <!-- ckeditor close -->
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        {{--<input type="submit"  name="submit" class="btn btn-info" value="Submit">--}}
                        {!! Form::submit('submit',['class'=>'btn btn-info']) !!}
                    </div>
                </div>


            {!! Form::close() !!}
            {{--</form>--}}






        </div>
    </div>

@endsection

