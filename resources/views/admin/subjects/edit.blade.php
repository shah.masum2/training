

@extends('admin.layouts.master')


@section('content')

    <i class="fa fa-book"></i>  Update Course Information </div>


<div class="card-body ">
    <div id="reportArea">
        @if(session()->has('message'))
            <p class="text-danger"><b>{{ session('message') }}</b></p>
        @endif

  {!! Form::open(['url' => '/admin/subjects/'.$subjects->id,'method'=>'patch','data-toggle'=>'validator','id'=>'registration_form','files'=>true ]) !!}

    {{--<form action="{{ url('admin/subjects/update',$subjects->id) }}" data-toggle="validator" id="registration_form" method="post" enctype="multipart/form-data" role="form">--}}
    {{ csrf_field() }}

  <div class="row">

      <div class="form-group col-md-5">
          <label class="control-label col-sm-12" for=""> Course Category :</label>
          <div class="col-sm-12">
              <select name="course_category_id" id="" class="form-control input-sm" data-error="Please select Course Catagory" required>
                  <option @php echo(($subjects->course_category_id)==='Web Application')?'selected':''; @endphp value="Web Application">Web Application</option>
                  <option @php echo(($subjects->course_category_id)==='Graphics')?'selected':''; @endphp value="Graphics">Graphics</option>
                  <option @php echo(($subjects->course_category_id)==='HR Management')?'selected':''; @endphp value="HR Management">HR Management</option>
                  <option @php echo(($subjects->course_category_id)==='IT Training')?'selected':''; @endphp value="IT Training">IT Training</option>
                  <option @php echo(($subjects->course_category_id)==='Event Management')?'selected':''; @endphp value="Event Management">Event Management</option>
              </select>
          </div>
          <div class="help-block with-errors"></div>
      </div>
      <div class="form-group col-md-5">
          <label class="control-label col-sm-12" for=""> Module Type :</label>
          <div class="col-sm-12">
              <select name="course_module_type" id="course_module_type" class="form-control input-sm" data-error="Please select Module Type" required>

                  <option @php echo(($subjects->course_module_type)==='Workshop')?'selected':''; @endphp value="Workshop">Workshop</option>
                  <option @php echo(($subjects->course_module_type)==='Weekly Course')?'selected':''; @endphp value="Weekly Course">Weekly Course</option>
                  <option @php echo(($subjects->course_module_type)==='Certificate Course')?'selected':''; @endphp value="Certificate Course">Certificate Course</option>


              </select>
          </div>
          <div class="help-block with-errors"></div>
      </div>

  </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="">Course Title:</label>
        <div class="col-sm-10">
            <input type="text"  name="course_title" value="{{$subjects->course_title}}"  class="form-control input-sm" data-error="Please write Course Title" required >
        </div>
        <div class="help-block with-errors"></div>
    </div>
   <div class="row">
       <div class="form-group col-md-5">
           <label class="control-label col-sm-12" for="">Course Image:</label>
           <div class="col-sm-12">
               <input type="file"  id="course_image" name="course_image" class="form-control input-sm" data-error="This field is required" required>
               <span class="text-danger">Image size must be 1200 * 400</span>
           </div>
           <div class="help-block with-errors"></div>
       </div>
       <div class="form-group col-md-5">
           <label class="control-label col-sm-12" for="">Start Date</label>
           <div class="col-sm-12">
               <input type="date"  name="course_date" value="{{$subjects->course_date}}" class="form-control input-sm" data-error="Please select Start Date" required>
           </div>
           <div class="help-block with-errors"></div>
       </div>

   </div>

  <div class="row">
      <div class="col-md-10">
          <div class="row">

              <div class="form-group col-md-4">
                  <label class="control-label col-sm-12" for="">Time:</label>
                  <div class="col-sm-12">
                      <input type="time"  name="course_class_time" value="{{$subjects->course_class_time}}" class="form-control input-sm" data-error="This field is required" required>
                  </div>
                  <div class="help-block with-errors"></div>
              </div>


              <div class="form-group col-md-4">
                  <label class="control-label col-sm-12" for="">Course Fee </label>
                  <div class="col-sm-12">
                    <input type="text" name="course_fee" value="{{$subjects->course_fee}}" class="form-control input-sm" placeholder="Enter Course Fee">
                  </div>
                  <div class="help-block with-errors"></div>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label col-sm-12" for="">Number of Classes </label>
                  <div class="col-sm-12">
                      <input type="number"  name="course_total_classes" value="{{$subjects->course_total_classes}}" class="form-control input-sm" data-error="Please select Module Type" required>
                  </div>
                  <div class="help-block with-errors"></div>
              </div>


          </div>
      </div>
  </div>

    <div class="form-group ">
        <label class="control-label col-sm-2" for="">Total Hours </label>
        <div class="col-sm-10">
            <input type="number"  name="course_total_duration" value="{{$subjects->course_total_duration}}"  class="form-control input-sm" placeholder="Enter Total Hours">
        </div>
        <div class="help-block with-errors"></div>
    </div>

    <div class="row">
    <div class="col-md-10">
    <div class="row ">

        <div class="form-group col-md-4">
            <label class="control-label col-sm-12" for="">Select Venue </label>
            <div class="col-sm-12">
                <select name="course_venue" id="course_venue" class="form-control input-sm" data-error="Please select Venue" required>
                    <option @php echo(($subjects->course_venue)==='Dhanmondi')?'selected':''; @endphp value="Dhanmondi">Dhanmondi</option>
                    <option @php echo(($subjects->course_venue)==='Banani')?'selected':''; @endphp value="Banani">Banani</option>

                </select>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group col-md-4">
            <label class="control-label col-sm-12" for="">Course Type:</label>
            <div class="col-sm-12">
                <select name="course_type" id="course_type" class="form-control input-sm" data-error="Please select Course Type" required>
                    <option @php echo(($subjects->course_type)==='Web Application')?'selected':''; @endphp value="Web Application">Web Application</option>
                    <option @php echo(($subjects->course_type)==='Graphics')?'selected':''; @endphp value="Graphics">Graphics</option>
                    <option @php echo(($subjects->course_type)==='HR Management')?'selected':''; @endphp value="HR Management">HR Management</option>
                    <option @php echo(($subjects->course_type)==='IT Training')?'selected':''; @endphp value="IT Training">IT Training</option>
                    <option @php echo(($subjects->course_type)==='Event Management')?'selected':''; @endphp value="Event Management">Event Management</option>
                </select>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group col-md-4">
            <label class="control-label col-sm-12" for="">Course Deadline </label>
            <div class="col-sm-12">
                <input type="date"  name="course_deadline" value="{{$subjects->course_deadline}}" class="form-control input-sm" data-error="Please provide Course Deadline" required>
            </div>
            <div class="help-block with-errors"></div>
        </div>

    </div>
    </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="">Course About :</label>
        <div class="col-sm-10">
            <textarea name="course_about"   class="form-control input-sm"  rows="2" data-error="This field is required" required>{{$subjects->course_about}}</textarea>
        </div>
        <!-- ckeditor start -->
        <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
        <script>
            CKEDITOR.replace('course_about');
        </script>
        <!-- ckeditor close -->
        <div class="help-block with-errors"></div>

    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="">Course Object:</label>
        <div class="col-sm-10">
            <textarea name="course_object" placeholder="" class="form-control input-sm input-sm"  rows="2" data-error="This field is required" required>{{$subjects->course_object}}</textarea>
        </div>
        <!-- ckeditor start -->
        <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
        <script>
            CKEDITOR.replace('course_object');
        </script>
        <!-- ckeditor close -->
        <div class="help-block with-errors"></div>

    </div>


    <div class="form-group">
        <label class="control-label col-sm-2" for="">Others Information:</label>
        <div class="col-sm-10">
            <textarea name="course_others" id="course_others"  class="form-control input-sm"  rows="2" data-error="This field is required" required> {{$subjects->course_others}} </textarea>

        </div>

        <!-- ckeditor start -->
        <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
        <script>
            CKEDITOR.replace('course_others');
        </script>
        <!-- ckeditor close -->
        <div class="help-block with-errors"></div>

    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="">Who will Attend :</label>
        <div class="col-sm-10">
            <textarea  name="course_who_attend" id="course_who_attend"  class="form-control input-sm" placeholder="Enter the eligibility" rows="2" data-error="This field is required" required>{{$subjects->course_who_attend}} </textarea>
        </div>
        <!-- ckeditor start -->
        <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
        <script>
            CKEDITOR.replace('course_who_attend');
        </script>
        <!-- ckeditor close -->
        <div class="help-block with-errors"></div>
    </div>


    <div class="row ">

        <div class="col-md-5">
            <a href="{{url('admin/subjects/')}}" name="cancel" class="btn  btn-outline-warning btn-block" value="">Cancel</a>
        </div>

        <div class="col-md-5">
            <input type="submit" name="submit" class="btn btn-info btn-block" value="Update">
        </div>

    </div>



{{--</form>--}}
            {!! Form::close() !!}
{{--</form>--}}






</div>
</div>

@endsection

