@extends('admin.layouts.master')


@section('content')
    @if(session()->has('message'))
        {{ session('message') }}
    @endif

    <i class="fa fa-table"></i> View Course Details </div>

    <div class="card-body ">
        <!-- card body start that means main body here -->
        <p align="right">
            <a href="{{url('/admin/subjects/'.$subjects->id.'/edit')}}" class=" right btn btn-outline-warning  btn-sm">  Edit Course </a>
        </p>
        <div class="col-md-9">
            <div class="margin_bottom margin_top">
                <div class="row course_summury_left_Padding">
                    <div class="col-md-6 text-left course_summury_left_margin"><h4>{{$subjects->course_title }}</h4></div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="row course_bg_highlight">
                            <div class="course_summury_left_Padding margin_top " style="background-color: lightblue">
                                <img src="{{ asset('images/'.$subjects->course_image) }}" height="400" width="1000" class="img-responsive "><br>

                                <div style="padding: 10px;">
                                    {{--<h6><b>Date:</b> {{ $subjects->course_fee }}</h6>--}}
                                    <h6><b>Date:</b> {{$subjects->course_date}}</h6>
                                    <h6><b>Duration:</b> Time ({{$subjects->course_class_time}})</h6>
                                    <h6><b>No of Classes:</b> {{$subjects->course_total_classes}}</h6>
                                    <h6><b>Last Date of Registration:</b> {{$subjects->course_deadline}}</h6>
                                    <h6><b>Venue:</b> {{$subjects->course_venue}}</h6><br>
                                </div>
                            </div>
                        </div>

                            <h5 class="margin_top">About Course</h5>
                                   {!! $subjects->course_about !!}

                            <br>
                            <h5 class="margin_top">Course Objects</h5>
                            {!! $subjects->course_object !!}

                            {{--<p><i class="fa fa-check-square-o" aria-hidden="true"></i> Learn how to make responsive website.</p>--}}

                            <h5 class="margin_top">Who will Attend</h5><!-- course_who_attend  -->
                            <p> {!! $subjects->course_who_attend !!}</p>
                            {{--<p><i class="fa fa-clone" aria-hidden="true"></i> Learn how to give interactive feature of your website.</p>--}}
                        </div>
                    </div>
                </div>


            <p align="left">
                <a href="{{url('/admin/subjects')}}" class=" right btn btn-outline-info  btn-sm">  Return Course  </a>
                <a href="{{url('/admin/subjects/'.$subjects->id.'/edit')}}" class=" right btn btn-outline-warning  btn-sm">  Edit Course  </a>

            </p>
        </div>
        </div>






@endsection



