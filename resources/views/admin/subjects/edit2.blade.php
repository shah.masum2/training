

@extends('admin.layouts.master')


@section('content')
    @if(session()->has('message'))
        {{ session('message') }}
    @endif
    <i class="fa fa-table"></i> Update Course Information
    </div>


    <div class="card-body">
        <div id="reportArea">

            {!! Form::open(['url' => '/admin/subjects/'.$subjects->id,'method'=>'patch','files'=>true ]) !!}
            {{--<form action="{{ url('/admin/subjects/'.$subjects->id) }}" method="post" enctype="multipart/form-data" >--}}
            {{ csrf_field() }}
                <div class="form-group">
                    {!! Form::label('Course Category') !!}
                    <div class="col-sm-10">
                        {{--<select value="{{$subjects->course_category_id }}" name="course_category_id" id="" class="form-control input-sm">--}}
                            {{--<option value="IT Training">IT Training</option>--}}
                            {{--<option value="IT Training">IT Training2</option>--}}
                            {{--<option value="IT Training">IT Training3</option>--}}
                            {{--<option value="IT Training">IT Training4</option>--}}
                            {{--<option value="IT Training">IT Training5</option>--}}
                            {{--<option value="IT Training">IT Training6</option>--}}
                        {{--</select>--}}
                        {!! Form::text('course_category_id',$subjects->course_category_id,['class'=>'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course Title') !!}
                    <div class="col-sm-10">
                        {!! Form::text('course_title',$subjects->course_title,['class'=>'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course Image') !!}
                    <div class="col-sm-10">
                        {!! Form::file('course_image') !!} <!-- ,['class'=>'form-control input-sm'] -->
                        <span class="text-danger">Image size must be 1200 * 400</span>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Start Date') !!}
                    <div class="col-sm-10">
                        {!! Form::text('course_date',$subjects->course_date,['class'=>'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Time') !!}
                    <div class="col-sm-10">
                        {!! Form::time('course_class_time',$subjects->course_class_time,['class'=>'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course Type') !!}
                    <div class="col-sm-10">
                        {!! Form::text('course_type',$subjects->course_type,['class'=>'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course Deadline') !!}
                    <div class="col-sm-10">
                        {!! Form::date('course_deadline',$subjects->course_deadline,['class'=>'form-control input-sm']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Course About') !!}
                    <div class="col-sm-10">
                        {!! Form::textarea('course_about',$subjects->course_about,['class'=>'form-control input-sm']) !!}
                    </div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_about');
                    </script>
                    <!-- ckeditor close -->

                </div>
                <div class="form-group">
                    {!! Form::label('Course Object') !!}
                    <div class="col-sm-10">
                        {!! Form::textarea('course_object',$subjects->course_object,['class'=>'form-control input-sm']) !!}
                    </div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_object');
                    </script>
                    <!-- ckeditor close -->

                </div>
                <div class="form-group">
                    {!! Form::label('Others Information') !!}
                    <div class="col-sm-10">
                        {!! Form::textarea('course_others',$subjects->course_others,['class'=>'form-control input-sm']) !!}
                    </div>

                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_others');
                    </script>
                    <!-- ckeditor close -->

                </div>

                <div class="form-group">
                    {!! Form::label('Who will Attend ') !!}
                    <div class="col-sm-10">
                        {!! Form::textarea('course_who_attend',$subjects->course_who_attend,['class'=>'form-control input-sm']) !!}
                    </div>
                    <!-- ckeditor start -->
                    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
                    <script>
                        CKEDITOR.replace('course_who_attend');
                    </script>
                    <!-- ckeditor close -->
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        {!! Form::submit('Update',['class'=>'btn btn-info']) !!}
                    </div>
                </div>


            {!! Form::close() !!}
            {{--</form>--}}






        </div>
    </div>

@endsection

