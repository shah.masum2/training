{{--{{$mentors}}--}}


{{--{{$applicants}}--}}

@extends('admin.layouts.master')

@section('content')

    <i class="fa fa-table"></i> Registered Trainers </div>

    <div class="card-body ">
        <!-- card body start that means main body here -->

        @if(session()->has('message'))
            {{ session('message') }}
        @endif

        <div class="table-responsive">
            <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">



                <thead>
                <tr>
                    <th>SL.</th>
                    <th>Name</th>
                    <th>Faculty</th>
                    <th>Phone</th>
                    <th>Last Degree</th>
                    <th>Previous Org.</th>
                    <th>Photo.</th>
                    <th align="center">Action</th>
                </tr>
                </thead>
                <tfoot></tfoot>
                <tbody>

                @php
                    $sl = 0;
                @endphp
                @foreach($mentors as $mentor)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $mentor->name }}</td>
                        <td>{{ $mentor->interest_course }}</td>
                        <td>{{ $mentor->mobile }}</td>
                        {{--<td>{{ $mentor->religion }}</td>--}}
                        {{--<td>{{@$regis->paybkash->transaction_id }}</td>--}}
                        <td>{{ @$mentor->trainer_edu->institute }}</td>
                        <td>{{ @$mentor->trainer_exp->com_name }}</td>
                        <td><img src="{{ asset('images/'.$mentor->photo) }}" height="100" width="100" class="img-responsive "></td>

                        <!-- for full details to view -->
                        <td>
                            <a href="{{url('/admin/mentors/'.$mentor->id)}}" class="btn btn-info btn-sm">View Resume</a>

                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
        <!-- main body close -->
    </div>



@endsection

