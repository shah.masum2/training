

@extends('admin.layouts.master')

@section('content')


    <div class="row" style="color: black; background-color: white;">
        <!-- Breadcrumbs-->


            <div class="col-12">
                <h3 align="center" style="margin-top: 10px;margin-bottom: 10px;">Resume</h3>
                <hr>

                <div class="container margin_top margin_bottom" style="margin-top: 10px;margin-bottom: 50px;">
                    <div class="row trainer_info margin_top margin_bottom">
                        <div class="col-md-8 trainerinfo_left">
                            <div class="row trainer_about margin_top margin_bottom">
                                <div class="col-md-2">
                                    <img src="{{url(asset('images/'.$details->photo))}}" height="100" width="100" class="img-responsive">
                                </div>
                                <div class="col-md-5">
                                    <table>
                                        <tr>
                                            <td>Name</td>
                                            <td>:<b> {{$details->name}}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            @php
                                                $sl =0;
                                            @endphp

                                            @foreach ($accs as $row)

                                            <td>:@php ++$sl @endphp {{$row->email}}</td>
                                                @endforeach
                                        </tr>
                                        <tr>
                                            <td>Mobil</td>
                                            <td>: {{$details->mobile}}</td>
                                        </tr>
                                        <tr>
                                            <td>Date of Birth</td>
                                            <td>: {{$details->date_of_birth}}</td>
                                        </tr>
                                        <tr>
                                            <td>Gender</td>
                                            <td>: {{$details->gender}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-5">
                                    <table>
                                        <tr>
                                            <td>Religion</td>
                                            <td>: {{$details->religion}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nationality</td>
                                            <td>: {{$details->nationality}}</td>
                                        </tr>
                                        <tr>
                                            <td>Marital Status</td>
                                            <td>: {{$details->marital_status}}</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td>: {{$details->address}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <div class="row course_status margin_top margin_bottom">
                                <h4 class="h4" style="">COURSE STATUS</h4> <hr/><br>

                                <div class="table table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Web Design</td>
                                            <td>1/1/2017</td>
                                            <td>1/1/2018</td>
                                            <td>Running</td>
                                        </tr>
                                        <tr>
                                            <td>Web Design</td>
                                            <td>1/1/2017</td>
                                            <td>1/1/2018</td>
                                            <td>Completed</td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-4 trainerinfo_right" style="padding-left: 60px;">
                            <div class="education">
                                <div class="row">
                                    <h3>EDUCATION</h3><hr/>
                                </div>

                                <div class="row">
                                    <table>
                                        <!-- Original Loop -->
                                        @php
                                            $sl =0;
                                        @endphp

                                        @foreach ($educations as $row)
                                            <input type="hidden" value=" {{++$sl}}" name="">
                                        <tbody>

                                        <h5>Degree : <b>{{ $row->degree }}</b></h5>
                                        <tr>
                                            <td>Institute</td>
                                            <td>: {{ $row->institute }} </td><br>
                                        </tr>
                                        <tr>
                                            <td>Department</td>
                                            <td>: {{ $row->department_group }}</td><br>
                                        </tr>
                                        <tr>
                                            <td>Result</td>
                                            <td>: {{ $row->result }}</td><br>
                                        </tr>
                                        <tr>
                                            <td>Year</td>
                                            <td>: {{ $row->year }}</td>
                                            <br>
                                        </tr>
                                        <tr>
                                            <td>Board</td>
                                            <td>: {{ $row->board }}</td><br>
                                        </tr>
                                        </tbody>
                                    </table><br>
                                    @endforeach

                                <!-- Original Loop -->
                                </div>
                            </div>
                            <br>




                            <div class="experience">
                                <div class="row">
                                    <h3>EXPERIENCES</h3><hr>
                                </div>
                                <div class="row">
                                    <table>
                                        <!-- Original EXPERIENCES Loop -->
                                        @php
                                            $sl =0;
                                        @endphp

                                        @foreach ($exps as $row)

                                            <input type="hidden" value=" {{++$sl}}" name="">
                                            <tbody>
                                        <h5>Designation<b> : {{$row->designation_title}}</b></h5><br><!-- designation_title -->
                                        <tr>
                                            <td>Com. Name</td>
                                            <td>: {{$row->com_name}}</td>
                                            <br>
                                        </tr>
                                        <tr>
                                            <td>Com. Location</td>
                                            <td>: {{$row->com_location}}</td><br>
                                        </tr>
                                        <tr>
                                            <td>Duration</td>
                                            <td>: {{$row->timefrom}} to {{$row->timeto}}</td><br>
                                        </tr>
                                        <br>


                                        </tbody>
                                    </table>
                                    @endforeach
                                    <br>
                                </div>
                                <br>
                                <div class="row">
                                    {{--<table>--}}
                                        {{--<p><b>Web Developer</b></p>--}}
                                        {{--<hr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Company Name</td>--}}
                                            {{--<td>: Datavision Ltd</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Company Location</td>--}}
                                            {{--<td>: Dhanmondi</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>Duration</td>--}}
                                            {{--<td>: 1/1/2017 to 1/1/2018</td>--}}
                                        {{--</tr>--}}
                                    {{--</table><br>--}}
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
        </div>


    </div>


    {{--<i class="fa fa-table"></i> Trainers' Resume </div>--}}

    {{--<div class="card-body ">--}}
        {{--<!-- card body start that means main body here -->--}}

        {{--{{$details}}--}}
{{--<div class="row">--}}







{{--</div>--}}


{{--</div>--}}



@endsection
