

@extends('admin.layouts.master')

@section('content')
    @if(session()->has('message'))
        {{ session('message') }}
    @endif


    <div class="card-body ">
        <!-- card body start that means main body here -->


<h4 class="h4 text-center text-c">Add Course</h4>
<form method="POST" action="{{url('admin/adcourses')}}"  enctype="multipart/form-data" class="form-horizontal">
        {{csrf_field()}}
        <div id="reportArea">

            <div class="form-group">
                <label class="control-label col-sm-2" for=""> Course Category :</label>
                <div class="col-sm-10">
                    <select name="course_category_id" id="" class="form-control input-sm">
                        <option value="IT Training">IT Training</option>
                        <option value="IT Training">IT Training</option>
                        <option value="IT Training">IT Training</option>
                        <option value="IT Training">IT Training</option>
                        <option value="IT Training">IT Training</option>
                        <option value="IT Training">IT Training</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Course Title:</label>
                <div class="col-sm-10">
                    <input type="text" name="course_title" class="form-control input-sm" placeholder="Enter Course Title"  >
                </div>
            </div>
            <div class="form-group">
                {{--<label class="control-label col-sm-2" for="">Course Image:</label>--}}
                {{--<div class="col-sm-10">--}}
                    {{--<input type="file"  id="c_image" name="c_image" class="form-control input-sm">--}}
                    {{--<span class="text-danger">Image size must be 1200 * 400</span>--}}
                {{--</div>--}}
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Start Date</label>
                <div class="col-sm-10">
                    <input type="date" name="course_date"  class="form-control input-sm">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Time:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control input-sm"  name="course_time">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Course Type:</label>
                <div class="col-sm-10">
                    <select name="course_type" id="c_type" class="form-control input-sm">
                        <option value="Web Development">Web Development</option>
                        <option value="Graphics">Graphics</option>
                        <option value="HR Management">HR Management</option>
                        <option value="Database">Database</option>
                        <option value="Database">Database</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Course Deadline </label>
                <div class="col-sm-10">
                    <input type="date" class="form-control input-sm"  name="course_deadline">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Course About :</label>
                <div class="col-sm-10">
                    <textarea name="course_about"  placeholder="Write About Course and use comma(,) for line break" class="form-control input-sm"  rows="2"></textarea>                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Course Object:</label>
                <div class="col-sm-10">
                    <textarea name="course_object" placeholder="Write About Object and use comma(,) for line break" class="form-control input-sm input-sm"  rows="2"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Others Information:</label>
                <div class="col-sm-10">
                    <textarea name="course_others" placeholder="Write others information and use comma(,) for line break" class="form-control input-sm"  rows="2"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="">Who will Attend :</label>
                <div class="col-sm-10">
                    <input type="text" name="course_who_attend"  class="form-control input-sm" placeholder="Enter the eligibility" >
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" name="submit" value="Add Course" class="btn btn-info">
                </div>
            </div>

        </div>

</form>





</div>

@endsection

