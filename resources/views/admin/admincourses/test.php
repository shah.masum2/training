<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>SL.</th>
        <th>Course Title</th>
        <th>Image</th>
        <th>Course Start Date</th>
        <th>Deadline</th>
        <th>Time</th>
        <th>Action</th>
    </tr>
    </thead>
    {{--<tfoot></tfoot>--}}
    <tbody>

    @php
    $sl = 0;
    @endphp
    @foreach($ad as $adn_course)
    <tr>
        <td>{{ ++$sl }}</td>
        <td>{{ $adn_course->course_title }}</td>
        <td>{{ $adn_course->c_image}}</td>
        <td>{{ $adn_course->c_date }}</td>
        <td>{{ $adn_course->c_time }}</td>
        <td>{{ $adn_course->c_deadline }}</td>

        <!-- for full details to view -->
        <td>
            <a href="{{url('/admin/admincourses/'.$adn_course->id)}}" class="btn btn-info btn-sm">View</a>

        </td>

    </tr>

    @endforeach
    </tbody>
</table>
