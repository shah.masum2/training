


@extends('admin.layouts.master')


@section('content')
    @if(session()->has('message'))
        {{ session('message') }}
    @endif


    <div class="card-body ">
        <!-- card body start that means main body here -->


        <div class="table-responsive">
            <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>

                    <th>Course Title</th>
                    <th>Image</th>
                    <th>Course Start Date</th>
                    <th>Deadline</th>
                    <th>Time</th>
                    <th>Action</th>
                </tr>
                </thead>
                {{--<tfoot></tfoot>--}}
                <tbody>


                        <td>{{$admincourses->course_title }}</td>
                        <td>{{ $admincourses->c_image}}</td>
                        <td>{{ $admincourses->c_date }}</td>
                        <td>{{ $admincourses->c_time }}</td>
                        <td>{{ $admincourses->c_deadline }}</td>

                        <!-- for full details to view -->
                        <td>
                            {{--<a href="{{url('/admin/admincourses/'.$adn_course->id)}}" class="btn btn-info btn-sm">View</a>--}}

                        </td>




                </tbody>
            </table>
        </div>
        <!-- main body close -->
    </div>



@endsection



