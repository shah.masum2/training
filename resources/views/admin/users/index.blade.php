
{{--{{$applicants}}--}}

@extends('admin.layouts.master')

@section('content')

    <i class="fa fa-table"></i> Registered Trainees </div>

    <div class="card-body ">
        <!-- card body start that means main body here -->

        @if(session()->has('message'))
            {{ session('message') }}
        @endif

        <div class="table-responsive">
            <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">



                <thead>
                <tr>
                    <th>SL.</th>
                    <th>Name</th>
                    <th>Interested</th>
                    <th>Phone</th>
                    <th>Organization</th>
                    <th>Venue</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot></tfoot>
                <tbody>

                @php
                    $sl = 0;
                @endphp
                @foreach($applicants as $regis)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $regis->first_name }}</td>
                        <td>{{ $regis->interest_course }}</td>
                        <td>{{ $regis->phone }}</td>
                        <td>{{ $regis->organization }}</td>
                        <td>{{ $regis->venue }}</td>

                        <!-- for full details to view -->
                        <td>
                            <a href="{{url('/admin/users/'.$regis->id)}}" class="btn btn-info btn-sm">View</a>

                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
        <!-- main body close -->
    </div>



@endsection

