

@extends('admin.layouts.master')

@section('content')
    @if(session()->has('message'))
        {{ session('message') }}
    @endif

    <i class="fa fa-user-circle"></i> Trainee's Information </div>

    <div class="card-body ">
        <!-- card body start that means main body here -->

        <div class="row">


            <div class="col-md-12"><!-- align="center" -->
                <h4 class="text-danger">Enrolled Course   : {{ $regis->interest_course }}</h4>
                <br>
                <h4 class="panel panel-danger">Personal Profile </h4>
                <hr>
                <!-- Personal Profile -->
                <div class="row">
                    <table class="table table-hover col-md-6">
                        <tr>
                            <th>Name</th>
                            <td>{{ $regis->first_name }} {{ $regis->last_name }}</td>
                        </tr>
                        <tr>
                            <th>Contact</th>
                            <td>{{ $regis->phone }}</td>
                        </tr>
                        <tr>
                            <th>Email </th>
                            <td>{{ $regis->email }}</td>
                        </tr>
                        <tr>
                            <th>Organization</th>
                            <td>{{ $regis->organization }}</td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td>{{ $regis->designation }}</td>
                        </tr>
                        <tr>
                            <th>Venue</th>
                            <td>{{ $regis->venue }}</td>
                        </tr>



                    </table>
                </div>
                <!-- transaction Report -->
                <div class="ror">
                    <h4>Transaction Status </h4>

                    <table class="table table-hover ">
                        <thead >
                        <tr class="text-c">
                            <th class="text-c">User ID</th>
                            <th>Date</th>
                            <th>Course Fee </th>
                            <th>Transaction ID</th>
                            <th> Vat </th>
                            <th> Total Amount</th>
                            <th>Payment Status </th>
                        </tr>
                        </thead>

                        <tbody >
                        <tr class="text-c">
                            <td>{{$regis->user_id}}</td>
                            <td>15 May, 2017</td>
                            <td>{{@$regis->paybkash->course_fee }} .00</td>
                            {{--<td>{{($user->get('first_name') ?: 'Guest');}}</td>--}}
                            <td>{{@$regis->paybkash->transaction_id }}</td>
                            {{--{{{ isset($regis->paybkash->transaction_id) ? $regis->paybkash->transaction_id : 'Default' }}}--}}
                            {{--<td>{{{ $regis->paybkash->transaction_id or 'Default' }}}</td>--}}


                            {{--<td>{{ $regis->paybkash->transaction_id ? $regis->paybkash->transaction_id : null }}</td>--}}
                            {{--<td>@php $regis->paybkash->transaction_id ? $regis->paybkash->transaction_id : null; @endphp</td>--}}
                            {{--<td>@php echo $regis->paybkash->transaction_id ? $regis->paybkash->transaction_id : null; @endphp</td>--}}
                            <td> 00 </td>
                            <td> {{@$regis->paybkash->course_fee }} .00</td>
                            <td class="text-success">Paid</td>
                        </tr>
                        </tbody>
                    </table>


                </div>
                <hr>

                <!-- Edit or Delete Client -->
                <div class="row" >
                    <p class="" align="right" style="margin-left: 800px;">

                        <a href="{{url('/admin/users/'.$regis->id.'/edit')}}" class="btn btn-sm btn-outline-info">Update</a>
                        <!-- Delete data following the main way of laravel  --> &nbsp;&nbsp;&nbsp;
                        {!! Form::open(['url' => '/admin/users/'.$regis->id,'method'=>'delete']) !!}
                        {!! Form::submit('Delete',['class'=>'btn btn-outline-danger btn-sm']) !!}
                        {!! Form::close() !!}

                    </p>



                </div>
            </div>


        </div>



        <!-- for full details to view -->

        <a href="{{url('/admin/users/')}}" class="btn btn-default btn-outline-primary">Return</a>




        <!-- main body close -->
    </div>



@endsection

