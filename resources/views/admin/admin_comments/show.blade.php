
@extends('admin.layouts.master')


@section('content')


    <i class="fa fa-table h3"></i>  Client's Comment</div>

    <div class="card-body ">
        <!-- card body start that means main body here -->
        @if(session()->has('message'))
            {{ session('message') }}
        @endif

        <div class="table-responsive">


                {{--@php--}}
                {{--$sl = 0;--}}
                {{--@endphp--}}
                {{--@foreach($comments as $subject)--}}

                        {{--<td>{{ ++$sl }}</td>--}}
                        <h4>{{ $admin_comments->name }}</h4>
                        {{--<td>{{ $subject->course_image}}</td>--}}
                        <h5>{{ $admin_comments->email }}</h5>
                        <p>{{ $admin_comments->message }}</p>
                    {{--<td>{{ $subject->course_deadline }}</td>--}}

                    <!-- for full details to view -->

                            <a href="{{url('/admin/admin_comments')}}" class="btn btn-info btn-sm">Back</a>


        </div>
        <!-- main body close -->
    </div>



@endsection




