
@extends('admin.layouts.master')


@section('content')
    @if(session()->has('message'))
        {{ session('message') }}
    @endif

    <i class="fa fa-table"></i>  All Comments</div>

    <div class="card-body ">
        <!-- card body start that means main body here -->


        <div class="table-responsive">
            <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    {{--<th>SL.</th>--}}
                    <th>Name</th>
                    <th>Email</th>
                    <th>Comments</th>
                    <th>Action</th>
                </tr>
                </thead>
                {{--<tfoot></tfoot>--}}
                <tbody>

                {{--@php--}}
                    {{--$sl = 0;--}}
                {{--@endphp--}}
                @foreach($comments as $subject)
                    <tr>
                        {{--<td>{{ ++$sl }}</td>--}}
                        <td>{{ $subject->name }}</td>
                        {{--<td>{{ $subject->course_image}}</td>--}}
                        <td>{{ $subject->email }}</td>
                        <td>{{ $subject->message }}</td>
                        {{--<td>{{ $subject->course_deadline }}</td>--}}

                        <!-- for full details to view -->
                        <td>
                            <a href="{{url('/admin/admin_comments/'.$subject->id)}}" class="btn btn-info btn-sm">View</a>

                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
        <!-- main body close -->
    </div>



@endsection




