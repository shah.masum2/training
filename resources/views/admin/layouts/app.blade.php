<!DOCTYPE html>
<html lang="en">

@include('admin.layouts.head')

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
@include('admin.layouts.header')
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
    {{--<ol class="breadcrumb">--}}
    {{--<li class="breadcrumb-item">--}}
    {{--<a href="#">Dashboard</a>--}}
    {{--</li>--}}
    {{--<li class="breadcrumb-item">Applicant Teainee</li>--}}
    {{--</ol>--}}




    <!-- Example DataTables Card-->


        <div class="card mb-3">
            <div class="card-header bg-info " style="color: #fff;">
                {{--<i class="fa fa-table"></i> Register Trainees</div>--}}


                {{--@yield('content')--}}

            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->

      @include('admin.layouts.footer')


    </div>
</body>

</html>