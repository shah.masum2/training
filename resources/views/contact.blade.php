
@include('layouts.header')


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Contact Us</title>
</head>
<body>





<section id="contact" style="">
    <div class="container">
        <div class="row">
            <div class="about_our_company" style="margin-bottom: 20px;">
                <h1 style="color:#fff;">Write Your Message</h1>
                <div class="titleline-icon"></div>
                <p style="color:#fff;">  </p>
            </div>
        </div>
        <div class="row">
           <div class="">
               @if(session()->has('message'))
                   <p class="text-danger alert alert-info"><b>{{ session('message') }}</b></p>
               @endif
           </div>
            <div class="col-md-8">
                <form action="{{url('clientfeedbacks')}}" method="post" name="sentMessage" id="contactForm" novalidate="">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Name *" name="name" id="name" required="" data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Your Email *" name="email" id="email" required="" data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Your Phone *" name="phone" id="phone" required="" data-validation-required-message="Please enter your phone number.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Your Message *" name="message" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl get">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <p style="color:#fff;">
                    <strong><i class="fa fa-map-marker"></i> Address</strong><br>
                    House : 15/A, Road : 4, Datavision Training, Dhaka - 1205
                </p>
                <p style="color:#fff;"><strong><i class="fa fa-phone"></i> Phone Number</strong><br>
                    (+88-02345687</p>
                <p style="color:#fff;">
                    <strong><i class="fa fa-envelope"></i>  Email Address</strong><br>
                    info@datavisiontraining.com</p>
                <p></p>
            </div>
        </div>
    </div>
</section>




</body>
</html>

@include('layouts.show_page_footer')
