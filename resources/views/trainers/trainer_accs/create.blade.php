
@include('layouts.form_header')
<!--  -->




{{--{{$latest_trainer_edu->trainer_pinfo_id }}--}}
{{--{{$trainer_id->trainer_pinfo_id}}--}}
{{--{{$trainer_id->id}}--}}
    <!-- === trainer_form  start === -->

    <div class="container trainer_form">
        <h2 class="well text-center margin_top margin_bottom">Trainer Registration Form</h2>
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn btn-default  btn-circle" disabled="disabled">1</a>
                    <p>Personal Info</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p>Education</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p>Experince</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-4" type="button" class="btn btn-default btn-primary btn-circle">4</a>
                    <p>Account</p>
                </div>
            </div>
        </div> <br>
        <form action="{{url('/trainers/trainer_accs')}}" method="post" enctype="multipart/form-data" role="form" data-toggle="validator"
id="registration_form">
            {{ csrf_field() }}

            <div class="row setup-content" id="step-4">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3>Account</h3>
                        <hr>
                        <div class="row margin_top">
                            <div class="col-md-12">
                                <input type="hidden" name="trainer_pinfo_id" value="{{$trainer_id->trainer_pinfo_id}}">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                        <input id="email" type="email" class="form-control" name="email" placeholder="Enter Your Email Address" data-error="Please provide valid email" required>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input id="password" type="password" name="password" placeholder="Password" class="form-control" data-minlength="8" data-error="Minimum of 8 characters" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" name="confirm_password" placeholder="Confirm Password" class="form-control" data-match="#password" data-match-error="Whoops, these don't match" required>
                                    <input type="hidden" name="status" value="1" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="pull-right finish_btn">
                                <button type="submit" class="btn btn-success margin_top margin_bottom">Finish!</button>
                            </div>
                        </div>


                </div>
            </div><br>

    </div>
</div>

@include('layouts.show_page_footer1')
