
{{--{{$course}}--}}

@include('layouts.header')



    <!-- === slide  start === -->
    <div class="content">
        <div class="container-fluid">
            <div class="row margin_bottom">
                <div class="slider_bg_img ">
                    <div class="black_opacity">
                        <div class="col-md-12">
                            <div class="slider_text row text-center wow zoomIn animated">
                                <h1>Better <span class="blue">Education</span> For Better <span class="blue">World</span></h1>
                                <h3>Start Learning Today!</h3><br>
                                <a href="{{url('register')}}" class="btn ">Book Your Seat Now &nbsp; <i class="fa fa-user" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- === slide  end === -->

        <!-- grab user purpose -->
        <!-- grab user purpose -->



        <!-- === services  start === -->

        <div class="container service ">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="ed_heading_top">
                        <h3 class="text-center margin_top margin_bottom">  Our Services </h3>


                    </div>
                </div>
            </div>
            <div class="row service_body margin_bottom">
                <div class="col-md-4">
                    <div class="service_bg_body">
                        <div class="service_icon text-center orange">
                            <span><i class="fa fa-user" aria-hidden="true"></i></span>
                        </div>
                        <div class="service_text text-center">
                            <h4 class="navi_blue">Certificate Provide</h4>
                            <p class="text-center gray">In a single collection, Font Awesome is a pictographic language of web-related actions. In a single collection, Font Awesome is a pictographic language of web-related actions.</p><br>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service_bg_body">
                        <div class="service_icon text-center orange">
                            <span><i class="fa fa-user" aria-hidden="true"></i></span>
                        </div>
                        <div class="service_text text-center">
                            <h4 class="navi_blue">Certificate Provide</h4>
                            <p class="text-center gray">In a single collection, Font Awesome is a pictographic language of web-related actions. In a single collection, Font Awesome is a pictographic language of web-related actions.</p><br>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service_bg_body">
                        <div class="service_icon text-center orange">
                            <span><i class="fa fa-user" aria-hidden="true"></i></span>
                        </div>
                        <div class="service_text text-center">
                            <h4 class="navi_blue">Certificate Provide</h4>
                            <p class="text-center gray">In a single collection, Font Awesome is a pictographic language of web-related actions. In a single collection, Font Awesome is a pictographic language of web-related actions.</p><br>
                        </div>
                    </div>
                </div>
            </div><br>
        </div><br>

        <!-- === services  end === -->


        <!-- === courses  start === -->

        <div class="container-fluid bg_navi_blue white">
            <div class="row margin_top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="ed_heading_top ">
                                <h3 class="text-center margin_top margin_bottom">  Our Courses </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row margin_top margin_bottom">
                        <div class="col-md-2 margin_top margin_bottom course_left_btn">
                            <a href="#" id="nav-toggle" aria-hidden="true">MENU</a>
                            <div id="nav">
                                <ul>
                                    <li><a  class="btn  a">All</a></li> <!--  -->
                                    <li><a  class="btn  b">Web Design</a></li>
                                    <li><a  class="btn c">Graphics Design</a></li>
                                    <li><a  class="btn ">Web Development</a></li>
                                    <li><a  class="btn ">Theme</a></li>
                                    <li><a  class="btn ">Wordpress</a></li>
                                    <li><a  class="btn ">Python</a></li>
                                    <li><a  class="btn ">CMS</a></li>
                                    <li><a  class="btn ">Animation</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-10 margin_top margin_bottom course_right background_white course_right_shadow">

                            <div class="row all">
                                <div class="col-md-6">
                                    <div class="row">
                                        <h4 class="orange">Certificate Course</h4>
                                    </div>
                                    <div class="row">
                                        @php
                                            $sl = 0;
                                        @endphp

                                        @foreach($course as $co)
                                            <input type="hidden" value=" {{++$sl}}" name="">

                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>{{$co->course_title}}</strong></h5>

                                            {{--{{$co->course_type}}--}}
                                        <p>{{$co->course_type}}</p>
                                        <p><a href="{{ url('course/'.$co->id) }}" class="label label-info">Details</a></p>

                                        @endforeach
                                    </div>

                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <h4 class="orange">Workshop</h4>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row wd">
                                <div class="col-md-6">
                                    <div class="row">
                                        <h4 class="orange">Certificate Course</h4>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <h4 class="orange">Workshop</h4>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row gd">
                                <div class="col-md-6">
                                    <div class="row">
                                        <h4 class="orange">Certificate Course</h4>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <h4 class="orange">Workshop</h4>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                    <div class="row">
                                        <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                                        <p>In a single collection, Font Awesome is a pictographic language.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div><br>
        </div>
    </div>
</div>

<!-- === courses  end === -->

<!-- === mentor  start === -->

<div class="container">
    <div class="col-md-10 col-md-offset-1">
        <div class="row margin_top margin_bottom">
            <div class="col-md-4 col-md-offset-4">
                <div class="ed_heading_top ">
                    <h3 class="text-center margin_top margin_bottom"> Meet Our Mentors </h3>
                </div>
            </div>
        </div>
        <div class="row margin_top margin_bottom">
            <div class="col-md-3 ">
                <div class="mentor_bg_body ">
                    <div class="mentor_img">
                        <img src="img/m1.jpg" class="img-responsive">
                    </div>
                    <div class="mentoe_text">
                        <h5 class="navi_blue"><strong>Sample Name</strong></h5>
                        <p class="gray">some info and designation</p>
                        <p><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="mentor_bg_body">
                    <div class="mentor_img">
                        <img src="img/m2.jpg" class="img-responsive">
                    </div>
                    <div class="mentoe_text">
                        <h5 class="navi_blue"><strong>Sample Name</strong></h5>
                        <p class="gray">some info and designation</p>
                        <p><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="mentor_bg_body">
                    <div class="mentor_img">
                        <img src="img/m1.jpg" class="img-responsive">
                    </div>
                    <div class="mentoe_text">
                        <h5 class="navi_blue"><strong>Sample Name</strong></h5>
                        <p class="gray">some info and designation</p>
                        <p><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="mentor_bg_body">
                    <div class="mentor_img">
                        <img src="img/m2.jpg" class="img-responsive">
                    </div>
                    <div class="mentoe_text">
                        <h5 class="navi_blue"><strong>Sample Name</strong></h5>
                        <p class="gray">some info and designation</p>
                        <p><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mentor_btn margin_top margin_bottom">
            <a href="#" class="btn">See All</a>
        </div><br>
    </div>
</div>

<!-- === mentor  end === -->


<!-- === register_add_btn  start === -->

<div class="container-fluid">
    <div class="row reg_background margin_top margin_bottom">
        <div class="container">
            <div class="col-md-4 col-md-offset-1">
                <div class="reg_text text-left">
                    <p>Want to career with <b>DataVision</b></p>
                    <h1 class="blue"><strong>Register Now</strong></h1>
                    <h6>Fewer compatibility concerns because Font Awesome.Fewer compatibility concerns because Font Awesome. </h6><br>
                    <a href="#" class="btn">Click for Register</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- === register_add_btn  end === -->

<!-- === testimonial  start === -->

<div class="container">
    <div class="row margin_top margin_bottom">
        <div class="col-md-4 col-md-offset-4">
            <div class="ed_heading_top ">
                <h3 class="text-center margin_top margin_bottom"> What Client Say About Us </h3>
            </div>
        </div>
    </div>
    <div class="row testimonial_text margin_top margin_bottom">
        <div class="col-md-12 margin_top">
            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                <!--  <ol class="carousel-indicators">
                   <li data-target="#testimonial4" data-slide-to="0" class="active"></li>
                   <li data-target="#testimonial4" data-slide-to="1"></li>
                   <li data-target="#testimonial4" data-slide-to="2"></li>
                 </ol> -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="testimonial4_slide">
                            <img src="img/t1.jpg" class="img-circle img-responsive" />
                            <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
                            <h4>Ben Hanna</h4>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial4_slide">
                            <img src="img/t1.jpg" class="img-circle img-responsive" />
                            <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
                            <h4>Ben Hanna</h4>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial4_slide">
                            <img src="img/t1.jpg" class="img-circle img-responsive" />
                            <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
                            <h4>Ben Hanna</h4>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#testimonial4" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#testimonial4" role="button" data-slide="next">
                    <span class="fa fa-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- === testimonial  end === -->

<!-- === upcoming_workshop  start === -->

<div class="container-fluid">
    <div class="row workshop">
        <div class="col-md-6 col-md-offset-6 workshop_text">
            <div class="col-md-2 col-md-offset-1">
                <h1 class="blue"><strong>13</strong></h1>
                <h5 class="margin_bottom">November, 2017.</h5>
            </div>
            <div class="col-md-8">
                <h5 class="blue margin_bottom"><i><b>Up Coming Workshop</b></i></h5>
                <h4><b>Web and Graphics Design</b></h4>
                <h6 class="blue margin_bottom"><i class="fa fa-map-marker" aria-hidden="true"></i><i> House # 17, R # 4, Dhanmondi, Dhaka, Bangladesh.</i></h6>
                <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
            </div>

        </div>
    </div>
</div>

<!-- === upcoming_workshop  end === -->


<div class="container-fluid">
    <div class="row footer_head bg_navi_blue text-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-6 margin_top margin_bottom">
                <h4><strong>Do you have any questions?</strong></h4>
                <h4>Feel free to contact us! </h4>
            </div>
            <div class="col-md-6 text-center">
                <a href="#" class="btn">Click Here!</a>
            </div>
        </div>
    </div>



@include('layouts.footer')

