
@include('layouts.form_header')
<!--  -->


    <body>
{{--{{$latest_trainer_edu->trainer_pinfo_id }}--}}
    <!-- === trainer_form  start === -->

    <div class="container trainer_form">
        <h2 class="well text-center margin_top margin_bottom">Trainer Registration Form</h2>
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#step-1" type="button" class="btn btn-default  btn-circle" disabled="disabled">1</a>
                    <p>Personal Info</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p>Education</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-3" type="button" class="btn btn-default btn-primary btn-circle">3</a>
                    <p>Experince</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p>Account</p>
                </div>
            </div>
        </div><br>
        <form action="{{url('/trainers/trainer_exps/')}}" method="post" enctype="multipart/form-data" role="form" data-toggle="validator"
id="registration_form"  onsubmit="return validateForm()" name="myForm">
            {{ csrf_field() }}

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li><br>
                        @endforeach
                    </ul>
                </div>
            @endif

        <div class="row setup-content" >
            <div class="col-xs-12">


                <div class="col-md-12">
                    <h3>Experiences</h3>
                    <hr>
                    <div id="trainer_form_experience">
                        <!-- from below part has added to the more experience added part  -->
                        <div class="row margin_top">
                            <input type="hidden" id="trainer_pinfo_id" name="trainer_pinfo_id[]" value="{{$latest_trainer_edu->trainer_pinfo_id}}"  class=" form-control trainer_pinfo_id"  required>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Title</label>
                                    <input name="designation_title[]" id="designation_title" type="text" placeholder="Enter Designation Here.." class="form-control designation_title" data-error="Please write your Designation" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <input name="com_name[]" type="text" placeholder="Enter Company Name Here.." class="form-control " id="com_name" data-error="Please write Company Name" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Department</label>
                                    <input name="com_department[]" type="text" placeholder="Enter Department Here.." class=" form-control" id="com_department">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <input name="com_location[]" type="text" placeholder="Enter Company Location Here.." class="form-control " id="com_location">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Time Duration</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input name="timefrom[]" type="date" class="form-control " id="timefrom" placeholder="From" data-error="This field is required" required>
                                        </div>
                                        <div class="col-md-4">
                                            <input id="timeto mySelect" type="date"  class="form-control " name="timeto[]" placeholder="To" >
                                        </div>
                                        <div class="col-md-4">
                                            <div class="checkbox">
                                                <label>
                                                    {{--<input type="checkbox" onchange="enable_cb(this,this.value)" name="continue[]" value="Currently Working" class="continue initial" id="myCheck" data-error="This field is required" required> Currently Working--}}
                                                    <input type="checkbox"  name="continue[]" value="Currently Working" class="continue initial" id="myCheck" > Currently Working
                                                    <input type="hidden" name="status[]" value="1">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- up part to be added at the JavaScript file -->
                    <div class="row">
                        <div class="col-md-12 add_remove_btn">
                            <div class="pull-right">
                                <!-- <button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Next</button> -->
                                <a class="more_experience_btn btn btn-success" href="#"><i class="fa fa-plus" aria-hidden="true"></i>Add More Experience</a>
                                <button type="submit" name="submit" class="btn btn-primary margin_top margin_bottom">Next<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><br>


    </form>

    </div>





@include('layouts.show_page_footer1')
