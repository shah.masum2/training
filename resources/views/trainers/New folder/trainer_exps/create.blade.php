
@include('layouts.form_header')
<!--  -->


    <body>
{{--{{$latest_trainer_edu->trainer_pinfo_id }}--}}
    <!-- === trainer_form  start === -->

    <div class="container trainer_form">
        <h2 class="well text-center margin_top margin_bottom">Trainer Registration Form</h2>

        <form action="{{url('/trainers/trainer_exps/')}}" method="post" enctype="multipart/form-data" role="form">
            {{ csrf_field() }}

        <div class="row setup-content" >
            <div class="col-xs-12">


                <div class="col-md-12">
                    <h3>Experiences</h3>
                    <hr>
                    <div id="trainer_form_experience">
                        <!-- from below part has added to the more experience added part  -->
                        <div class="row margin_top">
                            <input type="hidden" id="trainer_pinfo_id" name="trainer_pinfo_id[]" value="{{$latest_trainer_edu->trainer_pinfo_id}}"  class=" form-control trainer_pinfo_id"  required>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Title</label>
                                    <input name="designation_title[]" id="designation_title" type="text" placeholder="Enter Designation Here.." class="form-control designation_title"  required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <input name="com_name[]" type="text" placeholder="Enter Company Name Here.." class="form-control " id="com_name" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Department</label>
                                    <input name="com_department[]" type="text" placeholder="Enter Department Here.." class=" form-control" id="com_department">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <input name="com_location[]" type="text" placeholder="Enter Company Location Here.." class="form-control " id="com_location">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Time Duration</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input name="timefrom[]" type="date" class="form-control " id="timefrom" placeholder="From">
                                        </div>
                                        <div class="col-md-4">
                                            <input id="timeto mySelect" type="date"  class="form-control " name="timeto[]" placeholder="To">
                                        </div>
                                        <div class="col-md-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="continue[]" value="Currently Working" class="continue" > Currently Working
                                                    <input type="hidden" name="status[]" value="1">

                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- up part to be added at the JavaScript file -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-left margin_top margin_bottom"><a href="#" id="more_experience_btn">Add More Experiences</a> </div>
                            <div class="pull-right">
                                <!-- <button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Next</button> -->
                                <input type="submit" name="submit"  value="Next" class="btn btn-info margin_top margin_bottom">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><br>


    </form>

    </div>





@include('layouts.footer')
