

@include('layouts.form_header')
<!--  -->


<body>

<div class="home_body">


    <!-- === trainer_form  start === -->

    <div class="container trainer_form">
        <h2 class="well text-center margin_top margin_bottom">Trainer Registration Form</h2>
        <form action="{{url('/trainers/trainer_pinfos/')}}" method="post" enctype="multipart/form-data" role="form">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li><br>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{--<form action="{{ url('/admin/subjects') }}" method="post" enctype="multipart/form-data" role="form">--}}
                {{ csrf_field() }}

            <div class="row " ><!-- setup-content -->
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3>PERSONAL INFORMATION</h3>
                        <hr>
                        <div class="row margin_top">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Full Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"><!-- photo -->
                                <div class="form-group">
                                    <label class="control-label">Photo</label>
                                    <div class="input-group ">
                                        <input type="file" name="photo" class="form-control " placeholder="Photo size must be 300x300. (File format:jpg/png)" > <!-- don't give a name === doesn't send on POST/GET -->

                                    </div>
                                </div>
                                <!-- Custom -->
                                {{--<div class="input-group">--}}
                                    {{--<label class="input-group-btn"><span class="btn btn-primary">--}}
                                    {{--Browse&hellip; <input type="file" name="photo" placeholder="Image should not exceed more than 200KB & Size 300 * 300" style="display: none;" multiple></span></label>--}}
                                    {{--<input type="text" class="form-control" readonly>--}}
                                {{--</div>--}}
                            </div>
                                <!-- Custom -->


                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4"><!-- date_of_birth -->
                                <div class="form-group">
                                    <label class="control-label">Date of Birth</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar-o" aria-hidden="true"></i></span>
                                        <input id="date_of_birth" type="date" class=" form-control" name="date_of_birth" placeholder="mm/dd/yyyy">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Gender</label>
                                    <div class="form-group">
                                        <select name="gender" class="form-control">
                                            <option value="">Select Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Religion</label>
                                    <div class="form-group">
                                        <select name="religion" class="form-control">
                                            <option value="">Select Religion</option>
                                            <option value="Islam">Islam</option>
                                            <option value="Hinduism">Hinduism</option>
                                            <option value="Christianity">Christianity</option>
                                            <option value="Buddhism">Buddhism</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Mobile</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                        <input id="mobile" type="text" class="form-control" name="mobile" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Nationality</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="nationality" value="Bangladesh" class="country_check"> Bangladeshi
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <input  type="text" class="form-control other_country_check" name="email" placeholder="Others">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Marital Status</label>
                                    <div class="form-group">
                                        <select name="marital_status" class="form-control">
                                            <option value="">Select Option</option>
                                            <option value="Single">Single</option>
                                            <option value="Maried">Maried</option>
                                            <option value="Separate">Separate</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                        <textarea id="address"  class="form-control textarea " name="address" placeholder="Please give a full address">  </textarea>
                                        {{--<input id="address" type="text" class="form-control " name="address" placeholder="Please give a full address">--}}
                                        <input id="status" type="hidden"  name="status" value="1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--<a class="btn btn-primary   pull-right "  type="" >Next</a>--}}
                    <input type="submit" name="submit" class="btn btn-primary nextBtn  pull-right margin_top margin_bottom" value="Submit">
                    {{--<input type="submit" name="submit" value="Next" class="btn btn-primary pull-right">--}}
                    {{--<input type="submit" name="submit" value="Next" class="btn btn-primary pull-right">--}}
                    {{--<button class="btn btn-primary nextBtn  pull-right margin_top margin_bottom" type="button" >Next</button>--}}
                </div>
            <br>
        </form>

            <div class="row setup-content" id="step-2">
                {{--<div class="col-xs-12">--}}
                    {{--<div class="col-md-12 ">--}}
                        {{--<h3> EDUCATION</h3>--}}
                        {{--<hr>--}}
                        {{--<div class="input_fields_wrap ">--}}
                            {{--<div class="trainer_form_education">--}}
                                {{--<div class="row margin_top">--}}
                                    {{--<div class="col-md-2">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Degree</label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select name="degree" class="form-control">--}}
                                                    {{--<option value=""></option>--}}
                                                    {{--<option value="SSC">SSC</option>--}}
                                                    {{--<option value="HSC">HSC</option>--}}
                                                    {{--<option value="A'level">A'level</option>--}}
                                                    {{--<option value="O'level">O'level</option>--}}
                                                    {{--<option value="Diploma">Diploma</option>--}}
                                                    {{--<option value="Bachelor">Bachelor</option>--}}
                                                    {{--<option value="Master">Master</option>--}}
                                                    {{--<option value="Phd">Phd</option>--}}
                                                    {{--<option value="Mphil">Mphil</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-5">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Institute</label>--}}
                                            {{--<div class="input-group">--}}
                                                {{--<span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>--}}
                                                {{--<input id="institute" type="text" class="form-control" name="institute" placeholder="Please Write Your Institute Name">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-5">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Department/Group</label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select class="form-control">--}}
                                                    {{--<option>Please Select Department</option>--}}
                                                    {{--<option>Department Name</option>--}}
                                                    {{--<option>Department name</option>--}}
                                                    {{--<option>Please Select Department</option>--}}
                                                    {{--<option>Department Name</option>--}}
                                                    {{--<option>Department name</option>--}}
                                                    {{--<option>Please Select Department</option>--}}
                                                    {{--<option>Department Name</option>--}}
                                                    {{--<option>Department name</option>--}}
                                                    {{--<option>Please Select Department</option>--}}
                                                    {{--<option>Department Name</option>--}}
                                                    {{--<option>Department name</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Result <small style="color: #CCCCCC" >(Optional)</small> </label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<input id="result" type="text" class="form-control" name="result" placeholder="CGPA">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Year</label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select class="form-control">--}}
                                                    {{--<option>2017</option>--}}
                                                    {{--<option>2016</option>--}}
                                                    {{--<option>2015</option>--}}
                                                    {{--<option>2014</option>--}}
                                                    {{--<option>2013</option>--}}
                                                    {{--<option>2012</option>--}}
                                                    {{--<option>2017</option>--}}
                                                    {{--<option>2016</option>--}}
                                                    {{--<option>2015</option>--}}
                                                    {{--<option>2014</option>--}}
                                                    {{--<option>2013</option>--}}
                                                    {{--<option>2012</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Board</label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select class="form-control">--}}
                                                    {{--<option value="">Select</option>--}}
                                                    {{--<option value="Barisal">Barisal</option>--}}
                                                    {{--<option value="Chittagong">Chittagong</option>--}}
                                                    {{--<option value="Comilla">Comilla</option>--}}
                                                    {{--<option value="Dhaka">Dhaka</option>--}}
                                                    {{--<option value="Jessore">Jessore</option>--}}
                                                    {{--<option value="Rajshahi">Rajshahi</option>--}}
                                                    {{--<option value="Sylhet">Sylhet</option>--}}
                                                    {{--<option value="Dinajpur">Dinajpur</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="pull-left margin_top margin_bottom"><a href="#" class="more_degree_btn">Add More Degree</a></div>--}}
                                {{--<div class="pull-right ">--}}
                                    {{--<button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Back</button>&nbsp;&nbsp;<button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Next</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<br>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="row setup-content" id="step-3">
                {{--<div class="col-xs-12">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<h3>Experiences</h3>--}}
                        {{--<hr>--}}
                        {{--<div class="trainer_form_experience">--}}
                            {{--<div class="row margin_top">--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label">Title</label>--}}
                                        {{--<input id="designation_title" type="text" placeholder="Enter Designation Here.." class=" form-control" name="designation_title">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label">Company</label>--}}
                                        {{--<input id="com_name" type="text" placeholder="Enter Company Name Here.." class="form-control" name="com_name">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label">Department</label>--}}
                                        {{--<input id="com_department" type="text" placeholder="Enter Department Here.." class=" form-control" name="com_department">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label">Location</label>--}}
                                        {{--<input id="com_location" type="text" placeholder="Enter Company Location Here.." class="form-control" name="com_location">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-8">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="control-label">Time Duration</label>--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input id="timefrom" type="date" class="form-control " name="timefrom" placeholder="From">--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<input id="timeto" type="date" class="form-control mySelect" name="timeto" placeholder="To">--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-4">--}}
                                                {{--<div class="checkbox">--}}
                                                    {{--<label>--}}
                                                        {{--<input type="checkbox" class="myCheck"> Currently Working--}}
                                                    {{--</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="pull-left margin_top margin_bottom"><a href="#" class="more_experience_btn">Add More Experiences</a></div>--}}
                                {{--<div class="pull-right ">--}}
                                    {{--<button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Back</button>&nbsp;&nbsp;<button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Next</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="row setup-content" id="step-4">
                {{--<div class="col-xs-12">--}}
                    {{--<div class="col-md-12">--}}
                        {{--<h3>Account</h3>--}}
                        {{--<hr>--}}
                        {{--<div class="row margin_top">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Email</label>--}}
                                    {{--<div class="input-group">--}}
                                        {{--<span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>--}}
                                        {{--<input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Email Address">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Password</label>--}}
                                    {{--<input type="Password" placeholder="Password" class="form-control">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Confirm Password</label>--}}
                                    {{--<input type="Password" placeholder="Confirm Password" class="form-control">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="pull-right">--}}
                            {{--<button class="btn btn-primary nextBtn margin_top margin_bottom  add_field_button " type="button" >Back</button>&nbsp;&nbsp;<button class="btn btn-success margin_top margin_bottom  margin_top margin_bottom" type="submit">Finish!</button>--}}
                        {{--</div>--}}
                    {{--</div><br>--}}
                {{--</div>--}}
            </div>


    </div>
</div>
<!-- === trainer_form  end === -->




<!--  -->
@include('layouts.show_page_footer')
{{----}}