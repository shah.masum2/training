
@include('layouts.form_header')
<!--  -->


    <body>
{{--{{$latest_trainer_edu->trainer_pinfo_id }}--}}
{{--{{$trainer_id->trainer_pinfo_id}}--}}
{{--{{$trainer_id->id}}--}}
    <!-- === trainer_form  start === -->

    <div class="container trainer_form">
        <h2 class="well text-center margin_top margin_bottom">Trainer Registration Form</h2>

        <form action="{{url('/trainers/trainer_accs')}}" method="post" enctype="multipart/form-data" role="form">
            {{ csrf_field() }}

            <div class="row setup-content" id="step-4">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3>Account</h3>
                        <hr>
                        <div class="row margin_top">
                            <div class="col-md-12">
                                <input type="hidden" name="trainer_pinfo_id" value="{{$trainer_id->trainer_pinfo_id}}">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Your Email Address" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input type="password" name="password" placeholder="Password" class="form-control" required="required">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Confirm Password</label>
                                    <input type="password" name="confirm_password" placeholder="Confirm Password" class="form-control">
                                    <input type="hidden" name="status" value="1" >

                                </div>
                            </div>
                        </div>
                        <div class="pull-right">
                            <!-- <button class="btn btn-success margin_top margin_bottom  margin_top margin_bottom" type="submit">Finish!</button> -->
                            <input type="submit" name="submit" value="Finish" class="btn btn-success margin_top margin_bottom">
                        </div>
                    </div><br>
                </div>
            </div><br>

    </div>





@include('layouts.show_page_footer')
