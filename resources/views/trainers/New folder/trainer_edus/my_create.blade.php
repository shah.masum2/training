
{{--{{$courseid->id}}--}}
{{--{{$latest_trainer->id}}--}}

@include('layouts.form_header')
<!--  -->


    <body>
        

      <!-- === trainer_form  start === -->


    <h2 class="well text-center">Trainer Registration Form</h2>

  <div class="col-lg-12 well">
  <div class="row">
      <form action="{{url('/trainers/trainer_edus/')}}" method="post" enctype="multipart/form-data" role="form">
          {{ csrf_field() }}
          <div class="col-sm-12">
            <div class="row">

              <!-- education start -->
              <div class="row " id="s">
                  <div class="col-xs-12">
                  <div class="col-md-12 ">
                  <h3> EDUCATION</h3>
                  <hr>

                    <div class="adding">


                    <div class="input_fields_wrap wrapper">

                        <div class="trainer_form_education  " name="mytext" >
                            <div class="row margin_top">
                                <input type="hidden" name="trainer_pinfo_id" value="{{$latest_trainer->id}}" id="trainer_pinfo_id">
                                <div class="col-md-2">

                                    <div class="form-group">
                                        <label class="control-label">Degree</label>
                                        <div class="form-group">
                                            <select name="degree" class="form-control" required>
                                                <option value="">Select</option>
                                                <option value="SSC/O'level">SSC/O'level </option>
                                                <option value="HSC/A'level">HSC/A'level</option>
                                                <option value="Diploma">Diploma</option>
                                                <option value="Bachelor">Bachelor</option>
                                                <option value="Master">Master</option>
                                                <option value="Phd">Phd</option>
                                                <option value="Mphil">Mphil</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="control-label">Institute</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>
                                            <input id="institute" type="text"  class="form-control" name="institute" placeholder="Please Write Your Institute Name" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="control-label">Department/Group</label>
                                        <div class="form-group">
                                            <select name="department_group" id="department_group" class="form-control">
                                                <option value="">Please Select Department</option>
                                                <option value="Department Name">Department Name</option>
                                                <option value="Department Name">Department name</option>
                                                <option value="Department Name">Please Select Department</option>
                                                <option value="Department Name">Department Name</option>
                                                <option value="Department Name">Department name</option>
                                                <option value="Department Name">Please Select Department</option>
                                                <option value="Department Name">Department Name</option>
                                                <option value="Department Name">Department name</option>
                                                <option value="Department Name">Please Select Department</option>
                                                <option value="Department Name">Department Name</option>
                                                <option value="Department Name">Department name</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Result <small style="color: #bcc7cc" >(Optional)</small> </label>
                                        <div class="form-group">
                                            <input id="result" type="text" class="form-control" name="result" placeholder="CGPA/Division">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Year</label>
                                        <div class="form-group">
                                            <select name="year" class="form-control">
                                                <option value="2017">2017</option>
                                                <option value="2016">2016</option>
                                                <option value="2015">2015</option>
                                                <option value="2014">2014</option>
                                                <option value="2013">2013</option>
                                                <option value="2012">2012</option>
                                                <option value="2017">2017</option>
                                                <option value="2016">2016</option>
                                                <option value="2015">2015</option>
                                                <option value="2014">2014</option>
                                                <option value="2013">2013</option>
                                                <option value="2012">2012</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Board</label>
                                        <div class="form-group">
                                            <select name="board" class="form-control">
                                                <option value="">Select</option>
                                                <option value="Barisal">Barisal</option>
                                                <option value="Chittagong">Chittagong</option>
                                                <option value="Comilla">Comilla</option>
                                                <option value="Dhaka">Dhaka</option>
                                                <option value="Jessore">Jessore</option>
                                                <option value="Rajshahi">Rajshahi</option>
                                                <option value="Sylhet">Sylhet</option>
                                                <option value="Dinajpur">Dinajpur</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="remove_field" name="trainer_form_education[]">
                        </div>
                    </div>




                    </div>


                  <div class="row">
                  <div class="col-md-12">

                  <div class="pull-left margin_top margin_bottom">
                      <a class="btn addRow add_field_button">Add More Degree</a>
                      <a href="#" class="remove_field" name="mytext[]">Remove</a>
                      {{--<a  id="add" class="btn more_degree_btn addRow input_fields_wrap">Add More Degree</a>--}}
                  </div>

                  <div class="pull-right ">
                  <a class="btn btn-primary nextBtn margin_top margin_bottom add_field_button ">Back</a>&nbsp;&nbsp;
                      <input type="hidden" name="status" value="1" >
                      <input type="submit" name="submit" value="Next" class="btn btn-primary nextBtn margin_top margin_bottom ">
                  </div>
                  </div>
                  </div>
                  <br>
                  </div>
                  </div>
              </div>
              <!-- Education close -->





            </div>          
            </div>

        </form> <!-- form cosing -->
        </div>
  </div>
  </div>
      <!-- === trainer_form  end === -->
        </div>


        <!-- //////////////////////====================================/////////////////////////// -->

<!--  -->
@include('layouts.show_page_footer')
