

@include('layouts.show_page_header')

<body>
<!-- grab user purpose -->

{{--@guest--}}

    {{--@else--}}

        {{--<input type="text" name="user_id" id="user_id" value=" {{ Auth::user()->id }}">--}}
        {{--<p>{{ Auth::user()->id }} </p>--}}
    {{--@endif--}}

    <!-- grab user purpose -->
<div class="home_body">

    <div class="container-fluid background_white">
        <div class="container margin_top ">
            <div class="row margin_top margin_bottom">
                <div class="col-md-2">
                    <a href="{{url('/')}}"><b>< Back to home</b></a>
                </div>
                <div class="col-md-6 col-md-offset-1 animated_text">
                    <h5>20% off for Online Registration</h5>
                </div>


                <div class="col-md-2">
                    <div class="dropdown ">
                        <button class="btn bg_navi_blue white dropdown-toggle" type="button" data-toggle="dropdown"> Course Catagory &nbsp;
                            <span><i class="fa fa-bars" aria-hidden="true"></i></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Web Design</a></li>
                            <li><a href="#">Graphics Design</a></li>
                            <li><a href="#">Web Development</a></li>
                            <li><a href="#">Web Design</a></li>
                            <li><a href="#">Graphics Design</a></li>
                            <li><a href="#">Web Development</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- === course_summury_title  start === -->

    <div class="container-fluid">
        <div class="row bg_navi_blue course_summury_title">
            <div class="col-md-12 text-center">
                <h1 class="white margin_top margin_bottom"><strong>Front-End Development</strong></h1>


                @guest
                    <p class="margin_bottom">

                        <a href="{{url('register')}}" class="btn reg"><i class="fa fa-user-plus" aria-hidden="true"></i>  Book Your Seat</a><a href="#" class="btn fav"><i class="fa fa-heart" aria-hidden="true"></i>  Add to Favorite</a>
                    </p>
                    @else

                        {{--<input type="text" name="user_id" id="user_id" value=" {{ Auth::user()->id }}">--}}

                        <p class="margin_bottom">
                        {{--<form action="{{url('orders')}}" method="post" enctype="multipart/form-data">--}}
                        <form action="{{url('trai_regi_displays')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id}}">
                            <input type="hidden" name="subject_id" value="{{$course->id}}">
                            <input type="hidden" name="course_title" value="{{$course->course_title}}">
                            <input type="hidden" name="course_fee" value="{{$course->course_fee}}">
                            <input type="hidden" name="course_venue" value="{{$course->course_venue}}">
                            <button  class="btn reg" value ="Book Your Seat"><i class="fa fa-user-plus" aria-hidden="true">  Book Your Seat</i>  </button>
                            {{--<button href="{{url('login')}}" class="btn reg"><i class="fa fa-user-plus" aria-hidden="true"></i>  Book Your Seat</button>--}}

                            <a href="#" class="btn fav"><i class="fa fa-heart" aria-hidden="true"></i>  Add to Favorite</a>
                        </form>

                        </p>

                    @endif


            </div>
        </div>
    </div>

    <!-- === course_summury_title  end === -->


    <!-- === course_summury_body  start === -->

    <div class="container">
        <div class="row margin_top">
            <div class="col-md-8 course_summury_left background_white margin_top margin_bottom">
                <div class="margin_bottom margin_top">
                    <div class="row course_summury_left_Padding">
                        <div class="col-md-6 text-left course_summury_left_margin"><h4>{{$course->course_title}}</h4></div>
                        <div class="col-md-6 text-right "><p class="crse_fee_bottom ">COURSES FEE</p>
                            <h4>{{$course->course_fee}}</h4></div>

                        <!--  <div class="col-md-6 text-left">

                         </div>
                          <div class="col-md-6 text-right">

                         </div> -->
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row course_bg_highlight">
                                <div class="course_summury_left_Padding margin_top">
                                    <img src="{{ asset('images/'.$course->course_image) }}" class="img-responsive "><br>
                                    <h6><b>Date:</b> {{$course->course_date}}</h6>
                                    <h6><b>Duration:</b> Day ({{$course->course_class_time}})</h6>
                                    <h6><b>No of Classes:</b> {{$course->course_total_classes}}</h6>
                                    <h6><b>Last Date of Registration:</b> {{$course->course_deadline}}</h6>
                                    <h6><b>Venue:</b> {{$course->course_venue}}</h6><br>
                                </div>
                            </div>

                            <div class="row course_summury_left_Padding">
                                <h5 class="margin_top"> {{$course->course_title}}</h5>
                                {{--<h5 class="margin_top">{{$course->course_about }}</h5>--}}
                                <p>
                                    {!! $course->course_about !!}


                                </p>

                                <br>

                                <h5 class="margin_top">Course Objects</h5>
                                <p><i class="fa fa-check-square-o" aria-hidden="true"></i> {!! $course->course_object !!}</p>
                                <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Learn how to give interactive feature of your website.</p>
                                {{--<p><i class="fa fa-check-square-o" aria-hidden="true"></i> Learn how to make responsive website.</p>--}}

                                <h5 class="margin_top">Who will Attend</h5><!-- course_who_attend  -->
                                <p><i class="fa fa-clone" aria-hidden="true"></i> {!! $course->course_who_attend !!}</p>
                                {{--<p><i class="fa fa-clone" aria-hidden="true"></i> Learn how to give interactive feature of your website.</p>--}}
                                <p><i class="fa fa-clone" aria-hidden="true"></i> Learn how to make responsive website.</p>

                                <br>
                                <p class="margin_top text-center">Please Share With</p>
                                <p class="margin_bottom text-center"><button type="btn" class="facebook">Facebook</button><button type="btn" class="google">Google+</button><button type="btn" class="linked">Linkedin</button></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 course_summury_right">
                <div class="row course_summury_right_title bg_navi_blue margin_bottom margin_top">
                    <h4>Trainer For This Course</h4>
                </div>
                <div class="row background_white">
                    <div class="course_summary_right_trainer_info ">
                        <img src="img/m2.jpg" class="img-responsive">
                        <h5 class="navi_blue"><strong>Sample Name</strong></h5>
                        <h6 class="gray">some info and designation</h6>
                        <p>
                            In a single collection, Font Awesome is a pictographic language.In a single collection, Font
                            Awesome is a pictographic language.In a single collection, Font Awesome is a pictographic language.
                            In a single collection, Font Awesome is a pictographic language.In a single collection,
                            Font Awesome is a pictographic language.</p>
                        <p><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></p>
                    </div>
                </div>
                <div class="row course_summury_right_title bg_navi_blue margin_bottom margin_top">
                    <h4> Related Courses</h4>
                </div>
                <div class="row" id="web_d">
                    <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                    <p>In a single collection, Font Awesome is a pictographic language.</p>
                    <hr>
                </div>
                <div class="row" id="w_dev">
                    <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                    <p>In a single collection, Font Awesome is a pictographic language.</p>
                    <hr>
                </div>
                <div class="row" id="gra">
                    <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                    <p>In a single collection, Font Awesome is a pictographic language.</p>
                    <hr>
                </div>
                <div class="row" id="web_d">
                    <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                    <p>In a single collection, Font Awesome is a pictographic language.</p>
                    <hr>
                </div>
                <div class="row" id="gra">
                    <h5 class="navi_blue"><i class="fa fa-book" aria-hidden="true"></i>  <strong>Front-End Development</strong></h5>
                    <p>In a single collection, Font Awesome is a pictographic language.</p>
                    <hr>
                </div>


                <div class="row course_summury_right_workshop margin_top margin_bottom">
                    <div class="col-md-12 course_summury_right_workshop_txt">
                        <div class="col-md-2">
                            <h1 class="blue"><strong>13</strong></h1>
                            <h5 class="margin_bottom">November, 2017.</h5>
                        </div>
                        <div class="col-md-8 col-md-offset-2">
                            <h5 class="blue margin_bottom"><i><b>Up Coming Workshop</b></i></h5>
                            <h4><b>Web and Graphics Design</b></h4>
                            <h6 class="blue margin_bottom"><i class="fa fa-map-marker" aria-hidden="true"></i><i> House # 17, R # 4, Dhanmondi, Dhaka, Bangladesh.</i></h6>
                            <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- === course_summury_body  end === -->





</div>


@include('layouts.show_page_footer')
