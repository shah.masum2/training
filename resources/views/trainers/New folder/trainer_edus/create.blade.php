
{{--{{$courseid->id}}--}}
{{--{{$latest_trainer->id}}--}}

@include('layouts.form_header')
<!--  -->


<body>


<!-- === trainer_form  start === -->
<div class="container">

<h2 class="well text-center">Trainer Registration Form</h2>

<div class="col-lg-12 well">
<div class="row">
<form action="{{url('/trainers/trainer_edus/')}}" method="post" enctype="multipart/form-data" role="form">
{{ csrf_field() }}
<div class="col-sm-12">
<div class="row">

<!-- education start -->
<div class="row " id="">
    <div class="col-xs-12">
        <div class="col-md-12 ">
            <h3> EDUCATION</h3>
            <hr>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li><br>
                        @endforeach
                    </ul>
                </div>
        @endif

            <!-- also form adding start here -->
            {{--<div class="trainer_form_education input_fields_wrap">--}}


            <!-- /////////////////////////////////// -->
            {{--<input type="hidden" id="id" value="4"/>--}}
            <div id="trainer_form_education">
                <div class="row margin_top">

                    <input type="hidden" name="trainer_pinfo_id[]" value="{!!  $latest_trainer->id!!}" id="trainer_pinfo_id" class="trainer_pinfo_id">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">Degree</label>
                            <div class="form-group">
                                <select name="degree[]" class="form-control degree" required>
                                    <option value="">Select Degree</option>
                                    <option value="A'level">SSC/A'level</option>
                                    <option value="O'level">HSC/O'level</option>
                                    <option value="Diploma">Diploma</option>
                                    <option value="Bachelor">Bachelor</option>
                                    <option value="Master">Master</option>
                                    <option value="Phd">Phd</option>
                                    <option value="Mphil">Mphil</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label">Institute</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>
                                <input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label">Department/Group</label>
                            <div class="form-group">
                                <select name="department_group[]" id="department_group" class="form-control department_group">
                                    <option value="Web Programming">Web Programming</option>
                                    <option value="Web Development">Web Development</option>
                                    <option value="Graphics & Animation">Graphics & Animation</option>
                                    <option value="Database">Database</option>
                                    <option value="Microsoft Office">Microsoft Office</option>
                                    <option value="3D Auto CAD MAX">3D Auto CAD MAX</option>
                                    <option value="Networking & Security">Networking & Security</option>
                                    <option value="Java Enterprise">Java Enterprise</option>
                                    <option value="#C & .NET Framework">#C & .NET Framework</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>
                            <input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Year</label>
                            <div class="form-group">
                                <select name="year[]" class="form-control year">
                                    <option value="2017">2017</option>
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2014</option>
                                    <option value="2013">2013</option>
                                    <option value="2012">2012</option>
                                    <option value="2017">2017</option>
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2014</option>
                                    <option value="2013">2013</option>
                                    <option value="2012">2012</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Board</label>
                            <div class="form-group">
                                <select name="board[]" class="form-control board">
                                    <option value="">Select</option>
                                    <option value="Barisal">Barisal</option>
                                    <option value="Chittagong">Chittagong</option>
                                    <option value="Comilla">Comilla</option>
                                    <option value="Dhaka">Dhaka</option>
                                    <option value="Jessore">Jessore</option>
                                    <option value="Rajshahi">Rajshahi</option>
                                    <option value="Sylhet">Sylhet</option>
                                    <option value="Dinajpur">Dinajpur</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="status[]" value="1" class="status">

                </div>
            </div>
            {{--<button class="rmv_btn pull-left">Remove </button>--}}

            <div class="col-md-12">
                <div class="pull-left margin_top margin_bottom"><a href="#" class="more_degree_btn">Add More Degree</a></div>
                <div class="pull-right ">
                    <!-- <button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Next</button> -->
                    {{--<input type="hidden" name="status" value="1" >--}}
                    <input type="submit" name="submit" value="Next" class="btn btn-primary nextBtn margin_top margin_bottom add_field_button">                </div>
            </div>

            <!-- //////////////////// /////////////// -->



        </div>
</div>
<!-- Education close -->


    <script>

       {{--//alert({!! $latest_trainer->id !!});--}}


       {{--/*====== trainer_form_education start===== */--}}
       {{--//final for done justified by Masum--}}
       {{--$(document).ready(function(){--}}
           {{--$('.more_degree_btn').on('click',function(){--}}
               {{--addform();--}}
           {{--});--}}
           {{--function addform()--}}
           {{--{--}}
               {{--var educationform=' <div class="trainer_form_education">\n' +--}}
                   {{--'                <div class="row margin_top">\n' +--}}
                   {{--'                    <input type="hidden" name="trainer_pinfo_id[]" value="3" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +--}}
                   {{--// '                    <input type="hidden" name="trainer_pinfo_id[]" value="{{$latest_trainer->id}}" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +--}}
                   {{--'                    <div class="col-md-2">\n' +--}}
                   {{--'                        <div class="form-group">\n' +--}}
                   {{--'                            <label class="control-label">Degree</label>\n' +--}}
                   {{--'                            <div class="form-group">\n' +--}}
                   {{--'                                <select name="degree[]" class="form-control degree" required>\n' +--}}
                   {{--'                                    <option value=""></option>\n' +--}}
                   {{--'                                    <option value="A\'level">SSC/A\'level</option>\n' +--}}
                   {{--'                                    <option value="O\'level">HSC/O\'level</option>\n' +--}}
                   {{--'                                    <option value="Diploma">Diploma</option>\n' +--}}
                   {{--'                                    <option value="Bachelor">Bachelor</option>\n' +--}}
                   {{--'                                    <option value="Master">Master</option>\n' +--}}
                   {{--'                                    <option value="Phd">Phd</option>\n' +--}}
                   {{--'                                    <option value="Mphil">Mphil</option>\n' +--}}
                   {{--'                                </select>\n' +--}}
                   {{--'                            </div>\n' +--}}
                   {{--'                        </div>\n' +--}}
                   {{--'                    </div>\n' +--}}
                   {{--'                    <div class="col-md-5">\n' +--}}
                   {{--'                        <div class="form-group">\n' +--}}
                   {{--'                            <label class="control-label">Institute</label>\n' +--}}
                   {{--'                            <div class="input-group">\n' +--}}
                   {{--'                                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>\n' +--}}
                   {{--'                                <input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name">\n' +--}}
                   {{--'                            </div>\n' +--}}
                   {{--'                        </div>\n' +--}}
                   {{--'                    </div>\n' +--}}
                   {{--'                    <div class="col-md-5">\n' +--}}
                   {{--'                        <div class="form-group">\n' +--}}
                   {{--'                            <label class="control-label">Department/Group</label>\n' +--}}
                   {{--'                            <div class="form-group">\n' +--}}
                   {{--'                                <select name="department_group[]" id="department_group" class="form-control department_group">\n' +--}}
                   {{--'                                    <option value="Please Select Department">Please Select Department</option>\n' +--}}
                   {{--'                                    <option value="Department Name">Department Name</option>\n' +--}}
                   {{--'                                    <option value="Department name">Department name</option>\n' +--}}
                   {{--'                                    <option value="Please Select Department">Please Select Department</option>\n' +--}}
                   {{--'                                    <option value="Department Name">Department Name</option>\n' +--}}
                   {{--'                                    <option value="Department name">Department name</option>\n' +--}}
                   {{--'                                    <option value="Please Select Department">Please Select Department</option>\n' +--}}
                   {{--'                                    <option value="Department Name">Department Name</option>\n' +--}}
                   {{--'                                    <option value="Department name">Department name</option>\n' +--}}
                   {{--'                                    <option value="Please Select Department">Please Select Department</option>\n' +--}}
                   {{--'                                    <option value="Department Name">Department Name</option>\n' +--}}
                   {{--'                                    <option value="Department name">Department name</option>\n' +--}}
                   {{--'                                </select>\n' +--}}
                   {{--'                            </div>\n' +--}}
                   {{--'                        </div>\n' +--}}
                   {{--'                    </div>\n' +--}}
                   {{--'                </div>\n' +--}}
                   {{--'                <div class="row">\n' +--}}
                   {{--'                    <div class="col-md-4">\n' +--}}
                   {{--'                        <div class="form-group">\n' +--}}
                   {{--'                            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>\n' +--}}
                   {{--'                            <input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">\n' +--}}
                   {{--'                        </div>\n' +--}}
                   {{--'                    </div>\n' +--}}
                   {{--'                    <div class="col-md-4">\n' +--}}
                   {{--'                        <div class="form-group">\n' +--}}
                   {{--'                            <label class="control-label">Year</label>\n' +--}}
                   {{--'                            <div class="form-group">\n' +--}}
                   {{--'                                <select name="year[]" class="form-control year">\n' +--}}
                   {{--'                                    <option value="2017">2017</option>\n' +--}}
                   {{--'                                    <option value="2016">2016</option>\n' +--}}
                   {{--'                                    <option value="2015">2015</option>\n' +--}}
                   {{--'                                    <option value="2014">2014</option>\n' +--}}
                   {{--'                                    <option value="2013">2013</option>\n' +--}}
                   {{--'                                    <option value="2012">2012</option>\n' +--}}
                   {{--'                                    <option value="2017">2017</option>\n' +--}}
                   {{--'                                    <option value="2016">2016</option>\n' +--}}
                   {{--'                                    <option value="2015">2015</option>\n' +--}}
                   {{--'                                    <option value="2014">2014</option>\n' +--}}
                   {{--'                                    <option value="2013">2013</option>\n' +--}}
                   {{--'                                    <option value="2012">2012</option>\n' +--}}
                   {{--'                                </select>\n' +--}}
                   {{--'                            </div>\n' +--}}
                   {{--'                        </div>\n' +--}}
                   {{--'                    </div>\n' +--}}
                   {{--'                    <div class="col-md-4">\n' +--}}
                   {{--'                        <div class="form-group">\n' +--}}
                   {{--'                            <label class="control-label">Board</label>\n' +--}}
                   {{--'                            <div class="form-group">\n' +--}}
                   {{--'                                <select name="board[]" class="form-control board">\n' +--}}
                   {{--'                                    <option value="">Select</option>\n' +--}}
                   {{--'                                    <option value="Barisal">Barisal</option>\n' +--}}
                   {{--'                                    <option value="Chittagong">Chittagong</option>\n' +--}}
                   {{--'                                    <option value="Comilla">Comilla</option>\n' +--}}
                   {{--'                                    <option value="Dhaka">Dhaka</option>\n' +--}}
                   {{--'                                    <option value="Jessore">Jessore</option>\n' +--}}
                   {{--'                                    <option value="Rajshahi">Rajshahi</option>\n' +--}}
                   {{--'                                    <option value="Sylhet">Sylhet</option>\n' +--}}
                   {{--'                                    <option value="Dinajpur">Dinajpur</option>\n' +--}}
                   {{--'                                </select>\n' +--}}
                   {{--'                            </div>\n' +--}}
                   {{--'                        </div>\n' +--}}
                   {{--'                    </div>\n' +--}}
                   {{--'                    <input type="hidden" name="status[]" value="1" class="status">\n' +--}}
                   {{--'\n' +--}}
                   {{--'                </div>\n' +--}}
                   {{--'                <button class="rmv_btn btn btn-defult text-danger" style=" ">Remove </button>\n' +--}}
                   {{--'            </div>';--}}


               {{--$('.trainer_form_education').append(educationform);--}}
           {{--};--}}
           {{--$('body').delegate('.rmv_edubtn','click', function(){--}}
               {{--$(this).parent().remove();--}}
           {{--});--}}
       {{--});--}}
       {{--/*====== trainer_form_education end===== */--}}



    </script>

    <script>

        {{--$('.more_degree_btn').on('click',function(){--}}
            {{--addform();--}}
        {{--});--}}


        {{--// if($latest_trainer->id === $latest_trainer->id){--}}
        {{--//     var dis_id = $latest_trainer->id;--}}
        {{--// }--}}

        {{--function addform()--}}
        {{--{--}}
            {{--var educationform=' <div class="trainer_form_education">\n' +--}}
                {{--'                <div class="row margin_top">\n' +--}}
                {{--// '                    <input type="hidden" name="trainer_pinfo_id[]" value="{{$latest_trainer->id}}" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +--}}
                {{--// '                    <input type="hidden" name="trainer_pinfo_id[]" value="JSON.parse( json_encode($latest_trainer->id );"  id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +--}}
                {{--'                    <input type="hidden" name="trainer_pinfo_id[]" value="{!! json_encode($latest_trainer->id) !!}" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +--}}
                {{--// '                    <input type="hidden" name="trainer_pinfo_id[]" value="{{$latest_trainer->id}}" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +--}}
                {{--'                    <div class="col-md-2">\n' +--}}
                {{--'                        <div class="form-group">\n' +--}}
                {{--'                            <label class="control-label">Degree</label>\n' +--}}
                {{--'                            <div class="form-group">\n' +--}}
                {{--'                                <select name="degree[]" class="form-control degree" required>\n' +--}}
                {{--'                                    <option value=""></option>\n' +--}}
                {{--'                                    <option value="A\'level">SSC/A\'level</option>\n' +--}}
                {{--'                                    <option value="O\'level">HSC/O\'level</option>\n' +--}}
                {{--'                                    <option value="Diploma">Diploma</option>\n' +--}}
                {{--'                                    <option value="Bachelor">Bachelor</option>\n' +--}}
                {{--'                                    <option value="Master">Master</option>\n' +--}}
                {{--'                                    <option value="Phd">Phd</option>\n' +--}}
                {{--'                                    <option value="Mphil">Mphil</option>\n' +--}}
                {{--'                                </select>\n' +--}}
                {{--'                            </div>\n' +--}}
                {{--'                        </div>\n' +--}}
                {{--'                    </div>\n' +--}}
                {{--'                    <div class="col-md-5">\n' +--}}
                {{--'                        <div class="form-group">\n' +--}}
                {{--'                            <label class="control-label">Institute</label>\n' +--}}
                {{--'                            <div class="input-group">\n' +--}}
                {{--'                                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>\n' +--}}
                {{--'                                <input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name">\n' +--}}
                {{--'                            </div>\n' +--}}
                {{--'                        </div>\n' +--}}
                {{--'                    </div>\n' +--}}
                {{--'                    <div class="col-md-5">\n' +--}}
                {{--'                        <div class="form-group">\n' +--}}
                {{--'                            <label class="control-label">Department/Group</label>\n' +--}}
                {{--'                            <div class="form-group">\n' +--}}
                {{--'                                <select name="department_group[]" id="department_group" class="form-control department_group">\n' +--}}
                {{--'                                    <option value="Please Select Department">Please Select Department</option>\n' +--}}
                {{--'                                    <option value="Department Name">Department Name</option>\n' +--}}
                {{--'                                    <option value="Department name">Department name</option>\n' +--}}
                {{--'                                    <option value="Please Select Department">Please Select Department</option>\n' +--}}
                {{--'                                    <option value="Department Name">Department Name</option>\n' +--}}
                {{--'                                    <option value="Department name">Department name</option>\n' +--}}
                {{--'                                    <option value="Please Select Department">Please Select Department</option>\n' +--}}
                {{--'                                    <option value="Department Name">Department Name</option>\n' +--}}
                {{--'                                    <option value="Department name">Department name</option>\n' +--}}
                {{--'                                    <option value="Please Select Department">Please Select Department</option>\n' +--}}
                {{--'                                    <option value="Department Name">Department Name</option>\n' +--}}
                {{--'                                    <option value="Department name">Department name</option>\n' +--}}
                {{--'                                </select>\n' +--}}
                {{--'                            </div>\n' +--}}
                {{--'                        </div>\n' +--}}
                {{--'                    </div>\n' +--}}
                {{--'                </div>\n' +--}}
                {{--'                <div class="row">\n' +--}}
                {{--'                    <div class="col-md-4">\n' +--}}
                {{--'                        <div class="form-group">\n' +--}}
                {{--'                            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>\n' +--}}
                {{--'                            <input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">\n' +--}}
                {{--'                        </div>\n' +--}}
                {{--'                    </div>\n' +--}}
                {{--'                    <div class="col-md-4">\n' +--}}
                {{--'                        <div class="form-group">\n' +--}}
                {{--'                            <label class="control-label">Year</label>\n' +--}}
                {{--'                            <div class="form-group">\n' +--}}
                {{--'                                <select name="year[]" class="form-control year">\n' +--}}
                {{--'                                    <option value="2017">2017</option>\n' +--}}
                {{--'                                    <option value="2016">2016</option>\n' +--}}
                {{--'                                    <option value="2015">2015</option>\n' +--}}
                {{--'                                    <option value="2014">2014</option>\n' +--}}
                {{--'                                    <option value="2013">2013</option>\n' +--}}
                {{--'                                    <option value="2012">2012</option>\n' +--}}
                {{--'                                    <option value="2017">2017</option>\n' +--}}
                {{--'                                    <option value="2016">2016</option>\n' +--}}
                {{--'                                    <option value="2015">2015</option>\n' +--}}
                {{--'                                    <option value="2014">2014</option>\n' +--}}
                {{--'                                    <option value="2013">2013</option>\n' +--}}
                {{--'                                    <option value="2012">2012</option>\n' +--}}
                {{--'                                </select>\n' +--}}
                {{--'                            </div>\n' +--}}
                {{--'                        </div>\n' +--}}
                {{--'                    </div>\n' +--}}
                {{--'                    <div class="col-md-4">\n' +--}}
                {{--'                        <div class="form-group">\n' +--}}
                {{--'                            <label class="control-label">Board</label>\n' +--}}
                {{--'                            <div class="form-group">\n' +--}}
                {{--'                                <select name="board[]" class="form-control board">\n' +--}}
                {{--'                                    <option value="">Select</option>\n' +--}}
                {{--'                                    <option value="Barisal">Barisal</option>\n' +--}}
                {{--'                                    <option value="Chittagong">Chittagong</option>\n' +--}}
                {{--'                                    <option value="Comilla">Comilla</option>\n' +--}}
                {{--'                                    <option value="Dhaka">Dhaka</option>\n' +--}}
                {{--'                                    <option value="Jessore">Jessore</option>\n' +--}}
                {{--'                                    <option value="Rajshahi">Rajshahi</option>\n' +--}}
                {{--'                                    <option value="Sylhet">Sylhet</option>\n' +--}}
                {{--'                                    <option value="Dinajpur">Dinajpur</option>\n' +--}}
                {{--'                                </select>\n' +--}}
                {{--'                            </div>\n' +--}}
                {{--'                        </div>\n' +--}}
                {{--'                    </div>\n' +--}}
                {{--'                    <input type="hidden" name="status[]" value="1" class="status">\n' +--}}
                {{--'\n' +--}}
                {{--'                </div>\n' +--}}
                {{--'                <button class="rmv_btn btn btn-defult text-danger" style=" ">Remove </button>\n' +--}}
                {{--'            </div>';--}}


            {{--$('.trainer_form_education').append(educationform);--}}
        {{--};--}}
        {{--$('body').delegate('.rmv_edubtn','click', function(){--}}
            {{--$(this).parent().remove();--}}
        {{--});--}}

    </script>




</div>
</div>
</div>

</form> <!-- form cosing -->


</div>
</div>
<!-- === trainer_form  end === -->
   </div> <!-- container -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- //////////////////////====================================/////////////////////////// -->

<!--  -->
@include('layouts.show_page_footer1')
