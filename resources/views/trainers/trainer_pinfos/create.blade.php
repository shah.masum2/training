

@include('layouts.form_header')
<!--  -->


<body>

<div class="home_body">


    <!-- === trainer_form  start === -->

    <div class="container trainer_form">
        <h2 class="well text-center margin_top margin_bottom">Trainer Registration Form</h2>
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="#" type="button" class="btn btn-default btn-primary btn-circle">1</a>
                    <p>Personal Info</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#" type="button" class="btn btn-default btn-circle" disabled>2</a>
                    <p>Education</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#" type="button" class="btn btn-default btn-circle" disabled>3</a>
                    <p>Experince</p>
                </div>
                <div class="stepwizard-step">
                    <a href="#" type="button" class="btn btn-default btn-circle" disabled>4</a>
                    <p>Account</p>
                </div>
            </div>
        </div> <br>
        <form action="{{url('/trainers/trainer_pinfos/')}}" method="post" enctype="multipart/form-data" role="form" data-toggle="validator" id="registration_form">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li><br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- alert close -->
                {{ csrf_field() }}

            <div class="row " ><!-- setup-content -->
                <div class="col-md-12">
                    {{--<div class="">--}}
                        <h3>PERSONAL INFORMATION</h3>
                        <hr>
                        <div class="row margin_top">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Full Name" minlength="2" data-error="Please type your Full Name" required>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6"><!-- photo -->
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label">Photo</label>--}}
                                    {{--<div class="input-group ">--}}
                                        {{--<input type="file" name="photo" class="form-control " placeholder="Photo size must be 300x300. (File format:jpg/png)" > <!-- don't give a name === doesn't send on POST/GET -->--}}

                                    {{--</div>--}}
                            <!-- Custom -->
                                <div class="input-group" style="margin-top: 25px;">
                                    <label class="input-group-btn"><span class="btn btn-default">
                                    Browse&hellip; <input type="file" name="photo"  style="display: none;" multiple></span></label>
                                    <input type="text" class="form-control" placeholder="Image should not exceed more than 200KB & Size 300 * 300" readonly>
                                </div>
                                <!-- Custom -->
                                </div>



                            </div>






                        <div class="row">
                            <div class="col-md-4"><!-- date_of_birth -->
                                <div class="form-group">
                                    <label class="control-label">Date of Birth</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar-o" aria-hidden="true"></i></span>
                                        <input id="date_of_birth" type="date" class=" form-control" name="date_of_birth" placeholder="mm/dd/yyyy" data-error="Please Provide your birth date" required>
                                    </div>
                                     <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Gender</label>
                                        <select name="gender" class="form-control" data-error="Please select your gender" required>
                                            <option value="">Select Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Religion</label>
                                        <select name="religion" class="form-control" data-error="Please select your religion" required>
                                            <option value="">Select Religion</option>
                                            <option value="Islam">Islam</option>
                                            <option value="Hinduism">Hinduism</option>
                                            <option value="Christianity">Christianity</option>
                                            <option value="Buddhism">Buddhism</option>
                                            <option value="Other">Other</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Mobile</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                        <input id="mobile" type="text" class="form-control" name="mobile" placeholder="" minlength="11" maxlength="11" data-error="This number is not valid" pattern="^[_0-9]{1,}$" required>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Nationality</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="nationality" value="Bangladesh" class="country_check" data-error="Please select your Nationality" required> Bangladeshi
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <input  type="text" class="form-control other_country_check" name="email" placeholder="Others" >
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Marital Status</label>
                                    <div class="form-group">
                                        <select name="marital_status" class="form-control" data-error="Please select your marital status" required>
                                            <option value="">Select Option</option>
                                            <option value="Single">Single</option>
                                            <option value="Maried">Maried</option>
                                            <option value="Separate">Separate</option>
                                        </select>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                        <textarea id="address"  class="form-control textarea " name="address" placeholder="Please give a full address" data-error="Please provide your address" required>  </textarea>
                                        {{--<input id="address" type="text" class="form-control " name="address" placeholder="Please give a full address" data-error="Please provide your address" required>--}}
                                        <input id="status" type="hidden"  name="status" value="1">
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>


                    {{--<a class="btn btn-primary   pull-right "  type="" >Next</a>--}}
                    <div class="pull-right add_remove_btn">
                        <button type="submit" name="submit" class="btn btn-primary nextBtn  pull-right margin_top margin_bottom " value="">Next<i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </button>
                    </div>
                    {{--<input type="submit" name="submit" value="Next" class="btn btn-primary pull-right">--}}
                    {{--<input type="submit" name="submit" value="Next" class="btn btn-primary pull-right">--}}
                    {{--<button class="btn btn-primary nextBtn  pull-right margin_top margin_bottom" type="button" >Next</button>--}}
                </div>
            <br>
        </form>

    </div>
</div>
<!-- === trainer_form  end === -->




<!--  -->
@include('layouts.show_page_footer1')
{{----}}