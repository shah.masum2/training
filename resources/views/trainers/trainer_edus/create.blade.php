
{{--{{$courseid->id}}--}}
{{--{{$latest_trainer->id}}--}}

@include('layouts.form_header')
<!--  -->


<body>


<!-- === trainer_form  start === -->
<div class="container">

<h2 class="well text-center margin_top margin_bottom">Trainer Registration Form</h2>
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button" class="btn btn-default  btn-circle" disabled="disabled">1</a>
                <p>Personal Info</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default btn-primary btn-circle">2</a>
                <p>Education</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p>Experince</p>
            </div>
            <div class="stepwizard-step">
                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                <p>Account</p>
            </div>
        </div>
    </div> <br>


    <div class="col-lg-12 ">
<div class="row">
<form action="{{url('/trainers/trainer_edus/')}}" method="post" enctype="multipart/form-data" role="form" data-toggle="validator"
id="registration_form">
{{ csrf_field() }}
<div class="col-sm-12">
<div class="row">

<!-- education start -->
<div class="row " id="">
    <div class="col-xs-12">
        <div class="col-md-12 ">
            <h3> EDUCATION</h3>
            <hr>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li><br>
                        @endforeach
                    </ul>
                </div>
        @endif

            <!-- also form adding start here -->
            {{--<div class="trainer_form_education input_fields_wrap">--}}


            <!-- /////////////////////////////////// -->
            {{--<input type="hidden" id="id" value="4"/>--}}
            <div id="trainer_form_education">
                <div class="row margin_top">

                    <input type="hidden" name="trainer_pinfo_id[]" value="{!!  $latest_trainer->id!!}" id="trainer_pinfo_id" class="trainer_pinfo_id">
                    <div class="col-md-2">
                            <div class="form-group">
                              <label class="control-label">Degree</label>
                                <select name="degree[]" class="form-control degree"  data-error="Please select degree" required>
                                    <option value="">Select Degree</option>
                                    <option value="A'level">SSC</option>
                                    <option value="A'level">O'level</option>
                                    <option value="O'level">HSC</option>
                                    <option value="O'level">A'level</option>
                                    <option value="Diploma">Diploma</option>
                                    <option value="Bachelor">Bachelor</option>
                                    <option value="Master">Master</option>
                                    <option value="Phd">Phd</option>
                                    <option value="Mphil">Mphil</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label">Institute</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>
                                <input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name" data-error="Please provide your Institute name" required>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-5">
                            <div class="form-group">
                              <label class="control-label">Department/Group</label>
                                <select name="department_group[]" id="department_group" class="form-control department_group" data-error="Please select Department/Group" required>
                                    <option value="">Select</option>
                                    <option value="Science">Science</option>
                                    <option value="Commerce">Commerce</option>
                                    <option value="Arts">Arts</option>
                                    <option value="Web Programming">Web Programming</option>
                                    <option value="Web Development">Web Development</option>
                                    <option value="Graphics & Animation">Graphics & Animation</option>
                                    <option value="Database">Database</option>
                                    <option value="Microsoft Office">Microsoft Office</option>
                                    <option value="3D Auto CAD MAX">3D Auto CAD MAX</option>
                                    <option value="Networking & Security">Networking & Security</option>
                                    <option value="Java Enterprise">Java Enterprise</option>
                                    <option value="#C & .NET Framework">#C & .NET Framework</option>
                                </select>
                                 <div class="help-block with-errors"></div>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>
                            <input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">
                        </div>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group">
                              <label class="control-label">Year</label>
                                <select name="year[]" class="form-control year" data-error="Please select passing year" required>
                                    <option value="">Select Year</option>
                                    <option value="2018">2018</option>
                                    <option value="2017">2017</option>
                                    <option value="2016">2016</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2014</option>
                                    <option value="2013">2013</option>
                                    <option value="2012">2012</option>
                                    <option value="2011">2011</option>
                                    <option value="2010">2010</option>
                                    <option value="2009">2009</option>
                                    <option value="2008">2008</option>
                                    <option value="2007">2007</option>
                                    <option value="2006">2006</option>
                                    <option value="2005">2005</option>
                                    <option value="2004">2004</option>
                                    <option value="2003">2003</option>
                                    <option value="2002">2002</option>
                                    <option value="2001">2001</option>
                                    <option value="2000">2000</option>
                                    <option value="1999">1999</option>
                                    <option value="1998">1998</option>
                                    <option value="1997">1997</option>
                                    <option value="1996">1996</option>
                                    <option value="1995">1995</option>
                                    <option value="1994">1994</option>
                                    <option value="1993">1993</option>
                                    <option value="1992">1992</option>
                                    <option value="1991">1991</option>
                                    <option value="1990">1990</option>
                                    <option value="1989">1989</option>
                                    <option value="1988">1988</option>
                                    <option value="1987">1987</option>
                                    <option value="1986">1986</option>
                                    <option value="1985">1985</option>
                                    <option value="1984">1984</option>
                                    <option value="1983">1983</option>
                                    <option value="1982">1982</option>
                                    <option value="1981">1981</option>
                                    <option value="1980">1980</option>
                                </select>
                                 <div class="help-block with-errors"></div>
                            </div>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group">
                              <label class="control-label">Board</label>
                                <select name="board[]" class="form-control board" data-error="Please select Board" required>
                                    <option value="">Select</option>
                                    <option value="Barisal">Barisal</option>
                                    <option value="Chittagong">Chittagong</option>
                                    <option value="Comilla">Comilla</option>
                                    <option value="Dhaka">Dhaka</option>
                                    <option value="Jessore">Jessore</option>
                                    <option value="Rajshahi">Rajshahi</option>
                                    <option value="Sylhet">Sylhet</option>
                                    <option value="Dinajpur">Dinajpur</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                    </div>
                    <input type="hidden" name="status[]" value="1" class="status">
                </div>
            </div>
            {{--<button class="rmv_btn pull-left">Remove </button>--}}

                <div class="col-md-12 add_remove_btn">
                <div class="pull-right ">
                    <!-- <button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Next</button> -->
                    {{--<input type="hidden" name="status" value="1" >--}}
                    <a class="more_degree_btn btn btn-success" href="#"><i class="fa fa-plus" aria-hidden="true"></i>Add More Degree</a>
                    <button type="submit" name="submit" class="btn btn-primary nextBtn margin_top margin_bottom add_field_button">Next<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                </div>
            </div>

            <!-- //////////////////// /////////////// -->



        </div>
</div>
<!-- Education close -->




</div>
</div>
</div>

</form> <!-- form cosing -->


</div>
</div>
<!-- === trainer_form  end === -->
   </div> <!-- container -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- //////////////////////====================================/////////////////////////// -->

<!--  -->
@include('layouts.show_page_footer1')
