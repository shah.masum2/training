
{{--{{$courseid->id}}--}}
{{--{{$latest_trainer->id}}--}}

@include('layouts.form_header')
<!--  -->


    <body>
        

      <!-- === trainer_form  start === -->


    <h2 class="well text-center">Trainer Registration Form</h2>

 <div class="col-lg-12 well">
  <div class="row">
      <form action="{{url('/trainers/trainer_edus/')}}" method="post" enctype="multipart/form-data" role="form">
          {{ csrf_field() }}
          <div class="col-sm-12">
            <div class="row">

              <!-- education start -->
                <div class="row " id="">
                    <div class="col-xs-12">
                        <div class="col-md-12 ">
                            <h3> EDUCATION</h3>
                            <hr>

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                        @endif

                            <!-- also form adding start here -->
                            {{--<div class="trainer_form_education input_fields_wrap">--}}
                            <div class="wrapper ">
                            {{--<div class="add_field_button ">--}}

                            {{--<div class=" trainer_add input_fields_wrap">--}}
                                {{--<div class="row margin_top">--}}
                                    {{--<input type="hidden" name="trainer_pinfo_id[]" value="{{$latest_trainer->id}}" id="trainer_pinfo_id" class="trainer_pinfo_id">--}}
                                    {{--<div class="col-md-2">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Degree</label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select name="degree[]" class="form-control degree" required>--}}
                                                    {{--<option value=""></option>--}}
                                                    {{--<option value="A'level">SSC/A'level</option>--}}
                                                    {{--<option value="O'level">HSC/O'level</option>--}}
                                                    {{--<option value="Diploma">Diploma</option>--}}
                                                    {{--<option value="Bachelor">Bachelor</option>--}}
                                                    {{--<option value="Master">Master</option>--}}
                                                    {{--<option value="Phd">Phd</option>--}}
                                                    {{--<option value="Mphil">Mphil</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-5">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Institute</label>--}}
                                            {{--<div class="input-group">--}}
                                                {{--<span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>--}}
                                                {{--<input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-5">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Department/Group</label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select name="department_group[]" id="department_group" class="form-control department_group">--}}
                                                    {{--<option value="Please Select Department">Please Select Department</option>--}}
                                                    {{--<option value="Department Name">Department Name</option>--}}
                                                    {{--<option value="Department name">Department name</option>--}}
                                                    {{--<option value="Please Select Department">Please Select Department</option>--}}
                                                    {{--<option value="Department Name">Department Name</option>--}}
                                                    {{--<option value="Department name">Department name</option>--}}
                                                    {{--<option value="Please Select Department">Please Select Department</option>--}}
                                                    {{--<option value="Department Name">Department Name</option>--}}
                                                    {{--<option value="Department name">Department name</option>--}}
                                                    {{--<option value="Please Select Department">Please Select Department</option>--}}
                                                    {{--<option value="Department Name">Department Name</option>--}}
                                                    {{--<option value="Department name">Department name</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>--}}
                                            {{--<input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Year</label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select name="year[]" class="form-control year">--}}
                                                    {{--<option value="2017">2017</option>--}}
                                                    {{--<option value="2016">2016</option>--}}
                                                    {{--<option value="2015">2015</option>--}}
                                                    {{--<option value="2014">2014</option>--}}
                                                    {{--<option value="2013">2013</option>--}}
                                                    {{--<option value="2012">2012</option>--}}
                                                    {{--<option value="2017">2017</option>--}}
                                                    {{--<option value="2016">2016</option>--}}
                                                    {{--<option value="2015">2015</option>--}}
                                                    {{--<option value="2014">2014</option>--}}
                                                    {{--<option value="2013">2013</option>--}}
                                                    {{--<option value="2012">2012</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-4">--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">Board</label>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<select name="board[]" class="form-control board">--}}
                                                    {{--<option value="">Select</option>--}}
                                                    {{--<option value="Barisal">Barisal</option>--}}
                                                    {{--<option value="Chittagong">Chittagong</option>--}}
                                                    {{--<option value="Comilla">Comilla</option>--}}
                                                    {{--<option value="Dhaka">Dhaka</option>--}}
                                                    {{--<option value="Jessore">Jessore</option>--}}
                                                    {{--<option value="Rajshahi">Rajshahi</option>--}}
                                                    {{--<option value="Sylhet">Sylhet</option>--}}
                                                    {{--<option value="Dinajpur">Dinajpur</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<input type="hidden" name="status[]" value="1" class="status">--}}

                                {{--</div>--}}
                            {{--</div>--}}


                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<div class="pull-left margin_top margin_bottom"><a href="#" class=" add_field_button">Add More Degree</a></div>--}}
                                    {{--<div class="pull-right ">--}}
                                        {{--<button class="btn btn-primary nextBtn margin_top margin_bottom more_degree_btn add_field_button " type="button" >Next</button>--}}
                                        {{--<input type="hidden" name="status[]" value="1" class="status" >--}}
                                        {{--<input type="submit" name="submit" value="Next" class="btn btn-primary nextBtn margin_top margin_bottom add_field_button">--}}

                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="pull-right ">--}}
                                    {{--<a class="btn btn-primary nextBtn margin_top margin_bottom add_field_button ">Back</a>&nbsp;&nbsp;--}}
                                    {{--<input type="hidden" name="status" value="1" >--}}
                                    {{--<input type="submit" name="submit" value="Next" class="btn btn-primary nextBtn margin_top margin_bottom add_field_button">--}}
                                {{--</div>--}}


                            {{--</div>--}}
                            {{--<br>--}}

                            {{--</div>--}}
                        </div>


                            <!-- /////////////////////////////////// -->

                            <div class="trainer_form_education">
                                <div class="row margin_top">
                                    <div class="col-md-2">
                                        <input type="hidden" name="trainer_pinfo_id[]" value="{{$latest_trainer->id}}" id="trainer_pinfo_id" class="trainer_pinfo_id">
                                        <div class="form-group">
                                            <label class="control-label">Degree</label>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option value=""></option>
                                                    <option value="A'level">SSC/A'level</option>
                                                    <option value="O'level">HSC/O'level</option>
                                                    <option value="Diploma">Diploma</option>
                                                    <option value="Bachelor">Bachelor</option>
                                                    <option value="Master">Master</option>
                                                    <option value="Phd">Phd</option>
                                                    <option value="Mphil">Mphil</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label">Institute</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>
                                                <input id="institute" type="text" class="form-control" name="institute" placeholder="Please Write Your Institute Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label">Department/Group</label>
                                            <div class="form-group">
                                                <select class="form-control" >
                                                    <option value="Please Select Department">Please Select Department</option>
                                                    <option value="Department Name">Department Name</option>
                                                    <option value="Department name">Department name</option>
                                                    <option value="Please Select Department">Please Select Department</option>
                                                    <option value="Department Name">Department Name</option>
                                                    <option value="Department name">Department name</option>
                                                    <option value="Please Select Department">Please Select Department</option>
                                                    <option value="Department Name">Department Name</option>
                                                    <option value="Department name">Department name</option>
                                                    <option value="Please Select Department">Please Select Department</option>
                                                    <option value="Department Name">Department Name</option>
                                                    <option value="Department name">Department name</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>
                                            <input id="result" type="text" placeholder="" class=" form-control" name="result">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Year</label>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option value="2017">2017</option>
                                                    <option value="2016">2016</option>
                                                    <option value="2015">2015</option>
                                                    <option value="2014">2014</option>
                                                    <option value="2013">2013</option>
                                                    <option value="2012">2012</option>
                                                    <option value="2017">2017</option>
                                                    <option value="2016">2016</option>
                                                    <option value="2015">2015</option>
                                                    <option value="2014">2014</option>
                                                    <option value="2013">2013</option>
                                                    <option value="2012">2012</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Board</label>
                                            <div class="form-group">
                                                <select class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="Barisal">Barisal</option>
                                                    <option value="Chittagong">Chittagong</option>
                                                    <option value="Comilla">Comilla</option>
                                                    <option value="Dhaka">Dhaka</option>
                                                    <option value="Jessore">Jessore</option>
                                                    <option value="Rajshahi">Rajshahi</option>
                                                    <option value="Sylhet">Sylhet</option>
                                                    <option value="Dinajpur">Dinajpur</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="pull-left margin_top margin_bottom"><a href="#" class="more_degree_btn">Add More Degree</a></div>
                                <div class="pull-right ">
                                    <!-- <button class="btn btn-primary nextBtn margin_top margin_bottom add_field_button " type="button" >Next</button> -->
                                    <a href="trainer_form-exp.html" class="btn btn-primary margin_top margin_bottom">Next</a>
                                </div>
                            </div>

                            <!-- /////////////////////////////////// -->






                        </div>
                </div>
              <!-- Education close -->



            </div>          
          </div>
          </div>

     </form> <!-- form cosing -->

      <script>
         //alert(5 +
         $(document).ready(function() {
             var max_fields      = 10; //maximum input boxes allowed
             var wrapper         = $(".input_fields_wrap"); //Fields wrapper
             var add_button      = $(".add_field_button"); //Add button ID

             var x = 1; //initlal text box count
             $(add_button).click(function(e){ //on add input button click
                 e.preventDefault();
                 if(x < max_fields){ //max input box allowed
                     x++; //text box increment
//                     $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
                     $(wrapper).append('<div class="trainer_add ">\n' +
                         '                                \n' +
                         '                            <div class=" input_fields_wrap remove_field">\n' +
                         '                                <div class="row margin_top">\n' +
                         '                                    <input type="hidden" name="trainer_pinfo_id[]" value="{{$latest_trainer->id}}" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +
                         '                                    <div class="col-md-2">\n' +
                         '                                        <div class="form-group">\n' +
                         '                                            <label class="control-label">Degree</label>\n' +
                         '                                            <div class="form-group">\n' +
                         '                                                <select name="degree[]" class="form-control degree" required>\n' +
                         '                                                    <option value=""></option>\n' +
                         '                                                    <option value="A\'level">SSC/A\'level</option>\n' +
                         '                                                    <option value="O\'level">HSC/O\'level</option>\n' +
                         '                                                    <option value="Diploma">Diploma</option>\n' +
                         '                                                    <option value="Bachelor">Bachelor</option>\n' +
                         '                                                    <option value="Master">Master</option>\n' +
                         '                                                    <option value="Phd">Phd</option>\n' +
                         '                                                    <option value="Mphil">Mphil</option>\n' +
                         '                                                </select>\n' +
                         '                                            </div>\n' +
                         '                                        </div>\n' +
                         '                                    </div>\n' +
                         '                                    <div class="col-md-5">\n' +
                         '                                        <div class="form-group">\n' +
                         '                                            <label class="control-label">Institute</label>\n' +
                         '                                            <div class="input-group">\n' +
                         '                                                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>\n' +
                         '                                                <input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name">\n' +
                         '                                            </div>\n' +
                         '                                        </div>\n' +
                         '                                    </div>\n' +
                         '                                    <div class="col-md-5">\n' +
                         '                                        <div class="form-group">\n' +
                         '                                            <label class="control-label">Department/Group</label>\n' +
                         '                                            <div class="form-group">\n' +
                         '                                                <select name="department_group[]" id="department_group" class="form-control department_group">\n' +
                         '                                                    <option value="Please Select Department">Please Select Department</option>\n' +
                         '                                                    <option value="Department Name">Department Name</option>\n' +
                         '                                                    <option value="Department name">Department name</option>\n' +
                         '                                                    <option value="Please Select Department">Please Select Department</option>\n' +
                         '                                                    <option value="Department Name">Department Name</option>\n' +
                         '                                                    <option value="Department name">Department name</option>\n' +
                         '                                                    <option value="Please Select Department">Please Select Department</option>\n' +
                         '                                                    <option value="Department Name">Department Name</option>\n' +
                         '                                                    <option value="Department name">Department name</option>\n' +
                         '                                                    <option value="Please Select Department">Please Select Department</option>\n' +
                         '                                                    <option value="Department Name">Department Name</option>\n' +
                         '                                                    <option value="Department name">Department name</option>\n' +
                         '                                                </select>\n' +
                         '                                            </div>\n' +
                         '                                        </div>\n' +
                         '                                    </div>\n' +
                         '                                </div>\n' +
                         '                                <div class="row">\n' +
                         '                                    <div class="col-md-4">\n' +
                         '                                        <div class="form-group">\n' +
                         '                                            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>\n' +
                         '                                            <input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">\n' +
                         '                                        </div>\n' +
                         '                                    </div>\n' +
                         '                                    <div class="col-md-4">\n' +
                         '                                        <div class="form-group">\n' +
                         '                                            <label class="control-label">Year</label>\n' +
                         '                                            <div class="form-group">\n' +
                         '                                                <select name="year[]" class="form-control year">\n' +
                         '                                                    <option value="2017">2017</option>\n' +
                         '                                                    <option value="2016">2016</option>\n' +
                         '                                                    <option value="2015">2015</option>\n' +
                         '                                                    <option value="2014">2014</option>\n' +
                         '                                                    <option value="2013">2013</option>\n' +
                         '                                                    <option value="2012">2012</option>\n' +
                         '                                                    <option value="2017">2017</option>\n' +
                         '                                                    <option value="2016">2016</option>\n' +
                         '                                                    <option value="2015">2015</option>\n' +
                         '                                                    <option value="2014">2014</option>\n' +
                         '                                                    <option value="2013">2013</option>\n' +
                         '                                                    <option value="2012">2012</option>\n' +
                         '                                                </select>\n' +
                         '                                            </div>\n' +
                         '                                        </div>\n' +
                         '                                    </div>\n' +
                         '                                    <div class="col-md-4">\n' +
                         '                                        <div class="form-group">\n' +
                         '                                            <label class="control-label">Board</label>\n' +
                         '                                            <div class="form-group">\n' +
                         '                                                <select name="board[]" class="form-control board">\n' +
                         '                                                    <option value="">Select</option>\n' +
                         '                                                    <option value="Barisal">Barisal</option>\n' +
                         '                                                    <option value="Chittagong">Chittagong</option>\n' +
                         '                                                    <option value="Comilla">Comilla</option>\n' +
                         '                                                    <option value="Dhaka">Dhaka</option>\n' +
                         '                                                    <option value="Jessore">Jessore</option>\n' +
                         '                                                    <option value="Rajshahi">Rajshahi</option>\n' +
                         '                                                    <option value="Sylhet">Sylhet</option>\n' +
                         '                                                    <option value="Dinajpur">Dinajpur</option>\n' +
                         '                                                </select>\n' +
                         '                                            </div>\n' +
                         '                                        </div>\n' +
                         '                                    </div>\n' +
                         '                                    {{--<input type="hidden" name="status[]" value="1" class="status">--}}\n' +
                         '\n' +
                         '                                </div>\n' +
                         '                            </div>\n' +
                         '\n' +
                         '\n' +
                         '                            <div class="row">\n' +
                         '                                <div class="col-md-12">\n' +
                         '                                    <div class="pull-left margin_top margin_bottom"><a href="#" class=" add_field_button">Add More Degree</a></div>\n' +
                         '                                    <div class="pull-right ">\n' +
                         '                                        {{--<button class="btn btn-primary nextBtn margin_top margin_bottom more_degree_btn add_field_button " type="button" >Next</button>--}}\n' +
                         '                                        <input type="hidden" name="status[]" value="1" class="status" >\n' +
                         '                                        <input type="submit" name="submit" value="Next" class="btn btn-primary nextBtn margin_top margin_bottom add_field_button">\n' +
                         '\n' +
                         '                                    </div>\n' +
                         '                                </div>\n' +
                         '\n' +
                         '                                {{--<div class="pull-right ">--}}\n' +
                         '                                    {{--<a class="btn btn-primary nextBtn margin_top margin_bottom add_field_button ">Back</a>&nbsp;&nbsp;--}}\n' +
                         '                                    {{--<input type="hidden" name="status" value="1" >--}}\n' +
                         '                                    {{--<input type="submit" name="submit" value="Next" class="btn btn-primary nextBtn margin_top margin_bottom add_field_button">--}}\n' +
                         '                                {{--</div>--}}\n' +
                         '\n' +
                         '\n' +
                         '                            </div>\n' +
                         '                            <br>\n' +
                         '                                \n' +
                         '                            </div>'); //add input box
                 }
             });

             $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                 e.preventDefault(); $(this).parent('div').remove(); x--;
             })
         });


      </script>

  </div>
</div>
      <!-- === trainer_form  end === -->


      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- //////////////////////====================================/////////////////////////// -->

<!--  -->
@include('layouts.show_page_footer')
