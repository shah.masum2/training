-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2017 at 01:25 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_deadline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_about` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_object` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_others` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_who_attend` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_title`, `c_image`, `c_date`, `c_time`, `c_type`, `c_deadline`, `c_about`, `c_object`, `c_others`, `c_who_attend`, `created_at`, `updated_at`) VALUES
(1, 'JAVA & SPRING FRAMEWORK', '', '11-11-2017', '9:30-05:30pm', 'IT NEW WORK', '15-11-2017', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut cupiditate ipsam, omnis ratione reiciendis veritatis.', 'sit amet, consectetur adipisicing elit. Aut cupiditate ipsam, omnis ratione reiciendis veritatis.\r\n', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut cupiditate ipsam, omnis ratione reiciendis veritatis.\r\n', 'Lorem ipsum dolor \r\nsit amet, consectetur \r\nadipisicing elit. Aut \r\ncupiditate ipsam, \r\nomnis ratione reiciendis veritatis.\r\n', '2017-11-09 04:15:10', '2017-11-09 06:17:15'),
(2, 'Database & Banking', '', '10-11-2018', '6:00pm', 'Bank Management', '19-11-2018', 'lsdjlghslg sdhlgkh sd', 'lkdshag hdsghhglhal salg', 'sldhgk ghiotoitiowt towti', 'Banker Person', NULL, NULL),
(3, 'PHP & Web Development', 'Web Design & Framework', '20-10-2017', '9:00-6:00 PM', 'ICT Management', '25-12-2017', 'This aims to make the trainees well equipped with these technologies so that they can leverage these skills for building their own web applications.', 'This aims to make the trainees well equipped with these technologies so that they can leverage these skills for building their own web applications.', 'This aims to make the trainees well equipped with these technologies so that they can leverage these skills for building their own web applications.', 'This aims to make the trainees well equipped with these\r\ntechnologies so \r\nthat they can \r\nleverage these skills for \r\nbuilding their own web applications.', '2017-11-15 02:16:11', '2017-11-23 00:16:32'),
(4, 'Networking', '', '30-11-2017', '10:00 - 6:00', 'Network & Hardware', '30-12-207', 'This PHP MySQL training is recommended for professionals and students who wish to learn web development and existing developers who want to expand their skill set.', 'This PHP MySQL training is recommended for professionals and students who wish to learn web development and existing developers who want to expand their skill set.', 'This PHP MySQL training is recommended for professionals and students who wish to learn web development and existing developers who want to expand their skill set.', 'This PHP MySQL training is ', '2017-11-22 01:17:00', '2017-11-29 01:19:00');

-- --------------------------------------------------------

--
-- Table structure for table `fronts`
--

CREATE TABLE `fronts` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_deadline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_about` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_object` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_others` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_who_attend` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_05_045852_create_training_regis_table', 1),
(4, '2017_11_08_181817_create_courses_table', 2),
(7, '2017_11_10_111035_create_fronts_table', 3),
(8, '2017_11_11_201752_create_admins_table', 3),
(9, '2017_11_13_064012_create_trai_regi_displays_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training_regis`
--

CREATE TABLE `training_regis` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_course` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `venue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_regis`
--

INSERT INTO `training_regis` (`id`, `first_name`, `last_name`, `email`, `phone`, `interest_course`, `gender`, `organization`, `designation`, `venue`, `username`, `password`, `confirm`, `created_at`, `updated_at`) VALUES
(26, 'Ayeasha', 'dfffffff', 'idb.shah@gmail.com', '0182548888', 'JAVA', 'Female', 'rtuyiu', 'Frontend Designer', 'Banani', 'uuuuuuuuuuu', '333333333', '333333333333', '2017-11-13 01:17:34', '2017-11-13 01:17:34'),
(27, 'Ayeasha', 'dfffffff', 'idb.shah@gmail.com', '0182548888', 'JAVA', 'Female', 'rtuyiu', 'Frontend Designer', 'Banani', 'uuuuuuuuuuu', '111111111', '1111111111', '2017-11-13 01:18:00', '2017-11-13 01:18:00'),
(28, 'Karim', 'Hossain', 'admin@gmail.com', '017285544', 'ASP.NET', 'Female', 'Datavision Traininh', 'Frontend Designer', 'Banani', 'test2', 'ddddddd', 'ddddddddd', '2017-11-13 01:20:43', '2017-11-13 01:20:43'),
(29, 'Karim', 'Hossain', 'admin@gmail.com', '017285544', 'ASP.NET', 'Female', 'Datavision Traininh', 'Frontend Designer', 'Banani', 'test2', '22222222222', '222222222222222', '2017-11-13 01:21:25', '2017-11-13 01:21:25'),
(30, 'Ayeasha', 'test2', 'admin@gmail.com', '0182548888', 'JAVA', 'Male', 'Datavision', 'lkdsglkh', 'Banani', 'ayeasha1', '1111111111111', '111111111111111', '2017-11-13 02:32:06', '2017-11-13 02:32:06'),
(31, 'Kamal', 'Hossain', 'kamal@gmail.com', '923983489', 'DATABASE', 'Male', 'DVTraining', 'Manager', 'Banani', 'Anwar Hossain', '11111111', '11111111', '2017-11-13 04:38:18', '2017-11-13 04:38:18'),
(32, 'Kamal', 'Hossain', 'anwarmsc70@gmail.com', '923983489', 'ASP.NET', 'Female', 'DVTraining', 'Manager', 'Banani', 'kamal', '22222222222', '22222222222222', '2017-11-13 04:40:14', '2017-11-13 04:40:14'),
(33, 'Kamal', 'Hossain', 'anwarmsc70@gmail.com', '923983489', 'ASP.NET', 'Female', 'DVTraining', 'Manager', 'Banani', 'kamal', '55555555', '55555555', '2017-11-13 04:42:47', '2017-11-13 04:42:47');

-- --------------------------------------------------------

--
-- Table structure for table `trai_regi_displays`
--

CREATE TABLE `trai_regi_displays` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shah', 'shah.masum2@gmail.com', '$2y$10$aOR.e/sJa.Py2DGxkueSduMez/Qpv3uw8qjKDgvxVKCBAV.SrtlFW', 'kYQHIa3YfqrCYhe0fRgzNyzF8ivDdJHsCYue6040MdLeTMWwdd8xN9SIu3hb', '2017-11-11 23:39:57', '2017-11-11 23:39:57'),
(2, 'admin', 'admin@gmail.com', '$2y$10$0GOlrkiuDwRs/zLC5PbkBu4.ytnYiLjkzX8AXktc/pmLYOHjsvH5W', 'Yo07nHG39W1lZ3iqJth4ymuJ9JIC3yY7eGpT9cWVMk0Q3HBPg71rwaRXw4vW', '2017-11-12 02:31:57', '2017-11-12 02:31:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fronts`
--
ALTER TABLE `fronts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `training_regis`
--
ALTER TABLE `training_regis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trai_regi_displays`
--
ALTER TABLE `trai_regi_displays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fronts`
--
ALTER TABLE `fronts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `training_regis`
--
ALTER TABLE `training_regis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `trai_regi_displays`
--
ALTER TABLE `trai_regi_displays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
