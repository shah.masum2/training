-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2017 at 02:05 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training`
--

-- --------------------------------------------------------

--
-- Table structure for table `admincourses`
--

CREATE TABLE `admincourses` (
  `id` int(10) UNSIGNED NOT NULL,
  `c_category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_class_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_deadline` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_about` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_object` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_others` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_who_attend` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admincourses`
--

INSERT INTO `admincourses` (`id`, `c_category_id`, `course_title`, `c_image`, `c_date`, `c_class_time`, `c_type`, `c_deadline`, `c_about`, `c_object`, `c_others`, `c_who_attend`, `created_at`, `updated_at`) VALUES
(1, 'ICT', 'Web Development', '', '15/12/207', '9:50 - 6:30 PM', 'Information & Communicatin', '25/01/2018', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, nam!\r\n', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, nam!\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, nam!\r\n', 'Consectetur adipisicing elit. Harum, nam!\r\n', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, nam!\r\n', '2017-11-15 02:12:12', '2017-11-28 23:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `c_category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_class_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_deadline` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_about` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_object` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_others` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_who_attend` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `c_category_id`, `course_title`, `c_image`, `c_date`, `c_class_time`, `c_type`, `c_deadline`, `c_about`, `c_object`, `c_others`, `c_who_attend`, `created_at`, `updated_at`) VALUES
(1, '', 'PHP Framwork Laravel', '', '', '', '', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fronts`
--

CREATE TABLE `fronts` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_deadline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_about` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_object` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_others` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_who_attend` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_05_045852_create_training_regis_table', 1),
(7, '2017_11_10_111035_create_fronts_table', 3),
(8, '2017_11_11_201752_create_admins_table', 3),
(12, '2014_10_12_000000_create_users_table', 5),
(13, '2017_11_13_064012_create_trai_regi_displays_table', 6),
(14, '2017_11_13_110945_create_training_confirmations_table', 7),
(16, '2017_11_14_112017_create_paybkashes_table', 8),
(17, '2017_11_08_181817_create_courses_table', 9),
(18, '2017_11_18_111151_create_admincourses_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `paybkashes`
--

CREATE TABLE `paybkashes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training_confirmations`
--

CREATE TABLE `training_confirmations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training_regis`
--

CREATE TABLE `training_regis` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_course` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `venue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trai_regi_displays`
--

CREATE TABLE `trai_regi_displays` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `interest_course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `venue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `interest_course`, `gender`, `organization`, `designation`, `venue`, `user_id`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Masum', 'Shah', 'shah.masum2@gmail.com', '012454545', 'ASP.NET', 'Male', 'Datavision', 'Web Developer', 'Banani', 'shah11', '$2y$10$fLj2OebrD5RBIslOVhmhgeAavwGQ43nobEL4lUC15TeyowmBoEq7W', 'IkRgEyurerpP7Ax5sWNLlDy9hY2nQr81WNMuUtRshfkOEPua6RwwFTnzeQeE', '2017-11-14 23:27:20', '2017-11-14 23:27:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admincourses`
--
ALTER TABLE `admincourses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fronts`
--
ALTER TABLE `fronts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `paybkashes`
--
ALTER TABLE `paybkashes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `paybkashes_transaction_id_unique` (`transaction_id`),
  ADD UNIQUE KEY `paybkashes_mobile_unique` (`mobile`);

--
-- Indexes for table `training_confirmations`
--
ALTER TABLE `training_confirmations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_regis`
--
ALTER TABLE `training_regis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trai_regi_displays`
--
ALTER TABLE `trai_regi_displays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_user_id_unique` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admincourses`
--
ALTER TABLE `admincourses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fronts`
--
ALTER TABLE `fronts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `paybkashes`
--
ALTER TABLE `paybkashes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `training_confirmations`
--
ALTER TABLE `training_confirmations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `training_regis`
--
ALTER TABLE `training_regis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `trai_regi_displays`
--
ALTER TABLE `trai_regi_displays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
