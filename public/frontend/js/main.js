

/*======  nav sticky start ===== */

$(document).ready(function(){
    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky")
        } else {
            navbar.classList.remove("sticky");
        }
    }
});
/*======  nav sticky End ===== */

/*====== nav start ===== */



/*====== nav End ===== */
$(document).ready(function(){
    var navigation = responsiveNav("#nav", {
        insert: "before"
    });
});

$(document).ready(function(){
    $(".a").click(function(){
        $(".all").show(1000);
        $(".wd").hide(1000);
        $(".gd").hide(1000);
    });
    $(".b").click(function(){
        $(".all").hide(1000);
        $(".wd").show(1000);
        $(".gd").hide(1000);
    });
    $(".c").click(function(){
        $(".all").hide(1000);
        $(".wd").hide(1000);
        $(".gd").show(1000);
    });

    $(".btn").on("click", function(){
        $(this).css("background-color", "#fff");
        $(this).css("color", "#000");
    });
});


/*====== country_checkbox start===== */

$(document).ready(function(){
    $(function() {
        enable_cb();
        $(".country_check").click(enable_cb);
    });

    function enable_cb() {
        if (this.checked) {
            $("input.other_country_check").attr("disabled", true);

        } else {
            $("input.other_country_check").removeAttr("disabled");
        }
    }
});

/*====== country_checkbox end===== */

/*====== time_duration_checkbox start===== */

$(document).ready(function(){
    $(function() {
        enable_cb();
        $("#myCheck").click(enable_cb);
    });

    function enable_cb() {
        if (this.checked) {
            $("input#mySelect").attr("disabled", true);

        } else {
            $("input#mySelect").removeAttr("disabled");
        }
    }
});

/*====== time_duration_checkbox end===== */

/*====== grad_form start===== */

/*====== grad_form end===== */

/*====== trainer_form_education start===== */
//final for done justified by Masum
$(document).ready(function(){
    $('.more_degree_btn').on('click',function(){
        addform();
    });

    function addform()
    {
        var dynamic_id = $('#trainer_pinfo_id').val() ;
        //alert(dynamic_id);
        var educationform=' <div id="trainer_form_education">\n' +
            '            <hr>'+
            '                <div class="row margin_top">\n'+
            '                    <input type="hidden" name="trainer_pinfo_id[]" value="'+dynamic_id+'" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +
            // '                    <input type="hidden" name="trainer_pinfo_id[]" value="{{$latest_trainer->id}}" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +
            '                    <div class="col-md-2">\n' +
            '                        <div class="form-group">\n' +
            '                            <label class="control-label">Degree</label>\n' +
            '                            <div class="form-group">\n' +
            '                                <select name="degree[]" class="form-control degree" required>\n' +
            '                                    <option value="">Select Degree</option>\n' +
            '                                    <option value="A\'level">SSC/A\'level</option>\n' +
            '                                    <option value="O\'level">HSC/O\'level</option>\n' +
            '                                    <option value="Diploma">Diploma</option>\n' +
            '                                    <option value="Bachelor">Bachelor</option>\n' +
            '                                    <option value="Master">Master</option>\n' +
            '                                    <option value="Phd">Phd</option>\n' +
            '                                    <option value="Mphil">Mphil</option>\n' +
            '                                </select>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <div class="col-md-5">\n' +
            '                        <div class="form-group">\n' +
            '                            <label class="control-label">Institute</label>\n' +
            '                            <div class="input-group">\n' +
            '                                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>\n' +
            '                                <input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name">\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <div class="col-md-5">\n' +
            '                        <div class="form-group">\n' +
            '                            <label class="control-label">Department/Group</label>\n' +
            '                            <div class="form-group">\n' +
            '                                <select name="department_group[]" id="department_group" class="form-control department_group">\n' +
            '                                    <option value="Please Select Department">Please Select Department</option>\n' +
            '                                    <option value="Department Name">Department Name</option>\n' +
            '                                    <option value="Department name">Department name</option>\n' +
            '                                    <option value="Please Select Department">Please Select Department</option>\n' +
            '                                    <option value="Department Name">Department Name</option>\n' +
            '                                    <option value="Department name">Department name</option>\n' +
            '                                    <option value="Please Select Department">Please Select Department</option>\n' +
            '                                    <option value="Department Name">Department Name</option>\n' +
            '                                    <option value="Department name">Department name</option>\n' +
            '                                    <option value="Please Select Department">Please Select Department</option>\n' +
            '                                    <option value="Department Name">Department Name</option>\n' +
            '                                    <option value="Department name">Department name</option>\n' +
            '                                </select>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                <div class="row">\n' +
            '                    <div class="col-md-4">\n' +
            '                        <div class="form-group">\n' +
            '                            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>\n' +
            '                            <input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <div class="col-md-4">\n' +
            '                        <div class="form-group">\n' +
            '                            <label class="control-label">Year</label>\n' +
            '                            <div class="form-group">\n' +
            '                                <select name="year[]" class="form-control year">\n' +
            '                                    <option value="2017">2017</option>\n' +
            '                                    <option value="2016">2016</option>\n' +
            '                                    <option value="2015">2015</option>\n' +
            '                                    <option value="2014">2014</option>\n' +
            '                                    <option value="2013">2013</option>\n' +
            '                                    <option value="2012">2012</option>\n' +
            '                                    <option value="2017">2017</option>\n' +
            '                                    <option value="2016">2016</option>\n' +
            '                                    <option value="2015">2015</option>\n' +
            '                                    <option value="2014">2014</option>\n' +
            '                                    <option value="2013">2013</option>\n' +
            '                                    <option value="2012">2012</option>\n' +
            '                                </select>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <div class="col-md-4">\n' +
            '                        <div class="form-group">\n' +
            '                            <label class="control-label">Board</label>\n' +
            '                            <div class="form-group">\n' +
            '                                <select name="board[]" class="form-control board">\n' +
            '                                    <option value="">Select</option>\n' +
            '                                    <option value="Barisal">Barisal</option>\n' +
            '                                    <option value="Chittagong">Chittagong</option>\n' +
            '                                    <option value="Comilla">Comilla</option>\n' +
            '                                    <option value="Dhaka">Dhaka</option>\n' +
            '                                    <option value="Jessore">Jessore</option>\n' +
            '                                    <option value="Rajshahi">Rajshahi</option>\n' +
            '                                    <option value="Sylhet">Sylhet</option>\n' +
            '                                    <option value="Dinajpur">Dinajpur</option>\n' +
            '                                </select>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <input type="hidden" name="status[]" value="1" class="status">\n' +
            '\n' +
            '                </div>\n' +
            '                <button class="rmv_btn btn btn-defult text-danger" style=" ">Remove </button>\n' +
            '            </div>';


        $('#trainer_form_education').append(educationform);
    };
    $('body').delegate('.rmv_edubtn','click', function(){
        $(this).parent().remove();
    });
});
/*====== trainer_form_education end===== */
// <div id="trainer_form_experience">'+ //<br><hr><a href="#" class="rmv_btn pull-right">Remove</a><br>
/*====== more_experience_btn start===== */
//
