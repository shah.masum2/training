

/*======  nav sticky start ===== */

$(document).ready(function(){
    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset >= sticky) {
            navbar.classList.add("sticky")
        } else {
            navbar.classList.remove("sticky");
        }
    }
});
/*======  nav sticky End ===== */

/*====== nav start ===== */



/*====== nav End ===== */
$(document).ready(function(){
    var navigation = responsiveNav("#nav", {
        insert: "before"
    });
});

$(document).ready(function(){
    $(".a").click(function(){
        $(".all").show(1000);
        $(".wd").hide(1000);
        $(".gd").hide(1000);
    });
    $(".b").click(function(){
        $(".all").hide(1000);
        $(".wd").show(1000);
        $(".gd").hide(1000);
    });
    $(".c").click(function(){
        $(".all").hide(1000);
        $(".wd").hide(1000);
        $(".gd").show(1000);
    });

    $(".btn").on("click", function(){
        $(this).css("background-color", "#fff");
        $(this).css("color", "#000");
    });
});

$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

/*====== country_checkbox start===== */

$(document).ready(function(){
    $(function() {
        enable_cb();
        $(".country_check").click(enable_cb);
    });

    function enable_cb() {
        if (this.checked) {
            $("input.other_country_check").attr("disabled", true);

        } else {
            $("input.other_country_check").removeAttr("disabled");
        }
    }
});

/*====== country_checkbox end===== */

/*====== time_duration_checkbox start===== */

$(document).ready(function(){
    $(function() {
        enable_cb();
        $(".myCheck").click(enable_cb);
    });

    function enable_cb() {
        if (this.checked) {
            $("input.mySelect").attr("disabled", true);

        } else {
            $("input.mySelect").removeAttr("disabled");
        }
    }
});





// <script type="text/javascript">

/*====== trainer_form_education start===== */

$(document).ready(function(){
    $('.more_degree_btn').on('click',function(){
        addform();
    });
    function addform()
    {
        var educationform='<div class="trainer_form_education">\n' +
            '                                <div class="row margin_top">\n' +
            '                                    <input type="hidden" name="trainer_pinfo_id[]" value="{!! $latest_trainer->id !!}" id="trainer_pinfo_id" class="trainer_pinfo_id">\n' +
            '                                    <div class="col-md-2">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label class="control-label">Degree</label>\n' +
            '                                            <div class="form-group">\n' +
            '                                                <select name="degree[]" class="form-control degree" required>\n' +
            '                                                    <option value=""></option>\n' +
            '                                                    <option value="A\'level">SSC/A\'level</option>\n' +
            '                                                    <option value="O\'level">HSC/O\'level</option>\n' +
            '                                                    <option value="Diploma">Diploma</option>\n' +
            '                                                    <option value="Bachelor">Bachelor</option>\n' +
            '                                                    <option value="Master">Master</option>\n' +
            '                                                    <option value="Phd">Phd</option>\n' +
            '                                                    <option value="Mphil">Mphil</option>\n' +
            '                                                </select>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-5">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label class="control-label">Institute</label>\n' +
            '                                            <div class="input-group">\n' +
            '                                                <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>\n' +
            '                                                <input id="institute" type="text" class="form-control institute" name="institute[]" placeholder="Please Write Your Institute Name">\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-5">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label class="control-label">Department/Group</label>\n' +
            '                                            <div class="form-group">\n' +
            '                                                <select name="department_group[]" id="department_group" class="form-control department_group">\n' +
            '                                                    <option value="Please Select Department">Please Select Department</option>\n' +
            '                                                    <option value="Department Name">Department Name</option>\n' +
            '                                                    <option value="Department name">Department name</option>\n' +
            '                                                    <option value="Please Select Department">Please Select Department</option>\n' +
            '                                                    <option value="Department Name">Department Name</option>\n' +
            '                                                    <option value="Department name">Department name</option>\n' +
            '                                                    <option value="Please Select Department">Please Select Department</option>\n' +
            '                                                    <option value="Department Name">Department Name</option>\n' +
            '                                                    <option value="Department name">Department name</option>\n' +
            '                                                    <option value="Please Select Department">Please Select Department</option>\n' +
            '                                                    <option value="Department Name">Department Name</option>\n' +
            '                                                    <option value="Department name">Department name</option>\n' +
            '                                                </select>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="row">\n' +
            '                                    <div class="col-md-4">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label class="control-label">Result <small style="color: #CCC">(Optional)</small></label>\n' +
            '                                            <input id="result" type="text" class="form-control" name="result[]" placeholder="CGPA/Division">\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-4">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label class="control-label">Year</label>\n' +
            '                                            <div class="form-group">\n' +
            '                                                <select name="year[]" class="form-control year">\n' +
            '                                                    <option value="2017">2017</option>\n' +
            '                                                    <option value="2016">2016</option>\n' +
            '                                                    <option value="2015">2015</option>\n' +
            '                                                    <option value="2014">2014</option>\n' +
            '                                                    <option value="2013">2013</option>\n' +
            '                                                    <option value="2012">2012</option>\n' +
            '                                                    <option value="2017">2017</option>\n' +
            '                                                    <option value="2016">2016</option>\n' +
            '                                                    <option value="2015">2015</option>\n' +
            '                                                    <option value="2014">2014</option>\n' +
            '                                                    <option value="2013">2013</option>\n' +
            '                                                    <option value="2012">2012</option>\n' +
            '                                                </select>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-4">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label class="control-label">Board</label>\n' +
            '                                            <div class="form-group">\n' +
            '                                                <select name="board[]" class="form-control board">\n' +
            '                                                    <option value="">Select</option>\n' +
            '                                                    <option value="Barisal">Barisal</option>\n' +
            '                                                    <option value="Chittagong">Chittagong</option>\n' +
            '                                                    <option value="Comilla">Comilla</option>\n' +
            '                                                    <option value="Dhaka">Dhaka</option>\n' +
            '                                                    <option value="Jessore">Jessore</option>\n' +
            '                                                    <option value="Rajshahi">Rajshahi</option>\n' +
            '                                                    <option value="Sylhet">Sylhet</option>\n' +
            '                                                    <option value="Dinajpur">Dinajpur</option>\n' +
            '                                                </select>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <input type="hidden" name="status[]" value="1" class="status">\n' +
            '\n' +
            '                                </div>\n' +
            '                            </div>';


        $('.trainer_form_education').append(educationform);
    };
    $('body').delegate('.rmv_edubtn','click', function(){
        $(this).parent().remove();
    });
});
/*====== trainer_form_education end===== */













// </script>

<!-- addRow -->


// //============================ ////////Masum///////////==============================
//
// $(document).ready(function() {
//     var max_fields      = 6; //maximum input boxes allowed
//     var wrapper         = $(".input_fields_wrap"); //Fields wrapper
//     var add_button      = $(".add_field_button"); //Add button ID
//
//     var x = 1; //initlal text box count
//     $(add_button).click(function(e){ //on add input button click
//         e.preventDefault();
//         if(x < max_fields){ //max input box allowed
//             x++; //text box increment
//             $(wrapper).append('  <div class="trainer_form_education ">\n' +
//                 '                            <div class="row margin_top">\n' +
//                 '                                <div class="col-md-2">\n' +
//                 '\n' +
//                 '                                    <div class="form-group">\n' +
//                 '                                        <label class="control-label">Degree</label>\n' +
//                 '                                        <div class="form-group">\n' +
//                 '                                            <select name="degree" class="form-control" required>\n' +
//                 '                                                <option value="">Select</option>\n' +
//                 '                                                <option value="SSC/O\'level">SSC/O\'level </option>\n' +
//                 '                                                <option value="HSC/A\'level">HSC/A\'level</option>\n' +
//                 '                                                <option value="Diploma">Diploma</option>\n' +
//                 '                                                <option value="Bachelor">Bachelor</option>\n' +
//                 '                                                <option value="Master">Master</option>\n' +
//                 '                                                <option value="Phd">Phd</option>\n' +
//                 '                                                <option value="Mphil">Mphil</option>\n' +
//                 '                                            </select>\n' +
//                 '                                        </div>\n' +
//                 '                                    </div>\n' +
//                 '                                </div>\n' +
//                 '\n' +
//                 '                                <div class="col-md-5">\n' +
//                 '                                    <div class="form-group">\n' +
//                 '                                        <label class="control-label">Institute</label>\n' +
//                 '                                        <div class="input-group">\n' +
//                 '                                            <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span>\n' +
//                 '                                            <input id="institute" type="text"  class="form-control" name="institute" placeholder="Please Write Your Institute Name" required>\n' +
//                 '                                        </div>\n' +
//                 '                                    </div>\n' +
//                 '                                </div>\n' +
//                 '\n' +
//                 '                                <div class="col-md-5">\n' +
//                 '                                    <div class="form-group">\n' +
//                 '                                        <label class="control-label">Department/Group</label>\n' +
//                 '                                        <div class="form-group">\n' +
//                 '                                            <select name="department_group" id="department_group" class="form-control">\n' +
//                 '                                                <option value="">Please Select Department</option>\n' +
//                 '                                                <option value="Department Name">Department Name</option>\n' +
//                 '                                                <option value="Department Name">Department name</option>\n' +
//                 '                                                <option value="Department Name">Please Select Department</option>\n' +
//                 '                                                <option value="Department Name">Department Name</option>\n' +
//                 '                                                <option value="Department Name">Department name</option>\n' +
//                 '                                                <option value="Department Name">Please Select Department</option>\n' +
//                 '                                                <option value="Department Name">Department Name</option>\n' +
//                 '                                                <option value="Department Name">Department name</option>\n' +
//                 '                                                <option value="Department Name">Please Select Department</option>\n' +
//                 '                                                <option value="Department Name">Department Name</option>\n' +
//                 '                                                <option value="Department Name">Department name</option>\n' +
//                 '                                            </select>\n' +
//                 '                                        </div>\n' +
//                 '                                    </div>\n' +
//                 '                                </div>\n' +
//                 '                            </div>\n' +
//                 '                            <div class="row">\n' +
//                 '                                <div class="col-md-4">\n' +
//                 '                                    <div class="form-group">\n' +
//                 '                                        <label class="control-label">Result <small style="color: #bcc7cc" >(Optional)</small> </label>\n' +
//                 '                                        <div class="form-group">\n' +
//                 '                                            <input id="result" type="text" class="form-control" name="result" placeholder="CGPA/Division">\n' +
//                 '                                        </div>\n' +
//                 '                                    </div>\n' +
//                 '                                </div>\n' +
//                 '                                <div class="col-md-4">\n' +
//                 '                                    <div class="form-group">\n' +
//                 '                                        <label class="control-label">Year</label>\n' +
//                 '                                        <div class="form-group">\n' +
//                 '                                            <select name="year" class="form-control">\n' +
//                 '                                                <option value="2017">2017</option>\n' +
//                 '                                                <option value="2016">2016</option>\n' +
//                 '                                                <option value="2015">2015</option>\n' +
//                 '                                                <option value="2014">2014</option>\n' +
//                 '                                                <option value="2013">2013</option>\n' +
//                 '                                                <option value="2012">2012</option>\n' +
//                 '                                                <option value="2017">2017</option>\n' +
//                 '                                                <option value="2016">2016</option>\n' +
//                 '                                                <option value="2015">2015</option>\n' +
//                 '                                                <option value="2014">2014</option>\n' +
//                 '                                                <option value="2013">2013</option>\n' +
//                 '                                                <option value="2012">2012</option>\n' +
//                 '                                            </select>\n' +
//                 '                                        </div>\n' +
//                 '                                    </div>\n' +
//                 '                                </div>\n' +
//                 '\n' +
//                 '                                <div class="col-md-4">\n' +
//                 '                                    <div class="form-group">\n' +
//                 '                                        <label class="control-label">Board</label>\n' +
//                 '                                        <div class="form-group">\n' +
//                 '                                            <select name="board" class="form-control">\n' +
//                 '                                                <option value="">Select</option>\n' +
//                 '                                                <option value="Barisal">Barisal</option>\n' +
//                 '                                                <option value="Chittagong">Chittagong</option>\n' +
//                 '                                                <option value="Comilla">Comilla</option>\n' +
//                 '                                                <option value="Dhaka">Dhaka</option>\n' +
//                 '                                                <option value="Jessore">Jessore</option>\n' +
//                 '                                                <option value="Rajshahi">Rajshahi</option>\n' +
//                 '                                                <option value="Sylhet">Sylhet</option>\n' +
//                 '                                                <option value="Dinajpur">Dinajpur</option>\n' +
//                 '                                            </select>\n' +
//                 '                                        </div>\n' +
//                 '                                    </div>\n' +
//                 '                                </div>\n' +
//                 '                            </div>\n' +
//                 '                        </div>\n'+
//                 '<a href="#" class="remove_field">Remove</a>'
//                 ); //add input box
//         }
//     });
//
//     $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
//         e.preventDefault(); $(this).parent().parent('div').remove(); x--;
//     })
// });
//
