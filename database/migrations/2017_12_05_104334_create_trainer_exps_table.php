<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainerExpsTable extends Migration
{

    public function up()
    {
        Schema::create('trainer_exps', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('trainer_pinfo_id')->unsigned();
            $table->foreign('trainer_pinfo_id')->references('id')->on('trainer_pinfos')->onDelete('cascade');
/*designation_title com_name com_department com_location timefrom timeto continue*/
//            $table->integer('trainer_pinfo_id');
            $table->string('designation_title');
            $table->string('com_name');
            $table->string('com_department')->nullable();
            $table->string('com_location');
            $table->string('timefrom')->nullable();
            $table->string('timeto')->nullable();
            $table->string('continue')->nullable();


            $table->boolean('status');// to soft delete purposes added this column
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainer_exps');
    }
}
