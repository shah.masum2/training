<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainerAccsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainer_accs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trainer_pinfo_id')->unsigned();
            $table->foreign('trainer_pinfo_id')->references('id')->on('trainer_pinfos');//->onDelete('cascade')

            $table->string('email');
            $table->string('password');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainer_accs');
    }
}
