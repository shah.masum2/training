<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainerEdusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainer_edus', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('trainer_pinfo_id')->unsigned();
            $table->foreign('trainer_pinfo_id')->references('id')->on('trainer_pinfos')->onDelete('cascade');
            $table->string('degree');
            $table->string('institute');
            $table->string('department_group');
            $table->integer('result');
            $table->integer('year');
            $table->string('board');

            $table->boolean('status');// to soft delete purposes added this column
            $table->softDeletes();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainer_edus');
    }
}
