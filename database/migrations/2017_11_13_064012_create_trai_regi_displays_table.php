<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraiRegiDisplaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trai_regi_displays', function (Blueprint $table) {
            $table->increments('id');
//            $table->integer('training_regi_id')->unsigned();
//            $table->foreign('training_regi_id')->references('id')->on('training_regis')->onDelete('cascade');//->onDelete("cascade")
//            $table->string('interest_course');
//            $table->string('venue');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');//->onDelete("cascade")

            $table->string('subject_id');
            $table->string('course_title');
            $table->string('course_fee');
            $table->string('course_venue');

          /*
            user_id" val
            subject_id"
            course_title
             course_fee"
                course_venue
           * */


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trai_regi_displays');
    }
}
