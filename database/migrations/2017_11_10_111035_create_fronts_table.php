<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrontsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fronts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('course_title','255');
            $table->string('c_image','255');
            $table->string('c_date','255');
            $table->string('c_time','255');
            $table->string('c_type','255');
            $table->string('c_deadline','255');
            $table->string('c_about','255');
            $table->string('c_object','255');
            $table->string('c_others','255');
            $table->string('c_who_attend','255');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fronts');
    }
}
