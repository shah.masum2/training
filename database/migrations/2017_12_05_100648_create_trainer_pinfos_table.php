<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainerPinfosTable extends Migration
{
    /**
     * Run the migrations.
     *photo
    date of birth
    gender
    religion
    mobile
    nationality
    marital status
    address
     * @return void
     */
    public function up()
    {
        Schema::create('trainer_pinfos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('photo');
            $table->string('date_of_birth');
            $table->string('gender');
            $table->string('religion');
            $table->integer('mobile');
            $table->string('nationality')->nullable();
            $table->string('marital_status');
            $table->text('address');


            $table->boolean('status');// to soft delete purposes added this column
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainer_pinfos');
    }
}
