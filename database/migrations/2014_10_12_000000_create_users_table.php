<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
//            original Table column
//            $table->string('name','50');
//            $table->string('email','50')->unique();
//            $table->string('password','50');
//            original table column close here


            //custom table column field
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('interest_course');
            $table->string('gender');
            $table->string('organization');
            $table->string('designation');
            $table->string('venue');

////            for confirmation two columns bellow
//            $table->boolean('confirmed')->default(0);
//            $table->string('token',254)->nullable();
////            for confirmation two columns up

            //For username and password creation
            $table->string('user_id')->unique();
            $table->string('password');


//            $table->string('status');



            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
