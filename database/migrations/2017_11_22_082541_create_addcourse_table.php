<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddcourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addcourse', function (Blueprint $table) {
            $table->increments('id');

            $table->string('course_category_id');
            $table->string('course_title');
            $table->string('course_image');
            $table->string('course_date');
            $table->string('course_class_time');
            $table->string('course_type');
            $table->string('course_deadline');
            $table->string('course_about');
            $table->string('course_object');
            $table->string('course_others');
            $table->string('course_who_attend');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addcourse');
    }
}
