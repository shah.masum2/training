<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');


            $table->string('c_category_id');
            $table->string('course_title');
            $table->string('c_image');
            $table->string('c_date');
            $table->string('c_class_time');
            $table->string('c_type');
            $table->string('c_deadline');
            $table->string('c_about');
            $table->string('c_object');
            $table->string('c_others');
            $table->string('c_who_attend');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
