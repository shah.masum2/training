<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');

            $table->string('course_category_id');
//            added later
            $table->string('course_module_type'); //workshop or certification course
//            added later
            $table->string('course_title');
            $table->string('course_image');
            $table->string('course_date');
            $table->string('course_class_time');
            //added later
            $table->integer('course_fee');
            $table->string('course_total_classes');
            $table->string('course_total_duration');
            $table->string('course_venue');
            // added later up
            $table->string('course_type');
            $table->string('course_deadline');
            $table->text('course_about');
            $table->text('course_object');
            $table->text('course_others');
            $table->text('course_who_attend');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
