<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingRegisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_regis', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('first_name','255');
            $table->string('last_name','255');
            $table->string('email','255');
            $table->string('phone','255');
            $table->string('interest_course','255');
            $table->string('gender','255');
            $table->string('organization','255');
            $table->string('designation','255');
            $table->string('venue','255');


            //For username and password creation
            $table->string('username','255');
            $table->string('password','255');
            $table->string('confirm','255');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_regis');
    }
}
