<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer_acc extends Model
{

    protected $table = 'trainer_accs';
    protected $primaryKey = 'id';
    protected $fillable =[

        'email',
        'password',

        'status',

    ];


    public function trainer_email(){


//    	return $this->belongsTo(Training_regi::class);
        return $this->belongsTo(Trainer_pinfo::class);

    }




}
