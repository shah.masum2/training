<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class emailNotify extends Mailable
{
    use Queueable, SerializesModels;

    //I have creted new
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
//    public function __construct()
    public function __construct(User $user)
    {
        //I have creted
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        return $this->view('view.name');
        return $this->view('emails.welcome');
    }
}
