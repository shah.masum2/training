<?php

namespace App\Http\Controllers;

use App\Mentor;
use App\Trainer_acc;
use App\Trainer_edu;
use Illuminate\Http\Request;
use App\Trainer_pinfo;
use App\Trainer_exp;
use Illuminate\Support\Facades\DB;

class MentorsController extends Controller
{

    public function index()
    {

        $mentors = Trainer_pinfo::all();

        return view('admin/mentors/index',compact('mentors'));



    }



    public function create()
    {

    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {

        $details = Trainer_pinfo::find($id);
//        $acc = Trainer_acc::find($id);
        //$education = Trainer_edu::all($id);
        $educations =  DB::select( DB::raw("SELECT * FROM trainer_edus WHERE trainer_pinfo_id = '$id'") );
        $exps =  DB::select( DB::raw("SELECT * FROM trainer_exps WHERE trainer_pinfo_id = '$id'") );
        $accs =  DB::select( DB::raw("SELECT * FROM trainer_accs WHERE trainer_pinfo_id = '$id'") );


//        return view('admin/mentors/show',compact('details','educations','exps'));
        return view('admin/mentors/show',compact('details','educations','exps','accs'));



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function edit(Mentor $mentor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mentor $mentor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mentor $mentor)
    {
        //
    }
}
