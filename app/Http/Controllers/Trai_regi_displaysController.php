<?php

namespace App\Http\Controllers;

use App\Trai_regi_display;
use App\Training_regi;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Course;

class Trai_regi_displaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $trai_regi_displays = Trai_regi_display::all()->last();

        //return view('trai_regi_displays.index');
        return view('trai_regi_displays.index',compact('trai_regi_displays'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // $trai_regi_displays = User::all()->last();

        //var_dump($retrives);
        //$retrives = Training_regi::max();
  //      $trai_regi_displays = Training_regi::all()->last();                    // this is the final
        //print $retrives;
//        dd($retrives);
        //$retrives =Training_regi::find('id')->last(); // last element
        //$retrives =Training_regi::find(1); // last element
        //$retrives = DB::table('training_regis')->latest()->first();
        //dd($retrives);

//$price = DB::table('orders')->max('price');
//====================== below one is correct
//    $retrives = DB::table('training_regis')
//                                ->select('id','interest_course', 'email')
//                                ->where('id', DB::raw("(select max(`id`) from training_regis)"))
//                                ->get();
//        ======================== below is final redirect route ============================
        //return view('trai_regi_displays/create', compact('trai_regi_displays'));
//        ======================== Up is the final route         ============================
//        return view('front/trai_regi_displays/create', compact('trai_regi_displays'));





        return view('trai_regi_displays/create');


    }


        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //dd($request);
//        $table->string('user_id');
//        $table->string('course_id');
//        $table->string('interest_course');

//       "user_id" => "1"
//       "subject_id" => "2"
//      "course_title" => "Graphics and Animation"
//      "course_fee" => "7500"
//      "course_venue" => "Banani"

        $this->validate($request,array(
            'user_id' => 'required |min:1 | max:5',
            'subject_id' => 'required',
            'course_title' => 'required | max:50',
            'course_fee' => 'required | max:50',
            'course_venue' => 'required |min:1 | max:50',

        ));
        //Session::flash('regi','Thanks for Registration !!');
        // store in the databae
        $trai_regi_displays = new Trai_regi_display;
        $trai_regi_displays->user_id = $request->user_id;
        $trai_regi_displays->subject_id = $request->subject_id;
        $trai_regi_displays->course_title = $request->course_title;
        $trai_regi_displays->course_fee = $request->course_fee;
        $trai_regi_displays->course_venue = $request->course_venue;
        $trai_regi_displays->save();


        // print_r($training_regi->getQueueableId()) ;
        //print
        //dd($training_regi->id);

        // $courseid = $training_regi->id;

        return redirect('trai_regi_displays/');
//        return redirect('training_confirmations/create');
        //print $courseid;



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return view('/trai_regi_displays/show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
