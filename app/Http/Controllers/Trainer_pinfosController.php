<?php

namespace App\Http\Controllers;

use App\Trainer_pinfo;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Trainer_edu;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Trainer_pinfosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
//        $subjects = Subject::all();
//
//        return view('admin/subjects/index',compact('subjects'));

        $all_trainers = Trainer_pinfo::all();

        return view('trainers/trainer_pinfos/index',compact('all_trainers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('trainers/trainer_pinfos/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        name date_of_birth gender religion mobile nationality marital_status photo address status
       // dd($request);
        $this->validate($request,array(
            'name' =>'required | min:3 | max:60',
            'photo' => 'required|image|mimes:jpeg,png,jpg|max:300',
//            'photo' => '',
            'date_of_birth' =>'required',
            'gender' =>'required',
            'religion' =>'required',
            'mobile' =>'required | min:11 | max:11',
            'nationality' =>'',
            'marital_status' =>'required',
            'address' => 'required|min:5 | max:300',
            'status' =>'required',

        ));

        $trainer_pinfos = new Trainer_pinfo;
        $trainer_pinfos->name = $request->name;
//        $trainer_pinfos->photo = $request->photo;

        //image section below
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' .$photo->getClientOriginalExtension();
            $location = public_path('images/'.$filename);
//            Image::make($course_image)->resize(1200,400)->save($location);
            Image::make($photo)->resize(531,413)->save($location);
            $trainer_pinfos->photo = $filename;
        }
//        image section close
        $trainer_pinfos->date_of_birth = $request->date_of_birth; //added later
        $trainer_pinfos->gender = $request->gender; //added later
        $trainer_pinfos->religion = $request->religion; //added later
        $trainer_pinfos->mobile = $request->mobile; //added later
        $trainer_pinfos->nationality = $request->nationality; //added later
        $trainer_pinfos->marital_status = $request->marital_status; //added later
        $trainer_pinfos->address = $request->address; //added later
        $trainer_pinfos->status = $request->status; //added later



        $trainer_pinfos->save();
//        session()->flash('message','New Course Added Successfully !! ');


        //dd($trainer_pinfos->id);
        //dd($courseid);
        // $courseid = $trainer_pinfos->id;// correct way to get id



        return redirect('trainers/trainer_edus/create');




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trainer_pinfo  $trainer_pinfo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

//        $details = Trainer_edu::all()->get($id);
        //$education = Trainer_edu::all($id);
        $educations =  DB::select( DB::raw("SELECT * FROM trainer_edus WHERE trainer_pinfo_id = '$id'") );
//         $educations = DB::table('trainer_edus')
//             ->whereExists(function ($query) {
//                 $query->select(DB::raw(11))
//                     ->from('trainer_edus')
//                     ->whereRaw('trainer_edus.trainer_pinfo_id = trainer_pinfo_id');
//             })
//             ->get();
//        $educations = DB::table('trainer_edus')->where('trainer_pinfo_id', '=', $id)->all($id);

        return view('trainers/trainer_pinfos/show',compact('educations'));
//        return view('admin/mentors/show',compact('details','educations'));
//        return view('admin/mentors/show',compact('details','education'));

//        return view('school_acc_balance_sheets.index',array('school_acc_balance_sheets' => $school_acc_balance_sheets,'datas'=>$data));



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trainer_pinfo  $trainer_pinfo
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer_pinfo $trainer_pinfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trainer_pinfo  $trainer_pinfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer_pinfo $trainer_pinfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trainer_pinfo  $trainer_pinfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainer_pinfo $trainer_pinfo)
    {
        //
    }
}
