<?php

namespace App\Http\Controllers;

use App\Course;
use App\User;
use Illuminate\Http\Request;
use App\Training_regi;
use Illuminate\Support\Facades\Session;
use App\Paybkash;



class Training_regisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $training_regis = User::all();

        return view('/admin/training_regis/index',compact('training_regis'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
//        dd($id);
//        return view('front/training_regis/create',['id' => $id ]);
        return view('front/training_regis/create');


    }


//    public function create($id){
//
//        return view('front/training_regis/create',compact($id));
//    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request);

        $this->validate($request,array(
            'first_name' => 'required |min:3 | max:50',
            'last_name' => 'required |min:3 | max:50',
            'email' => 'required | max:50',
            'phone' => 'required | max:50',
            'interest_course' => 'required',
            'gender' => 'required',
            'organization' => '',
            'designation' => '',
            'venue' => 'required',

            'username' => 'required | min:3 | max:30',
            'password' => 'required | min:5 | max:40',
            'confirm' => 'required | min:5 | max:40',

        ));

        //Session::flash('regi','Thanks for Registration !!');

        // store in the databae
        $training_regi = new Training_regi;
        $training_regi->first_name = $request->first_name;
        $training_regi->last_name = $request->last_name;
        $training_regi->email = $request->email;
        $training_regi->phone = $request->phone;
        $training_regi->interest_course = $request->interest_course;
        $training_regi->gender = $request->gender;
        $training_regi->organization = $request->organization;
        $training_regi->designation = $request->designation;
        $training_regi->venue = $request->venue;
        $training_regi->username = $request->username;
        $training_regi->password = $request->password;
        $training_regi->confirm = $request->confirm;
        $training_regi->save();


        // print_r($training_regi->getQueueableId()) ;
        //print
        //dd($training_regi->id);

        // $courseid = $training_regi->id;

        return redirect('front/trai_regi_displays/create');
        //print $courseid;



        //return redirect('front/training_regis/index');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $regis = Training_regi::find($id)->paybkash;
        $regis = Training_regi::find($id);

        //$paybkash = Paybkash::find($id);

        return view('admin/training_regis/show',compact('regis'));
//        return view('admin.categories.show',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Training_regi::destroy($id);
        // show alert before delete data
        session()->flash('message','Data Deleted Successfully..');
//        session()->flash('message','Data Deleted Successfully..');
        return redirect('/admin/training_regis');


    }







}