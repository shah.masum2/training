<?php

namespace App\Http\Controllers;

use App\Paybkash;
use App\Training_confirmation;
use Illuminate\Http\Request;

class PaybkashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('paybkash/index');

        //return view('front.paybksh.index',compact('paybksh'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $paybkashs = Training_confirmation::all()->last();
//====================== below one is correct
//    $retrives = DB::table('trai_regi_displays')
//                                ->select('id','interest_course', 'email')
//                                ->where('id', DB::raw("(select max(`id`) from trai_regi_displays)"))
//                                ->get();
                return view('paybkash/create',compact('paybkashs'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request);

        $this->validate($request,array(
            'user_id' => 'required |min:1 | max:50',
//            'course_id' => 'required |min:1 | max:50',
            'subject_id' => 'required | max:50',
            'course_fee' => 'required | min:4 | max:22',
//            'transaction_id' => 'required | min:10| max:50',
//            'mobile' => '',
            'transaction_id' => 'required | min:10| max:30',



        ));



        $paybkash = new Paybkash;
        $paybkash->user_id = $request->user_id;
//        $paybkash->course_id = $request->course_id;
        $paybkash->subject_id = $request->subject_id;
        $paybkash->course_fee = $request->course_fee;
        $paybkash->transaction_id = $request->transaction_id;

        $paybkash->save();




        return redirect('paybkash/');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
