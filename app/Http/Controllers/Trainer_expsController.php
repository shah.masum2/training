<?php

namespace App\Http\Controllers;

use App\Trainer_edu;
use App\Trainer_pinfo;
use App\Trainer_exp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class Trainer_expsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $latest_trainer_edu = Trainer_edu::all()->last();

        return view('trainers/trainer_exps/create',compact('latest_trainer_edu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request);
//        $this->validate($request,array(
//            'title' =>'required | min:3 | max:60',
//            'company' =>'required',
//            'company_addres' =>'required',
//            'start_from' =>'required',
//            'end_from' =>'required | min:11 | max:11',
//            'continue' =>'required | min:11 | max:11',
//            'status' =>'required',
//
//        ));

//        ====================================Final Input=============================

        //dd($request);
        $input = Input::all();
        $condition = $input['trainer_pinfo_id'];
        foreach ($condition as $key => $condition) {
            $trainer_exps = new Trainer_exp;
            $trainer_exps->trainer_pinfo_id = $input['trainer_pinfo_id'][$key];
            $trainer_exps->designation_title= $input['designation_title'][$key];
            $trainer_exps->com_name= $input['com_name'][$key];
            $trainer_exps->com_department= $input['com_department'][$key];
            $trainer_exps->com_location= $input['com_location'][$key];
            $trainer_exps->timefrom= $input['timefrom'][$key];
            $trainer_exps->timeto= $input['timeto'][$key];
//            $trainer_exps->continue= $input['continue'][$key];
            $trainer_exps->status= $input['status'][$key];
            $trainer_exps->save();
        }

//        ====================================Final Input=============================

//        $trainer_exps = new Trainer_exp;
//        $trainer_exps->degree = $request->degree;
//        $trainer_exps->institute = $request->institute; //
//        $trainer_exps->department_group = $request->department_group; //
//        $trainer_exps->result = $request->result; //
//        $trainer_exps->year = $request->year; //
//        $trainer_exps->status = $request->status; //
//
//        $trainer_exps->save();
//        session()->flash('message','New Course Added Successfully !! ');

        return redirect('trainers/trainer_accs/create');
//        return redirect('trainers/trainer_exps/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trainer_exps  $trainer_exps
     * @return \Illuminate\Http\Response
     */
    public function show(Trainer_exps $trainer_exps)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trainer_exps  $trainer_exps
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer_exps $trainer_exps)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trainer_exps  $trainer_exps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer_exps $trainer_exps)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trainer_exps  $trainer_exps
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainer_exp $trainer_exps)
    {
        //
    }
}
