<?php

namespace App\Http\Controllers;

use App\Trainer_edu;
use App\Trainer_pinfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class Trainer_edusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $latest_trainer = Trainer_pinfo::all()->last();

        return view('trainers/trainer_edus/create',compact('latest_trainer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $validatedData = $request->validate([
        //     'name' => 'required|min:5',
        // ]);
        // Category::create($validatedData);
        // session()->flash('message','Data Inserted Successfully..');
        // return redirect('/admin/categories');
//        =========================================================================
//        ================== Successful test it is. ====================

//                $validatedData = $request->validate([
//                 'trainer_pinfo_id' =>'required',
//                 'degree' =>'required ',
//                 'institute' =>'required',
//                 'department_group' =>'required',
//                 'result' =>'required',
//                 'year' =>'required',
//                 'board' =>'required',
//                 'status' =>'required| min:1',
//
//        ]);
//
//        $input = Input::all($validatedData);
        //dd($request);
        $input = Input::all();
        $condition = $input['trainer_pinfo_id'];
        foreach ($condition as $key => $condition) {
            $trainer_edus = new Trainer_edu;
            $trainer_edus->trainer_pinfo_id = $input['trainer_pinfo_id'][$key];
            $trainer_edus->degree= $input['degree'][$key];
            $trainer_edus->institute= $input['institute'][$key];
            $trainer_edus->department_group= $input['department_group'][$key];
            $trainer_edus->result= $input['result'][$key];
            $trainer_edus->year= $input['year'][$key];
            $trainer_edus->board= $input['board'][$key];
            $trainer_edus->status= $input['status'][$key];
            $trainer_edus->save();
        }

//        ======================== Another test close ====================


        //dd($request);
//        $validatedData = $request->validate([
//
//                 'trainer_pinfo_id' =>'required',
//                 'degree' =>'required ',
//                 'institute' =>'required',
//                 'department_group' =>'required',
//                 'result' =>'required',
//                 'year' =>'required',
//                 'board' =>'required',
//                 'status' =>'required| min:1',
//
//        ]);
        /////////////Trainer_edu::create($validatedData);
//        =========================================
//        for ($i=0; $i < count($validatedData['trainer_pinfo_id']); ++$i)
//        {
//            $trainer_edus= new Trainer_edu;
//            $trainer_edus->trainer_pinfo_id = $validatedData['trainer_pinfo_id'][$i];
//            $trainer_edus->degree= $validatedData['degree'][$i];
//            $trainer_edus->institute= $validatedData['institute'][$i];
//            $trainer_edus->department_group= $validatedData['department_group'][$i];
//            $trainer_edus->result= $validatedData['result'][$i];
//            $trainer_edus->year= $validatedData['year'][$i];
//            $trainer_edus->board= $validatedData['board'][$i];
//            $trainer_edus->status= $validatedData['status'][$i];
//            $trainer_edus->save();
//        }

//        ==========================================
        // Trainer_edu::create($validatedData);





//
////        ========================= ORIGINAL DATA BELOW ================================================
//// if we use the process , we must remove the [] sign from the html form page. than data
////        would insert into the database else show error . ok
//        //dd($request);
//       $this->validate($request,array(
//            'trainer_pinfo_id' =>'required ',
//            'degree' =>'required ',
//            'institute' =>'required',
//            'department_group' =>'required',
//            'result' =>'required',
//            'year' =>'required ',
//            'board' =>'required ',
//            'status' =>'required',
//
//        ));
//
//        $trainer_edus = new Trainer_edu;
//        $trainer_edus->trainer_pinfo_id = $request->trainer_pinfo_id;
//        $trainer_edus->degree = $request->degree;
//        $trainer_edus->institute = $request->institute; //
//        $trainer_edus->department_group = $request->department_group; //
//        $trainer_edus->result = $request->result; //
//        $trainer_edus->year = $request->year; //
//        $trainer_edus->board = $request->board; //
//        $trainer_edus->status = $request->status; //
//
//        $trainer_edus->save();
//        session()->flash('message','New Course Added Successfully !! ');

        return redirect('trainers/trainer_exps/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trainer_edu  $trainer_edus
     * @return \Illuminate\Http\Response
     */
    public function show(Trainer_edu $trainer_edus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trainer_edu  $trainer_edus
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer_edu $trainer_edus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trainer_edu  $trainer_edus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer_edu $trainer_edus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trainer_edu  $trainer_edu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainer_edu $trainer_edus)
    {
        //
    }
}
