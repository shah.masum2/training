<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;


class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = Subject::all();

        return view('admin/subjects/index',compact('subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/subjects/create');
    }


    public function store(Request $request)
    {


        //$table->string('course_category_id');
//            added later
        //$table->string('course_module_type'); //workshop or certification course
//            added later
        //$table->string('course_title');
        //$table->string('course_image');
        //$table->string('course_date');
        //$table->string('course_class_time');
        //added later
        //$table->integer('course_fee');
        //$table->string('course_total_classes');
        //$table->string('course_total_duration');
        //$table->string('course_venue');
        // added later up
        //$table->string('course_type');
        //$table->string('course_deadline');
        //$table->text('course_about');
        //$table->text('course_object');
        //$table->text('course_others');
        //$table->text('course_who_attend');

        //dd($request);
        $this->validate($request,array(
            'course_category_id' =>'required',
            'course_module_type' =>'required',//added later
            'course_title' =>'required',
            'course_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'course_date' =>'required',
            'course_class_time' =>'required',
//            added later
            'course_fee' =>'required',
            'course_total_classes' =>'required',
            'course_total_duration' =>'required',//============
            'course_venue' =>'required',
//            added later
            'course_type' =>'required',
            'course_deadline' =>'required',
            'course_about' =>'required',
            'course_object' =>'required',
            'course_others' =>'required',
            'course_who_attend' =>'required',

        ));

        $subjects = new Subject;
        $subjects->course_category_id = $request->course_category_id;
        $subjects->course_module_type = $request->course_module_type; //added later
        $subjects->course_title = $request->course_title;
        $subjects->course_date = $request->course_date;
        $subjects->course_class_time = $request->course_class_time;
        //added later
        $subjects->course_fee = $request->course_fee;
        $subjects->course_total_classes = $request->course_total_classes;
        $subjects->course_total_duration = $request->course_total_duration;
        $subjects->course_venue = $request->course_venue;
        //added later
        $subjects->course_type = $request->course_type;
        $subjects->course_deadline = $request->course_deadline;
        $subjects->course_about = $request->course_about;
        $subjects->course_object = $request->course_object;
        $subjects->course_others = $request->course_others;
        $subjects->course_who_attend = $request->course_who_attend;

        //image section below
        if ($request->hasFile('course_image')) {
            $course_image = $request->file('course_image');
            $filename = time() . '.' .$course_image->getClientOriginalExtension();
            $location = public_path('images/'.$filename);
            Image::make($course_image)->resize(1200,400)->save($location);
            $subjects->course_image = $filename;
        }
//        image section close

//       another upload method
//
//        if ($request->hasFile('course_image')) {
//            $course_image = $request->file('course_image');
//            $filename = time().'.'.$course_image->getClientOriginalExtension();
//            $destinationPath = public_path('/images');
//            $course_image->move($destinationPath, $filename);
//            //$this->save();
//            //$subjects->save();
//            $subjects->course_image=$course_image;
//
//           // return back()->with('success','Image Upload successfully');
//        }
//

//

        $subjects->save();
        session()->flash('message','New Course Added Successfully !! ');

        return redirect('admin/subjects');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return view('admin/subjects/show');
        $subjects = Subject::find($id);
        return view('admin/subjects/show',compact('subjects'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subjects = Subject::find($id);
        return view('admin/subjects/edit',compact('subjects'));


        //return view('admin/subjects/edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);

        $this->validate($request,array(
            'course_category_id' =>'required',
            'course_module_type' =>'required',//added later
            'course_title' =>'required',
            'course_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'course_date' =>'required',
            'course_class_time' =>'required',
//            added later
            'course_fee' =>'required',
            'course_total_classes' =>'required',
            'course_total_duration' =>'required',//============
            'course_venue' =>'required',
//            added later
            'course_type' =>'required',
            'course_deadline' =>'required',
            'course_about' =>'required',
            'course_object' =>'required',
            'course_others' =>'required',
            'course_who_attend' =>'required',

        ));


        $subjects = Subject::find($id);
        $subjects->course_category_id = $request->course_category_id;
        $subjects->course_module_type = $request->course_module_type; //added later
        $subjects->course_title = $request->course_title;
        $subjects->course_date = $request->course_date;
        $subjects->course_class_time = $request->course_class_time;
        //added later
        $subjects->course_fee = $request->course_fee;
        $subjects->course_total_classes = $request->course_total_classes;
        $subjects->course_total_duration = $request->course_total_duration;
        $subjects->course_venue = $request->course_venue;
        //added later
        $subjects->course_type = $request->course_type;
        $subjects->course_deadline = $request->course_deadline;
        $subjects->course_about = $request->course_about;
        $subjects->course_object = $request->course_object;
        $subjects->course_others = $request->course_others;
        $subjects->course_who_attend = $request->course_who_attend;


        if ($request->hasFile('course_image')) {
            // for adding the new image
            $course_image = $request->file('course_image');
            $filename = time() . '.' .$course_image->getClientOriginalExtension();
            $location = public_path('images/'.$filename);
            Image::make($course_image)->resize(1200,400)->save($location);
            // below this is the existing or old image file name from database
            $oldFilename = $subjects->course_image;
            //update the database file name
            $subjects->course_image = $filename;
            //Delete the old one from database
            Storage::delete($oldFilename);
        }

        $subjects->update();

        session()->flash('message','Course Updated Successfully !! ');
        return redirect('admin/subjects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('admin/subjects');
    }
}
