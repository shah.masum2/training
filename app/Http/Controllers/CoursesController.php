<?php

namespace App\Http\Controllers;

use App\Course;
use App\Subject;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $courses = Subject::all();
         //dd($courses);
        return view('front.courses.index',compact('courses'));

//        $admincourse = Course::all();
//        //dd($courses);
//        return view('admin.courses.index',compact('admincourse'));
//

        // return view('front.courses.index')->withCourses('courses');

//        return view('school_job_app_prs.index', ['school_job_app_prs' => $school_job_app_prs]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('course/create');
//        return view('admin/courses/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request);

        $this->validate($request,array(
            'c_category_id' => 'required |min:3 | max:100',
            'course_title' => 'required |min:3 | max:100',
            'c_image' => '',
            'c_date' => 'required ',
            'c_class_time' => 'required',
            'c_type' => 'required',
            'c_deadline' => 'required',
            'c_about' => '',
            'c_object' => '',
            'c_others' => '',
            'c_who_attend' => 'required',

        ));

        //Session::flash('regi','Thanks for Registration !!');

        // store in the databae
        $training_regi = new Course;
        $training_regi->c_category_id = $request->c_category_id;
        $training_regi->course_title = $request->last_name;
        $training_regi->c_image = $request->c_image;
        $training_regi->c_date = $request->c_date;
        $training_regi->c_class_time = $request->c_class_time;
        $training_regi->c_type = $request->c_type;
        $training_regi->c_deadline = $request->c_deadline;
        $training_regi->c_about = $request->c_about;
        $training_regi->c_object = $request->c_object;
        $training_regi->c_others = $request->c_others;
        $training_regi->c_who_attend = $request->c_who_attend;
        $training_regi->save();

       // return redirect('admin/course/index');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $course = Subject::find($id);
        return view('front.courses.show',compact('course'));



        //           $course = Course::find($id);
//        return view('forms.show')->withForm($form);
//

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }
}
