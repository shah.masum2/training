<?php

namespace App\Http\Controllers;

use App\Trainer_acc;
use App\Trainer_exp;
use Illuminate\Http\Request;

class Trainer_accsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trainer_id = Trainer_exp::all()->last();
        return view('trainers/trainer_accs/create',compact('trainer_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request,array(
            'trainer_pinfo_id' =>'required',
            'email' =>'required',//added later
            'password' =>'required',
//            'course_date' =>'required',
            'status' =>'required',
//

        ));

        $trainer_acc = new Trainer_acc;
        $trainer_acc->trainer_pinfo_id = $request->trainer_pinfo_id;
//        $trainer_acc->course_module_type = $request->course_module_type; //added later
        $trainer_acc->email = $request->email;
        $trainer_acc->password = bcrypt($request->password);
        $trainer_acc->status = $request->status;
        $trainer_acc->save();

        return view('trainers/thanks');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
