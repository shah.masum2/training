<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function index()
    {

        $applicants = User::all();

        return view('admin/users/index',compact('applicants'));
        //return view('admin/users/index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('users/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);


        $this->validate($request,array(
            'first_name' => 'required |min:3 | max:50',
            'last_name' => 'required |min:3 | max:50',
            'email' => 'required | max:50',
            'phone' => 'required | max:50',
            'interest_course' => 'required',
            'gender' => 'required',
            'organization' => '',
            'designation' => '',
            'venue' => 'required',

            'user_id' => 'required | min:3 | max:30',
            'password' => 'required | min:5 | max:40',

        ));

        //Session::flash('regi','Thanks for Registration !!');

        // store in the databae
        $training_regi = new User;
        $training_regi->first_name = $request->first_name;
        $training_regi->last_name = $request->last_name;
        $training_regi->email = $request->email;
        $training_regi->phone = $request->phone;
        $training_regi->interest_course = $request->interest_course;
        $training_regi->gender = $request->gender;
        $training_regi->organization = $request->organization;
        $training_regi->designation = $request->designation;
        $training_regi->venue = $request->venue;
        $training_regi->user_id = $request->user_id;
        $training_regi->password = bcrypt($request->password);
//        $training_regi->confirm = $request->confirm;
        $training_regi->save();


        // print_r($training_regi->getQueueableId()) ;
        //print
        //dd($training_regi->id);

        // $courseid = $training_regi->id;

        return redirect('trai_regi_displays/create');
        //print $courseid;


    }


    public function show($id)
    {
        $regis = User::find($id);

        //$paybkash = Paybkash::find($id);

        return view('admin/users/show',compact('regis'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        // show alert before delete data
        session()->flash('message','Applicant Deleted Successfully..');
//        session()->flash('message','Data Deleted Successfully..');
        return redirect('/admin/users');

    }
}
