<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Adcourse;

class AdcourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('/front/adcourse/index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front/adcourse/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

//        $validatedData = $request->validate([
//            //'name' => 'required|min:5',
//            'course_category_id' => 'required',
//            'course_title' => 'required',
//            'course_date' => 'required',
//            'course_class_time' => 'required',
//            'course_type' => 'required',
//            'course_deadline' => 'required',
//            'course_about' => 'required',
//            'course_object' => 'required',
//            'course_others' => 'required',
//            'course_who_attend' => 'required',
//        ]);
//
//        $this->validate($request,array(
//            'course_category_id' => 'required',
//            'course_title' => 'required',
//            'course_date' => 'required',
//            'course_class_time' => 'required',
//            'course_type' => 'required',
//            'course_deadline' => 'required',
//            'course_about' => 'required',
//            'course_object' => 'required',
//            'course_others' => 'required',
//            'course_who_attend' => 'required',
//        ));

        $adcourse = new Adcourse;
        $adcourse->course_category_id = $request->course_category_id;
        $adcourse->course_title = $request->course_title;
        $adcourse->course_date = $request->course_date;
        $adcourse->course_class_time = $request->course_class_time;
        $adcourse->course_type = $request->course_type;
        $adcourse->course_deadline = $request->course_deadline;
        $adcourse->course_about = $request->course_about;
        $adcourse->course_object = $request->course_object;
        $adcourse->course_others = $request->course_others;
        $adcourse->course_who_attend = $request->course_who_attend;

        $adcourse->save();
        dd($adcourse);

        return redirect('/front/adcourses');

        //Adcourse::create($validatedData);
        //session()->flash('message','Data Inserted Successfully..');
        //return view('/front/adcourse/edit');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('front/adcurse/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
