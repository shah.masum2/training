<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
//use Greggilbert\Recaptcha\Recaptcha;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
//    protected $redirectTo = '/home';



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // dd($data);
        return Validator::make($data, [
//            'name' => 'required|string|max:255',
//            'email' => 'required|string|email|max:255|unique:users',
//            'password' => 'required|string|min:6|confirmed',

        // original form validation all of the up

//          my custom field of registration form
            'first_name' => 'required |min:3 | max:50',
            'last_name' => 'required |min:3 | max:50',
            'email' => 'required | max:50 | unique:users',
            'phone' => 'required | max:50 | string',
            'interest_course' => 'required',
            'gender' => 'required',
            'organization' => '',
            'designation' => '',
            'venue' => 'required',
//username and password below
            'user_id' => 'required | min:4 | max:30 |unique:users',
            'password' => 'required |string|min:6 | max:40|confirmed',
            'g-recaptcha-response' => 'required',

        ]);
    }

//    CUSTOM METHOD OVERRIDE FUNCTION

//    protected function validateLogin(Request $request)
//    { $this->validate($request, [
//        $this->loginUsername() => 'required',
//        'password' => 'required',
//        'g-recaptcha-response' => 'required|captcha'
//
//    ]); }
//





//    CUSTOM METHOD OVERRIDE FUNCTION


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return $user =  User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => bcrypt($data['password']),

//        custom from bellow

            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'interest_course' => $data['interest_course'],
            'gender' => $data['gender'],
            'organization' => $data['organization'],
            'designation' => $data['designation'],
            'venue' => $data['venue'],
            'user_id' => $data['user_id'],
            'password' => bcrypt($data['password']),
            'g-recaptcha-response' => $data['g-recaptcha-response'],

        ]);


       // \Mail::to($user)->send(new Mail);
        Mail::to($user)->send(new emailNotify);


    }



//========================closet bracket
}
