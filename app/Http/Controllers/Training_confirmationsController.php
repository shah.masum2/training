<?php

namespace App\Http\Controllers;

use App\Trai_regi_display;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Training_confirmation;

class Training_confirmationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('front/training_confirmations/index');
        //return view('front.trai_regi_displays.index');
//        return view('front.courses.index',compact('courses'));
//
    }

    /**
     * Show the form for creating a new resource.
     *
     *  $table->integer('user_id')->unsigned();
    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');//->onDelete("cascade")

    $table->string('subject_id');
    $table->string('course_title');
    $table->string('course_fee');
    $table->string('course_venue');
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $training_confirmations = Trai_regi_display::all()->last();

//====================== below one is correct
//    $retrives = DB::table('trai_regi_displays')
//                                ->select('id','interest_course', 'email')
//                                ->where('id', DB::raw("(select max(`id`) from trai_regi_displays)"))
//                                ->get();

        return view('training_confirmations.create',compact('training_confirmations'));
        //return view('front.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         //dd($request);

        $this->validate($request,array(
            'user_id' => 'required |min:1 | max:50',
            'subject_id' => 'required',
            'course_fee' => 'required ',
            'payment_by' => 'required ',

        ));

        //Session::flash('regi','Thanks for Registration !!');
        $training_confirmations = new Training_confirmation;
        $training_confirmations->user_id = $request->user_id;
        $training_confirmations->subject_id = $request->subject_id;
        $training_confirmations->course_fee = $request->course_fee;
        $training_confirmations->payment_by = $request->payment_by;
        $training_confirmations->save();

        return redirect('paybkash/create');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
