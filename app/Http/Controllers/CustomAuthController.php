<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class CustomAuthController extends Controller
{
    //

    public  function showRegisterForm(){

        return view('custom.register');

    }

    public  function register(Request $request){


        //return $request->all();
        $this->validation($request);
        User::create($request->all());
        return redirect('/')->with('Status','You are registered');

    }

    public function validation($request){

        return $this->validate($request,[

           // my custom field of registration form
            'first_name' => 'required |min:3 | max:50',
            'last_name' => 'required |min:3 | max:50',
            'email' => 'required | max:50 | unique:users',
            'phone' => 'required | max:50 | string',
            'interest_course' => 'required',
            'gender' => 'required',
            'organization' => '',
            'designation' => '',
            'venue' => 'required',
//username and password below
            'user_id' => 'required | min:4 | max:30',
            'password' => 'required |string|min:6 | max:40|confirmed',

            ]);

    }
}
