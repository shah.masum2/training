<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Paybkash;

class User extends Authenticatable
{
    use Notifiable;


    public function paybkash(){

        return $this->hasOne(Paybkash::class);
//        return $this->hasOne('App\Paybkash');
//        return $this->hasMany(Paybkash::class);

    }





    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'interest_course',
        'gender',
        'organization',
        'designation',
        'venue',
////        added for confirmation later bellow
//        'confirmed',
//        'token',
////        added for confirmation later up two columns
        'user_id',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];






}
