<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer_edu extends Model
{
    protected $table = 'trainer_edus';
    protected $primaryKey = 'id';
    protected $fillable =[

        'name',
        'photo',
        'date_of_birth',
        'gender',
        'religion',
        'mobile',
//        'nationality',
        'marital_status',
        'address',
        'status',

    ];


    public function trainer_pinfo(){


//    	return $this->belongsTo(Training_regi::class);
        return $this->belongsTo(Trainer_pinfo::class);

    }



//=======================closing bracket below main one
}
