<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer_pinfo extends Model
{
    
//    //$trainer_pinfs
    protected $table = 'trainer_pinfos';
    protected $primaryKey = 'id';
    protected $fillable =[

        'name',
        'photo',
        'date_of_birth',
        'gender',
        'religion',
        'mobile',
//        'nationality',
        'marital_status',
        'address',
        'status',

        ];
    //=================================

    public function trainer_edu(){

        return $this->hasOne(Trainer_edu::class);
//        return $this->hasOne('App\Paybkash');
//        return $this->hasMany(Paybkash::class);

    }

    public function trainer_exp(){

    return $this->hasOne(Trainer_exp::class);
//        return $this->hasOne('App\Paybkash');
//        return $this->hasMany(Paybkash::class);

}


}
